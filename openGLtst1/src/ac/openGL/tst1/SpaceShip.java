package ac.openGL.tst1;

import java.io.IOException;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;
import ac.ace3d.Vector;
import ac.ace3d.meshes.ColliderMesh;
import ac.ace3d.meshes.Mesh;
import ac.ace3d.world.Ace3dObject;
import ac.ace3d.world.Camera;
import ac.ace3d.Orientation;
import android.content.res.Resources;
import android.util.Log;

public class SpaceShip extends Ace3dObject {

	private Mesh m;
	private float speed;
	private float angularSpeed;
	
	protected SpaceShip(Resources res, Vector position) {
		super(res, position);
		//meshes = new ArrayList<MeshFromOBJ>();
		try {
			ArrayList<Mesh>meshs = ac.ace3d.meshes.objReader.processStream(R.raw.spaceship, res);
			for (int i=0; i < meshs.size(); i++)
			{
				meshes.add(meshs.get(i));
			}
		} catch (IOException e) {
			e.printStackTrace();    
		}		
		m = meshes.get(0);
		place(position);
	}

	public void setSpeed(float sp)
	{
		speed = sp;
	}	
	
	/*public void move() {
		
	}*/

	private void place(float x, float y, float z) {
		// TODO Auto-generated method stub
		m.translateX(x);
		m.translateY(y);
		m.translateZ(z);
	}	
	
	
	public void place(Vector position) {
		this.worldPosition = position;
		place(position.getiComponent(), position.getjComponent(), position.getkComponent());
		
	}

	/*public void rotate(float rx, float ry, float rz) {
		m.rotateX(rx);
		m.rotateY(ry);
		m.rotateZ(rz);
	}*/	
	
	/*public void setOrientation(Orientation or){
		super.setOrientation(or);
		direction = Orientation.getDirectionFormOrientation(or);
	}*/
	

	public void render(GL10 gl) {
		m.render(gl);
	}

	public Vector getPositionChange() {
		// TODO Auto-generated method stub
		return null;
	}

}

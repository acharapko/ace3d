package ac.openGL.tst1;

import java.io.IOException;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.Vector;
import ac.ace3d.collisions.RectangularPlateCollider;
import ac.ace3d.meshes.Mesh;
import ac.ace3d.meshes.objReader;
import ac.ace3d.meshes.TextureManager;
import ac.ace3d.world.FlatWall;
import ac.ace3d.world.Floor;
import ac.ace3d.Orientation;
import ac.ace3d.world.Group;
import ac.ace3d.world.Sprite;
import ac.ace3d.world.World;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Debug;
import android.util.Log;

public class MyRoom extends Group{	
	
	public MyRoom(Resources res, GL10 gl)
	{
		super(gl);		
		
		
		ArrayList<Mesh>meshes = new ArrayList<Mesh>();
		//meshes = objReader.getObjectWithCollidersFromOBJ(R.raw.tilefloor2, res);
		try {
			meshes = objReader.processStream(R.raw.tilefloor2, res);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Mesh m = meshes.get(0);		
		//m.rotateX(90);
		//m.setFaceCulling(ACE3D.ACE3D_CULL_FACE_NONE);
		Floor fl = new Floor(res, new Vector(7.0f, 0.0f, 7f), m);
		fl.setMaxDimension(14.0f);
		//fl.addCollider(new RectangularPlateCollider(fl, new Vector(7.0f, 0.0f, 7f), new Vector(0,1,0), 7, 6));
		//m.translate(0, -0.2f, -10f);	
		Bitmap b = BitmapFactory.decodeResource(res, R.drawable.tile);	
		//fl.setTexture(b);
		fl.setTexture(TextureManager.loadTexture(gl, b, "floor", GL10.GL_REPEAT, GL10.GL_REPEAT));
		fl.setTag("floor");
		addStationaryObject(fl);
		Log.d("Native Heap Size" ,Debug.getNativeHeapSize() / 1024 + " kb:");
		Log.d("Native Heap Allocated Size" ,Debug.getNativeHeapAllocatedSize() / 1024 + " kb:");
		Log.d("Native Heap Free Memory" ,Debug.getNativeHeapFreeSize() / 1024 + " kb:");
		//myProjectile mp = new myProjectile(res, new Vector(0,0,0));		
		//addStationaryObject(mp);
		
		FlatWall wll1 = new FlatWall(res, new Vector(6.22f, 1.50f, 0), 12.44f, 3);
		wll1.changeOrientation(new Orientation(270, 0, 0));
		
		b = BitmapFactory.decodeResource(res, R.drawable.bricks1);		
		wll1.setTexture(TextureManager.loadTexture(gl, b, "wall1", GL10.GL_REPEAT, GL10.GL_REPEAT), 12.44f, 3);
		addStationaryObject(wll1);
	
		FlatWall wll2 = new FlatWall(res, new Vector(6.22f, 1.50f, 14), 12.44f, 3);
		wll2.changeOrientation(new Orientation(90, 0, 0));
		//b = BitmapFactory.decodeResource(res, R.drawable.bricks1);	
		wll2.setTexture(TextureManager.getTexturePointer("wall1"), 12.44f, 3);
		addStationaryObject(wll2);
	
		FlatWall wll3 = new FlatWall(res, new Vector(12.44f, 1.50f, 7), 14f, 3);
		wll3.changeOrientation(new Orientation(180, 0, 0));
		//b = BitmapFactory.decodeResource(res, R.drawable.bricks1);
		wll3.setTexture(TextureManager.getTexturePointer("wall1"), 12.44f, 3);
		//wll3.setTexture(b, 14f, 3);
		addStationaryObject(wll3);
	
		FlatWall wll4 = new FlatWall(res, new Vector(0f, 1.50f, 7), 14f, 3);
		wll4.changeOrientation(new Orientation(0, 0, 0));
		//b = BitmapFactory.decodeResource(res, R.drawable.bricks1);	
		wll4.setTexture(TextureManager.getTexturePointer("wall1"), 12.44f, 3);
		addStationaryObject(wll4);
		//b.recycle()
	
		meshes = objReader.getObjectWithCollidersFromOBJ(R.raw.tilefloor2, res);
		m = meshes.get(0);		
		m.rotateX(180);
		//m.setFaceCulling(ACE3D.ACE3D_CULL_FACE_NONE);
	
		Floor ceiling = new Floor(res, new Vector(7.0f, 3.0f, 7f), m);
		//m.translate(0, -0.2f, -10f);	
		b = BitmapFactory.decodeResource(res, R.drawable.ceiling);	
		ceiling.setTexture(TextureManager.loadTexture(gl, b, "ceiling", GL10.GL_REPEAT, GL10.GL_REPEAT));
		ceiling.setTag("ceiling");  
		addStationaryObject(ceiling);
	
		Sprite spr1 = new Sprite(res, new Vector(6.22f, 1.50f, 0.01f), 1, 1);
		b = BitmapFactory.decodeResource(res, R.drawable.face);	
		spr1.setTexture(TextureManager.loadTexture(gl, b, "spr1", GL10.GL_REPEAT, GL10.GL_REPEAT), 1, 1);
		//spr1.setTexture(b, 1, 1);
		spr1.changeOrientation(new Orientation(270, 0, 0));
		addStationaryObject(spr1);
	
		Sprite spr2 = new Sprite(res, new Vector(0.01f, 1.50f, 4f), 2, 2);
		b = BitmapFactory.decodeResource(res, R.drawable.f3);	
		spr2.setTexture(TextureManager.loadTexture(gl, b, "spr2", GL10.GL_REPEAT, GL10.GL_REPEAT), 1, 1);
		//spr2.setTexture(b, 1, 1);
		spr2.changeOrientation(new Orientation(0, 0, 0));
		addStationaryObject(spr2);
		
	
		Crate c1 = new Crate(res, new Vector(4f, 1f, 6f));
		b = BitmapFactory.decodeResource(res, R.drawable.crate);	
		c1.setTexture(TextureManager.loadTexture(gl, b, "c1", GL10.GL_REPEAT, GL10.GL_REPEAT), 1, 1);
		addStationaryObject(c1);
	
		Crate c2 = new Crate(res, new Vector(6.4f, 1f, 6.4f));
		c2.changeOrientation(new Orientation(45,0,0));
		c2.setTexture(TextureManager.getTexturePointer("c1"), 1, 1);
		addStationaryObject(c2);
		
		Crate c3 = new Crate(res, new Vector(5.7f, 1f, 4.0f));
		c3.changeOrientation(new Orientation(30,0,0));
		c3.setTexture(TextureManager.getTexturePointer("c1"), 1, 1);
		addStationaryObject(c3);
		
		Crate c4 = new Crate(res, new Vector(6.5f, 3.83f, 8.1f));
		c4.changeOrientation(new Orientation(60,0,0));
		c4.setScaling(0.35f);
		c4.setTexture(TextureManager.getTexturePointer("c1"), 1, 1);
		addStationaryObject(c4);
		
		
		Sprite door = new Sprite(res, new Vector(10.0f, 1f, 13.99f), 2, 2);
		b = BitmapFactory.decodeResource(res, R.drawable.door);	
		door.setTexture(TextureManager.loadTexture(gl, b, "door1", GL10.GL_REPEAT, GL10.GL_REPEAT), 1, 1);
		//door.setTexture(b, 1, 1);
		door.changeOrientation(new Orientation(90, 0, 0));
		addStationaryObject(door);
	
		//SpaceShip sp = new SpaceShip(res, new Vector(6.22f, 1.50f, 1.1f));
		//addStationaryObject(sp);
	
		
		//ace3dR.addRenderable(fl);*/
		
	}

}

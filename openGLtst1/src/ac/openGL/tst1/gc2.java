package ac.openGL.tst1;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.ACEMath;
import ac.ace3d.Orientation;
import ac.ace3d.Vector;
import ac.ace3d.lighting.myLight;
import ac.ace3d.meshes.TextureManager;
import ac.ace3d.scene.Scene;
import ac.ace3d.world.Ace2DText;
import ac.ace3d.world.Player;
import ac.ace3d.world.Sprite;
import ac.ace3d.world.World;
import ac.ace3d.world.WorldA;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.os.Debug;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class gc2 extends Scene {
	
	//private Camera c;
	
	private int gcFrameTick = 0;
	private final static int GC_FRAME = 50;
	private float frameRate;
	
	
	private MyRoom mr;
	private World w;
	private int moveFront = 0;	  
	private int rotateY = 0;
	Ace2DText txt;
	
	private int sleepAdj = 0;
	
	private Player player;
	
	
	public gc2(Context context)
	{
		super(context); 
		setKeepScreenOn(true);
	}
	
	public void init(GL10 gl) {
		myLight ml = null;
		try {
			ml = new myLight(0);
		} catch (Exception e1) {
		
			e1.printStackTrace();
		}
		Log.d("Native Heap Size" ,Debug.getNativeHeapSize() / 1024 + " kb:");
		Log.d("Native Heap Allocated Size" ,Debug.getNativeHeapAllocatedSize() / 1024 + " kb:");
		Log.d("Native Heap Free Memory" ,Debug.getNativeHeapFreeSize() / 1024 + " kb:");
		ace3dR.addLight(ml);
		ace3dR.enableLighting();
		ace3dR.enableLightSource(0);
		
		player = new Player(new Vector(4.0f, 1.5f, 10));
		player.setTag("player");
		Log.d("Native Heap Free Memory" ,Debug.getNativeHeapFreeSize() / 1024 + " kb:");
		//c = new Camera();b
		//player.setPosition(new Vector(4.0f, 1.5f, 10));
		player.setSpeed(1.1f);
		player.setMaxAngSpeed(125);
		w = new WorldA();
		
	
		w.addMovingObject(player);
		Log.d("Native Heap Free Memory" ,Debug.getNativeHeapFreeSize() / 1024 + " kb:");
		mr = new MyRoom(getResources(), gl);
		mr.translate(new Vector(-0.0f, -0.00f, -0.0f));
		
		txt = new Ace2DText(getResources(), new Vector(-2.2f, 1.6f, -4.0f), 128, 128, 10, 10);
		txt.setText("Hello", gl);		
		txt.changeOrientation(new Orientation(270, 0, 0));
		ace3dR.addnonSceneRenderable(txt);
		
		
		/*Sprite spr1 = new Sprite(getResources(), new Vector(0.5f, 0.50f, -1.0f), 1, 1);
		Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.face);	
		spr1.setTexture(TextureManager.loadTexture(gl, b, "sprrr", GL10.GL_REPEAT, GL10.GL_REPEAT), 1, 1);
		//spr1.setTexture(b, 1, 1);
		spr1.changeOrientation(new Orientation(270, 0, 0));
		ace3dR.addnonSceneRenderable(spr1);*/
		
		
		//w.addObject(txt);
		//mr.applyCamera(c);
		w.addGroup(mr);
		ace3dR.applyCamera(player.getCamera());
		ace3dR.addRenderable(w);
		
		System.gc();
		
		
	}
	
	public void newFrame(float deltaTime, GL10 gl)
	{		
		//Log.d("New Frame","----------------------------------------");
		//lastFrameRate = 
		frameRate = 1 / deltaTime;
		
		
		/*if (targetFrameRate - 2 > frameRate){
			sleepAdj -= 4;
			if (sleepAdj < -maxDelay)
				sleepAdj = -maxDelay;
		}
		else if(targetFrameRate + 2 < frameRate){
			sleepAdj += 4;
			if (sleepAdj > maxDelay)
				sleepAdj = maxDelay;			
		}*/
		
		if (gcFrameTick++ == GC_FRAME){
			txt.setText(ACEMath.roundf(1 / deltaTime) + "", gl);
			gcFrameTick = 0;
		}
		if (moveFront == 1)
		{			
			player.setMoveFront();			
		}
		else if (moveFront == -1)
		{
			player.setMoveBack();
		}		
		else
			player.setStop();
		
		if (rotateY == 1)
		{
			player.turnLeft(deltaTime);
		}
		else if (rotateY == -1)
		{
			player.turnRight(deltaTime);
		}

		if (haveSensorsValues()){
			//if (SensorManager.getRotationMatrix(R, I, gravity, geomagnetic)){
				SensorManager.getOrientation(getRotationMatrix(), orientationValues);
				if (orientationValues[1] > 0.05){
					player.setAngularSpeed(orientationValues[1]);
					player.turnLeft(deltaTime);
					
				}
					
				else if (orientationValues[1] < -0.05){
					player.setAngularSpeed(orientationValues[1]);
					player.turnRight(deltaTime);
					
				}
				if (orientationValues[2] - getDefRoll() > 0.15)
					player.setMoveFront();
					
				else if (orientationValues[2] - getDefRoll() < -0.15) 
					player.setMoveBack();					
				//super.stopListeners();
			}
		player.makeMovement(deltaTime);
		//Log.d("New Frame", "prepareNewFrmae(" + deltaTime + ")");
		w.prepareNewFrame(deltaTime);
			 
		
		/*try {
			Thread.sleep(maxDelay + sleepAdj);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		/*if (gcFrameTick++ == GC_FRAME){
			System.gc();	
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			gcFrameTick = 0;		
		}
		else{
			try {
				Thread.sleep(15);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/
		
		
		
		
		
	}	
	/**
	 * Override the touch screen listener.
	 * 
	 * React to moves and presses on the touchscreen.
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		
		return true;
		
	}
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		
		if (keyCode == KeyEvent.KEYCODE_DPAD_UP) 
			moveFront = 0;	
		else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) 
			moveFront = 0;
		else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) 
			rotateY = 0;
		else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) 
			rotateY = 0;
			
		return true;
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {		
		
		if (keyCode == KeyEvent.KEYCODE_DPAD_UP) 
		{			
			moveFront = 1;
			//Log.d("upKey", "Up");
		}
		if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) 
		{
			moveFront = -1;
			//Log.d("downKey", "Down");
		}
		if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) 
		{
			rotateY = 1;			
			
		}
		if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) 
		{
			rotateY = -1;			
		}
		if (keyCode == KeyEvent.KEYCODE_W) 
		{
			
			player.getCamera().setPosition(player.getCamera().getPosition().addVector(new Vector(0.0f, 0.1f , -0f)));
		}
		if (keyCode == KeyEvent.KEYCODE_S) 
		{
			
			player.getCamera().setPosition(player.getCamera().getPosition().addVector(new Vector(0.0f, -0.1f , -0f)));
		}
		if (keyCode == KeyEvent.KEYCODE_A) 
		{
			
			player.getCamera().setPosition(player.getCamera().getPosition().addVector(new Vector(-0.1f, 0, -0f)));
		}
		
		if (keyCode == KeyEvent.KEYCODE_D) 
		{
			
			player.getCamera().setPosition(player.getCamera().getPosition().addVector(new Vector(0.1f, 0 , -0f)));
		}
		
		
		return true;
		
	}	
	/*public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}*/

	/*public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		if (event.sensor.getType() == Sensor.TYPE_ORIENTATION)
		{
			
		}
	
	}*/

}


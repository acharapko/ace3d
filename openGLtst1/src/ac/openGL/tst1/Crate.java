package ac.openGL.tst1;


import java.io.IOException;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;
import ac.ace3d.Vector;
import ac.ace3d.meshes.ColliderMesh;
import ac.ace3d.meshes.Mesh;
import ac.ace3d.world.Ace3dObject;
import ac.ace3d.world.Camera;
import ac.ace3d.Orientation;
import android.content.res.Resources;
import android.util.Log;
import ac.ace3d.R;
import ac.ace3d.collisions.GravityCollider;
import ac.ace3d.meshes.TexturedMesh;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Debug;

public class Crate extends Ace3dObject {

	private float scale = 0.5f;
	
	protected Crate(Resources res, Vector pos) {
		super(res, pos);
		//meshes = new ArrayList<MeshFromOBJ>();
		try {
			ArrayList<Mesh>meshs = ac.ace3d.meshes.objReader.processStream(ac.openGL.tst1.R.raw.die, res);
			for (int i=0; i < meshs.size(); i++)
			{
				meshes.add(meshs.get(i));
			}
		} catch (IOException e) {
			e.printStackTrace();    
		}		
		
		
		
	}
	
	public void setTexture(int texturePointer)
	{
		setTexture(texturePointer,1,1);
	}
	
	public void setTextures(int[] texturePointer)
	{
		setTexture(texturePointer,1,1);
	}
	
	public void setTexture(int texturePointer, float repeat_w, float repeat_h)
	{
		if (meshes.get(0) instanceof TexturedMesh)
			((TexturedMesh)meshes.get(0)).setTexture(texturePointer);		
	}
	
	public void setTexture(int[] texturePointer, float repeat_w, float repeat_h)
	{
		if (meshes.get(0) instanceof TexturedMesh)
			((TexturedMesh)meshes.get(0)).setTextures(texturePointer);		
	}
	
	public void setScaling(float newScale)
	{
		scale = newScale;
	}
	public void render(GL10 gl)
	{
		gl.glScalef(scale, scale, scale);
		super.render(gl);
		float rScale = 1 / scale;
		gl.glScalef(rScale, rScale, rScale);
	}

	public void setMovement(float time) {
		// TODO Auto-generated method stub
		
	}
	
	

}

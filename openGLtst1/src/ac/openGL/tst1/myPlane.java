package ac.openGL.tst1;

import java.io.IOException;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.ACE3D;
import ac.ace3d.Vector;
import ac.ace3d.meshes.ColliderMesh;
import ac.ace3d.meshes.Mesh;
import ac.ace3d.meshes.TextureManager;
import ac.ace3d.meshes.TexturedMesh;
import ac.ace3d.world.Ace3dObject;
import ac.ace3d.world.Camera;
import ac.ace3d.Orientation;
import ac.ace3d.collisions.PlaneCollider;
import ac.ace3d.collisions.RectangularPlateCollider;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class myPlane extends Ace3dObject{
	
	//private Mesh m;
	
	
	public myPlane(Resources res, Vector position, Orientation or)
	{
		super(res, position);
		//meshes = new ArrayList<MeshFromOBJ>();
		try { 
			ArrayList<Mesh>meshs = ac.ace3d.meshes.objReader.processStream(R.raw.plane_triangulated, res);
			for (int i=0; i < meshs.size(); i++)
			{
				meshes.add(meshs.get(i));
			}
		} catch (IOException e) {
			e.printStackTrace();    
		}		
		Mesh m = meshes.get(0);
		m.setFaceCulling(ACE3D.ACE3D_CULL_FACE_BACK);
		//Bitmap[] b = new Bitmap[2];
		//b[0] = BitmapFactory.decodeResource(res, R.drawable.bricks1);
		//b[1] = BitmapFactory.decodeResource(res, R.drawable.tile);
		//TextureManager.loadTextureArray(gl, texture, tag, wraps, wrapt)
		//m.setTexture(b);
		//meshes.add(m);
		Orientation or2 = new Orientation(or.getRotationY() - 90, 
				or.getRotationFromHorizont(), or.getRotationArounAxis());
		PlaneCollider pc = new RectangularPlateCollider(this, position, 
				or2, 2, 2);
		colliders.add(pc);
		orientation = or;
		applyOrientation(or);
		//setOrientation(or);		
		//place(position);
	}
	
	 public void setCurrentTextureFrame(int frame)
	   {
			if (meshes.get(0) instanceof TexturedMesh)
				((TexturedMesh)meshes.get(0)).setCurrentTextureFrame(frame);
	   }
	
	/*public void setOrientation(Orientation or)
	{
		super.setOrientation(or);
		or.rotateAroundY(270);
		((RectangularPlateCollider)colliders.get(0)).setOrientation(or);
	}*/
	
	public void setMovement(float time) {
		// TODO Auto-generated method stub
		
	}	
	
	/*public Vector getDirection() {
		
		return new Vector(direction.getiComponent(), direction.getjComponent(), direction.getkComponent());
	}	*/

}

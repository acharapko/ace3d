package ac.openGL.tst1;

import java.io.IOException;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.Vector;
import ac.ace3d.meshes.Mesh;
import ac.ace3d.meshes.TextureManager;
import ac.ace3d.meshes.TexturedMesh;
import ac.ace3d.world.MovingAce3dObject;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class MovingCrate extends MovingAce3dObject{
	
private float scale = 0.5f;
	
	protected MovingCrate(Resources res, Vector pos) {
		super(res, pos);
		//meshes = new ArrayList<MeshFromOBJ>();
		try {
			ArrayList<Mesh>meshs = ac.ace3d.meshes.objReader.processStream(R.raw.die, res);
			for (int i=0; i < meshs.size(); i++)
			{
				meshes.add(meshs.get(i));
			}
		} catch (IOException e) {
			e.printStackTrace();    
		}		
		
		Mesh m = meshes.get(0);
		
	}
	
	public void setScaling(float newScale)
	{
		scale = newScale;
	}
	
	public void render(GL10 gl)
	{
		gl.glScalef(scale, scale, scale);
		super.render(gl);
		float rScale = 1 / scale;
		gl.glScalef(rScale, rScale, rScale);
	}
	
	public void init(GL10 gl)
	{
		Mesh m = meshes.get(0);
		if (m instanceof TexturedMesh){
			Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.crate);	
			((TexturedMesh)m).setTexture(TextureManager.loadTexture(gl, b, "box", GL10.GL_REPEAT, GL10.GL_REPEAT));
		}
	}
	

}

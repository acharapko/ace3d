package ac.openGL.tst1;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.Vector;
import ac.ace3d.collisions.CollisionInfo;
import ac.ace3d.collisions.CollisionInfo.CollisionType;
import ac.ace3d.collisions.GravityDetecter;
import ac.ace3d.lighting.myLight;
import ac.ace3d.meshes.Mesh;
import ac.ace3d.meshes.objReader;
import ac.ace3d.meshes.TextureManager;
import ac.ace3d.scene.Scene;
import ac.ace3d.world.Camera;
import ac.ace3d.world.Floor;
import ac.ace3d.world.MovableAce3dOI.MovingComponent;
import ac.ace3d.Orientation;
import ac.ace3d.world.World;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class gc4 extends Scene{
	
	private long lastframeTime = 0;

	
	private int fr = 0;
	private Box box;
	Floor fl;
	
	public gc4(Context context)
	{
		super(context);
		
	}
	
	/*public void setRenderer(ace3dRenderer rend)
	{
		super.setRenderer(rend);
		ace3dR = rend;
	}*/
		
	public void init(GL10 gl)
	{
		myLight ml = null;
		try {
			ml = new myLight(0);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ace3dR.addLight(ml);
		ace3dR.enableLighting();
		ace3dR.enableLightSource(0);
		
		box = new Box(getResources(),new Vector(0.01f, 5.1f, -15f));
		box.init(gl);
		
		ArrayList<Mesh>meshes;
		meshes = objReader.getObjectWithCollidersFromOBJ(R.raw.simplefloor1, getResources());
		Mesh m = meshes.get(0);		
		//!!!!COLLIDER DOESNT MOVE!
		fl = new Floor(getResources(), new Vector(0.0f, -1.7f, -15f), m);
		Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.tile);	
		fl.setTexture(TextureManager.loadTexture(gl, b, "floor", GL10.GL_REPEAT, GL10.GL_REPEAT));
		fl.setTag("floor");
		
		ace3dR.addRenderable(fl);
		ace3dR.addRenderable(box);
		ace3dR.setSceneController(this);	
		
		
		
	}
	
	public void newFrame(float deltaTime, GL10 gl)
	{		
		
		/*float deltaTime;
		java.util.Date clock  = new java.util.Date();
		long currentTime = clock.getTime();
		if (lastframeTime != 0)
			deltaTime = (currentTime - lastframeTime) / 1000.0f;
		else
			deltaTime = 1.0f;
		lastframeTime = currentTime;*/
		
		box.setMovement(deltaTime);
		//Log.d("box.y", box.getWorldPosition().getjComponent() + "");
		//Log.d("boxGC.y", box.TstMethodGetGCPosition().getjComponent() + "");
		
		//insert detectCollision calls here	
		
		CollisionInfo ci = GravityDetecter.checkGravity(fl, box);
		//CollisionInfo ci = fl.detectCollision(box);
		//Log.d("cDist", ci.getcDist() + "");
		box.processGravity(ci, deltaTime);
		
		if (ci.getType() == CollisionType.NO_COLLISION){
			box.setAcceleration(new Vector(0, -10f, 0), MovingComponent.DUE_TO_GRAVITY);
			Log.d("processGravity", "accelerating due to gravity");
		}
		
		/*mp.setCurrentTextureFrame(fr);
		if (fr == 0)
			fr = 1;
		else
			fr = 0;*/		
		//mproject.move();
		
		
		
	}

	
	/**
	 * Override the touch screen listener.
	 * 
	 * React to moves and presses on the touchscreen.
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {		
		//box.setSpeed(0.5f);
		return true;
		
	}
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		
		
			
		return true;
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {		
		
		
		return true;
		
	}

	/*public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		
	}*/


}
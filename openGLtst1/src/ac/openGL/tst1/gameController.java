package ac.openGL.tst1;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.Vector;
import ac.ace3d.lighting.myLight;
import ac.ace3d.scene.Scene;
import ac.ace3d.world.Camera;
import ac.ace3d.world.WorldA;
import ac.ace3d.Orientation;
import ac.ace3d.world.World;
import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class gameController extends Scene{

	//private Resources res;
	//private ace3dRenderer ace3dR;	
	
	//private Context context;	
	//private long lastframeTime = 0;

	//Mesh p2, p;
	//myPlane mp;
	//SpaceShip ship;
	//Asteroid ast;
	//myProjectile mproject;
	Camera c;
	MyRoom mr;
	World w;
	private int moveFront = 0;	
	private int rotateY = 0;
	
	
	public gameController(Context context)
	{
		super(context);
		

		//res = context.getResources();
	}
	
	/*public void setRenderer(ace3dRenderer rend)
	{
		super.setRenderer(rend);
		ace3dR = rend;
	}*/
		
	public void init(GL10 gl)
	{
		myLight ml = null;
		try {
			ml = new myLight(0);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ace3dR.addLight(ml);
		ace3dR.enableLighting();
		ace3dR.enableLightSource(0);
		
		//Camera c = new Camera();
		/*ArrayList<Mesh>meshes;
		meshes = objReader.getObjectWithCollidersFromOBJ(R.raw.floor1, getResources());
		Mesh m = meshes.get(0);		
		//m.setFaceCulling(ACE3D.ACE3D_CULL_FACE_NONE);
		Floor fl = new Floor(getResources(), new Vector(0, -0.2f, -0.8f), m);
		//m.translate(0, -0.2f, -10f);	
		Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.grasstexture);	
		fl.setTexture(b);
		/ace3dR.addRenderable(fl);*/
		
		c = new Camera();
		c.setPosition(new Vector(2.0f, 0.9f, 3));
		w = new WorldA();
		mr = new MyRoom(getResources(), gl);
		mr.translate(new Vector(-0.0f, -0.00f, -0.0f));		
		//mr.applyCamera(c);
		w.addGroup(mr);
		ace3dR.applyCamera(c);
		ace3dR.addRenderable(w);
		ace3dR.setSceneController(this);
		
		/*Group g = new Group();		
		ArrayList<Mesh>meshes;
		meshes = objReader.getObjectWithCollidersFromOBJ(R.raw.die, getResources());
		Mesh m = meshes.get(0);		
		m.rotateX(90);
		//m.setFaceCulling(ACE3D.ACE3D_CULL_FACE_NONE);
		Floor fl = new Floor(getResources(), new Vector(0.0f, 4.0f, -10f), m);
		//m.translate(0, -0.2f, -10f);	
		Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.die);	
		fl.setTexture(b);
		fl.setTag("floor");
		g.addObject(fl);		
		myProjectile mp = new myProjectile(getResources(), new Vector(0,0,-10f));		
		g.addObject(mp);
		//ace3dR.addRenderable(fl);
		//ace3dR.addRenderable(mp);
		ace3dR.addRenderable(g);*/
		
		
		/*SimplePlane sp = new SimplePlane(3,2);
		Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.bricks1);	
		sp.setTexture(b,3,2);
		ace3dR.addRenderable(sp);
		sp.translateZ(-10);
		sp.rotateY(270);*/
		
		
		/*FlatWall wll = new FlatWall(getResources(), new Vector(0,0,-5), 2, 2);
		wll.setOrientation(new Orientation(200, 0, 0));
		Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.bricks1);	
		wll.setTexture(b, 2, 2);
		ace3dR.addRenderable(wll);*/
		/*myProjectile mp = new myProjectile(getResources(),  new Vector(0,0, -10));
		ace3dR.addRenderable(mp);*/
		
		
		
		//ace3dR.setCamera(c);
		/*Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.flooormap1);	
		float v[] = ac.ace3d.bitmapPlaneReader.getVerticesAdj(b, 2);
		Mesh m = new floorPlane(8, 8, 1, v);
		
		m.translate(-2, -1, -20);
		m.rotateX(90);
		m.setFaceCulling(ACE3D.ACE3D_CULL_FACE_NONE);
		//m.rotateY(180);
		//m.translateZ(-10);
		ace3dR.addRenderable(m);*/
		
		/*mp = new myPlane(getResources(), new Vector(-0.85f, 0, -10), new Orientation(5, 15, 0));
		//mp.setOrientation(new Orientation(55, 0, 0));
		//mp.rotate(new Vector(0, 1, 1), 0);
		mproject = new myProjectile(getResources(), new Vector(0, 0, 1));	
		
		//ship = new SpaceShip(getResources(),  new Vector(0, 0, -20));
		//ship.setOrientation(new Orientation(268, 0, -90));
		//ship.place(new Vector(0, 0, -25));
		//Log.d("ship", "placed");
		
		//ast = new Asteroid(getResources(),  new Vector(0, 0, -20), new Vector(0, 0, 1));
		//ast.setOrientation(new Orientation(268, 0, -90));
		//Vector v = new Vector(1, 2, 0);
		//v.rotateX(135);
		//ship.rotate(180, 0, 0);
		//ship.rotate(new Vector(-1, 0, -1), 180);
		//ship.rotate(0, 180, 0);
		//ship.rotate(0, 0,180);
		ace3dR.addRenderable(mp);
		ace3dR.addRenderable(mproject);
		//ace3dR.addRenderable(ast);
		//ace3dR.addRenderable(ship);
		ace3dR.setSceneController(this);*/
		
		/*ArrayList<Mesh>meshes = new ArrayList<Mesh>();
		try {
			meshes = ac.ace3d.meshes.objReader.processStream(R.raw.floor1, getResources());
		} catch (IOException e) {
			e.printStackTrace();    
		}		
		Mesh m = meshes.get(0);	
		//m.setFaceCulling(ACE3D.ACE3D_CULL_FACE_NONE);
		m.translate(0, -0.2f, -5f);	
		Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.grasstexture);	
		m.setTexture(b);
		m.rotateX(0);
		ace3dR.addRenderable(m);*/
		
		/*ArrayList<Mesh>meshes;
		meshes = objReader.getObjectWithCollidersFromOBJ(R.raw.floor1, getResources());
		Mesh m = meshes.get(0);			
		//Floor fl = new Floor(getResources(), new Vector(0, -0.2f, -5.0f), m);
		m.translate(0, -0.2f, -5f);
		
		Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.grasstexture);	
		m.setTexture(b);
		m.rotateX(0);
		//fl.setTag("floor");
		ace3dR.addRenderable(m);*/
		
		//Mesh m = new ship2();
		//m.setFaceCulling(ACE3D.ACE3D_CULL_FACE_NONE);
		//m.translateZ(-15);
		//m.rotateY(45);
		//m.translateY(2);		
		//m.translateX(1);
		//ace3dR.addRenderable(m);*/
		
		
		
		
		/*ArrayList<Mesh>meshes = new ArrayList<Mesh>();
		try {
			meshes = ac.ace3d.meshes.objReader.processStream(R.raw.die, getResources());
		} catch (IOException e) {
			e.printStackTrace();    
		}		
		Mesh m = meshes.get(0);	
		//m.setFaceCulling(ACE3D.ACE3D_CULL_FACE_NONE);
		m.translate(0, -4.099f, -10.9f);	
		Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.die);	
		m.setTexture(b);
		m.rotateX(0);
		ace3dR.addRenderable(m);
		
		myProjectile mp = new myProjectile(getResources(), new Vector(0,-4.099f,-10.9f));		
		ace3dR.addRenderable(mp);*/
		
	}
	
	public void newFrame(float deltaTime, GL10 gl)
	{
		if (moveFront == 1)
		{
			Vector cp = c.getPosition();
			Orientation o = c.getOrientation();
			Vector dir = new Vector();
			Orientation.getDirectionFormOrientation(o, dir);
			dir = dir.multiplyByScalar(0.2f);
			cp = cp.addVector(dir);
			c.setPosition(cp);
		}
		else if (moveFront == -1)
		{
			Vector cp = c.getPosition();
			Orientation o = c.getOrientation();
			Vector dir = new Vector();
			Orientation.getDirectionFormOrientation(o, dir);
			dir = dir.multiplyByScalar(0.2f);
			cp = cp.subtractVector(dir);
			c.setPosition(cp);
		}
		
		if (rotateY == 1)
		{
			Orientation o = c.getOrientation();
			o.rotateAroundY(-2);
			c.setOrientation(o);
		}
		else if (rotateY == -1)
		{
			Orientation o = c.getOrientation();
			o.rotateAroundY(2);
			c.setOrientation(o);
		}
		
		//w.applyCamera(c);
		//Log.d("NewFrame.c.z", + c.getPosition().getkComponent()+"");
		/*float deltaTime;
		java.util.Date clock  = new java.util.Date();
		long currentTime = clock.getTime();
		if (lastframeTime != 0)
			deltaTime = (currentTime - lastframeTime) / 1000.0f;
		else
			deltaTime = 1.0f;
		lastframeTime = currentTime;
		
		ship.setMovement(deltaTime);
		ast.setMovement(deltaTime);		
		ship.move();
		ast.move();*/
		
		/*Orientation or = ship.getOrientation();
		or.rotateAroundY(1);
		ship.setOrientation(or);*/
		
		/*float deltaTime;
		java.util.Date clock  = new java.util.Date();
		long currentTime = clock.getTime();
		if (lastframeTime != 0)
			deltaTime = (currentTime - lastframeTime) / 1000.0f;
		else
			deltaTime = 1.0f;
		lastframeTime = currentTime;
		
		mproject.setMovement(deltaTime);
		
		//insert detectCollision calls here		
		Vector cp = mp.detectCollision(mproject);
		if (!cp.isZero())
			mproject.setDirection(new Vector(0,0,1));
		mproject.move();
		*/
		
		
	}
	
	/*public void init()
	{
		ArrayList<MeshFromOBJ> msh = new ArrayList<MeshFromOBJ>();
		/*try {
			msh = ac.ace3d.meshes.objReader.processStream(R.raw.icosphere2, res);
		} catch (IOException e) {
			e.printStackTrace();    
		}		
		p = msh.get(0);*/
		
		//p.translateX(-1);
		
		/*msh = new ArrayList<MeshFromOBJ>();
		try {
			msh = ac.ace3d.meshes.objReader.processStream(R.raw.die, getResources());
		} catch (IOException e) {
			e.printStackTrace();    
		}		 
		p2 = msh.get(0); 

		p2.translateX(-0.5f);
		p2.translateZ(z);
		

		// Load the texture.
		p2.setTexture(BitmapFactory.decodeResource(getResources(), R.drawable.die));
		
		myLight l = null;
		try {
			l = new myLight(0);
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		
		ace3dR.addLight(l);
		ace3dR.enableLighting();
		ace3dR.enableLightSource(0);
		//ace3dR.addMesh(p);
		ace3dR.addRenderable(p2);
			
		//ace3dR.allowLocks(true);
		//ace3dR.setSceneLock(sLock);
		ace3dR.setSceneController(this);
	}
		
	public void newFrame()
	{		
		float deltaTime;
		java.util.Date clock  = new java.util.Date();
		long currentTime = clock.getTime();
		if (lastframeTime != 0)
			deltaTime = (currentTime - lastframeTime) / 1000.0f;
		else
			deltaTime = 1.0f;
		lastframeTime = currentTime;
		
		float trr = rotationRate * deltaTime;
		
		float[] orientationValues = new float[3];
		if ((gravity != null) && (geomagnetic != null)){
			//if (SensorManager.getRotationMatrix(R, I, gravity, geomagnetic)){
				SensorManager.getOrientation(getRotationMatrix(), orientationValues);
				if (orientationValues[1] > 0.3)
					tranx -= 0.01;
				else if (orientationValues[1] < -0.3)
					tranx += 0.01;
				
				if (orientationValues[2] - defRoll > 0.3)
					trany += 0.01;
				else if (orientationValues[2] - defRoll < -0.3) 
					trany -= 0.01;
				//super.stopListeners();
			}
		
		
		p2.rotateY(angle += trr);		
		p2.translateZ(z);
		p2.translateX(tranx);
		p2.translateY(trany);
	}*/
	
	
	/**
	 * Override the touch screen listener.
	 * 
	 * React to moves and presses on the touchscreen.
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		/*if (event.getX()> 0 && event.getX() < 200)
			z-=0.1;
		else
			z+=0.1;*/ 
		//mproject.setSpeed(0.5f);
		//ship.setSpeed(0.2f);
		return true;
		
	}
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		
		if (keyCode == KeyEvent.KEYCODE_DPAD_UP) 
			moveFront = 0;	
		else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) 
			moveFront = 0;
		else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) 
			rotateY = 0;
		else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) 
			rotateY = 0;
			
		return true;
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {		
		
		if (keyCode == KeyEvent.KEYCODE_DPAD_UP) 
		{
			/*Vector cp = c.getPosition();
			Log.d("cp.d", cp.getkComponent() + "");
			Orientation o = c.getOrientation();
			Vector dir = Orientation.getDirectionFormOrientation(o);
			dir = dir.multiplyByScalar(0.2f);
			cp = cp.addVector(dir);
			c.setPosition(cp);*/
			moveFront = 1;
			Log.d("upKey", "Up");
		}
		if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) 
		{
			//c.setPosition(c.getPosition().addVector(new Vector(0, 0, 0.1f)));
			moveFront = -1;
			Log.d("downKey", "Down");
		}
		if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) 
		{
			rotateY = 1;
			//Orientation o = c.getOrientation();
			//o.rotateAroundY(-2);
			//c.setOrientation(o);
			//c.setPosition(c.getPosition().addVector(new Vector(-0.1f, 0, -0f)));
			
		}
		if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) 
		{
			rotateY = -1;
			//Orientation o = c.getOrientation();
			//o.rotateAroundY(2);
			//c.setOrientation(o);
		}
		if (keyCode == KeyEvent.KEYCODE_W) 
		{
			
			c.setPosition(c.getPosition().addVector(new Vector(0.0f, 0.1f , -0f)));
		}
		if (keyCode == KeyEvent.KEYCODE_S) 
		{
			
			c.setPosition(c.getPosition().addVector(new Vector(0.0f, -0.1f , -0f)));
		}
		if (keyCode == KeyEvent.KEYCODE_A) 
		{
			
			c.setPosition(c.getPosition().addVector(new Vector(-0.1f, 0, -0f)));
		}
		
		if (keyCode == KeyEvent.KEYCODE_D) 
		{
			
			c.setPosition(c.getPosition().addVector(new Vector(0.1f, 0 , -0f)));
		}
		
		
		return true;
		
	}

	/*public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		
	}*/

}

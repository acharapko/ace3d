package ac.openGL.tst1;

import java.io.IOException;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.ACE3D;
import ac.ace3d.Vector;
import ac.ace3d.collisions.CollisionDetecter;
import ac.ace3d.collisions.CollisionInfo;
import ac.ace3d.collisions.CollisionInfo.CollisionType;
import ac.ace3d.lighting.myLight;
import ac.ace3d.meshes.Mesh;
import ac.ace3d.scene.Scene;
import ac.ace3d.world.Camera;
import ac.ace3d.Orientation;
import ac.ace3d.world.World;
import ac.ace3d.world.MovableAce3dOI.MovingComponent;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class gc3 extends Scene{
	
	private long lastframeTime = 0;

	//Mesh p2, p;
	myPlane mp;
	TriangulatedPlane tp;
	//SpaceShip ship;
	//Asteroid ast;
	myProjectile mproject;
	private int fr = 0;
	
	
	public gc3(Context context)
	{
		super(context);
		
	}
	
	/*public void setRenderer(ace3dRenderer rend)
	{
		super.setRenderer(rend);
		ace3dR = rend;
	}*/
		
	public void init(GL10 gl)
	{
		myLight ml = null;
		try {
			ml = new myLight(0);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ace3dR.addLight(ml);
		ace3dR.enableLighting();
		ace3dR.enableLightSource(0);
		
		tp = new TriangulatedPlane(getResources(), new Vector(0f, 0, -5f), new Orientation(0, 0, 0), gl);
		//mp = new myPlane(getResources(), new Vector(0f, 0, -10f), new Orientation(50, 0, 0));
		mproject = new myProjectile(getResources(), new Vector(-0.0f, 0.45f, -1));	
		mproject.setVelocity(new Vector(0,0,-0.1f), MovingComponent.MOVING_0);
		
		ace3dR.addRenderable(tp);
		ace3dR.addRenderable(mproject);
		
		ace3dR.setSceneController(this);	
		
		/*ArrayList<Mesh> meshes = new ArrayList<Mesh>();
		try {
			ArrayList<Mesh>meshs = ac.ace3d.meshes.objReader.processStream(R.raw.plane_triangulated, getResources());
			for (int i=0; i < meshs.size(); i++)
			{
				meshes.add(meshs.get(i));
			}
		} catch (IOException e) {
			e.printStackTrace();    
		}		
		Mesh m = meshes.get(0);
		m.rotateY(0);
		m.translateZ(-10);
		m.setFaceCulling(ACE3D.ACE3D_CULL_FACE_BACK);
		m.setTexture(BitmapFactory.decodeResource(getResources(), R.drawable.bricks1));
		ace3dR.addRenderable(m);*/
		
	}
	
	public void newFrame(float deltaTime, GL10 gl)
	{		
		/*float deltaTime;
		java.util.Date clock  = new java.util.Date();
		long currentTime = clock.getTime();
		if (lastframeTime != 0)
			deltaTime = (currentTime - lastframeTime) / 1000.0f;
		else
			deltaTime = 1.0f;
		lastframeTime = currentTime;*/
		
		//Log.d("newFrame", "New frame");
		//setting movement, so we know how objects moves in during that frame
		mproject.setMovement(deltaTime);
		//Log.d("newFrame", "deltaTime: " + deltaTime);
		//insert detectCollision calls here		
		CollisionInfo ci = new CollisionInfo();
		CollisionDetecter.detectCollision(tp, mproject, ci);
			//tp.detectCollision(mproject);
		
		if (ci.getType() == CollisionType.POINT_TO_PLANE){
			Log.d("ptp", "Collision: " + mproject.getSpeed());
			mproject.setVelocity(new Vector(0,0,-mproject.getSpeed()), MovingComponent.MOVING_0);
		}
		/*mp.setCurrentTextureFrame(fr);
		if (fr == 0)
			fr = 1;
		else
			fr = 0;*/
		//mproject.move();
		
		
		
	}

	
	/**
	 * Override the touch screen listener.
	 * 
	 * React to moves and presses on the touchscreen.
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {		
		mproject.setSpeed(-0.5f);
		return true;
		
	}
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		return true;
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {		
		
		
		return true;
		
	}

	/*public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		
	}*/


}

package ac.openGL.tst1;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.lighting.myLight;
import ac.ace3d.meshes.Mesh;
import android.content.res.Resources;
import android.graphics.*;
import android.opengl.GLU;
import android.opengl.GLUtils;
import android.opengl.GLSurfaceView.Renderer;

public class OpenGLRenderer implements Renderer {
	
	//private Mesh m;
	private Resources res;
	private float angle, z;
	Bitmap bitmap;
	private Mesh[] m = new Mesh[2];
	private int position = 0;
	myLight ml;
	
	public OpenGLRenderer(Resources r) {
		// Initialize our square. 
		//square = new FlatColoredSquare(0.5f, 0.5f, 1.0f, 1.0f);
		//square2 = new FlatColoredSquare(0.75f, 0.1f, 0.1f, 1.0f);
		//square3 = new SmoothColoredSquare();
		
		res = r;
		/*try{
			ml = new myLight(1);
		}
		catch (Exception e)
		{
			
		}*/
		//bitmap = BitmapFactory.decodeResource(res, R.drawable.die);	
		
		/*ArrayList<MeshFromOBJ> msh = new ArrayList<MeshFromOBJ>();
		try {
			msh = ac.ace3d.meshes.objReader.processStream(R.raw.die, res);
		} catch (IOException e) {
			e.printStackTrace();    
		}
		m = msh.get(0);*/
		angle = 10;
		//SimplePlane m  = new SimplePlane();
		//m.setTexture(bitmap);
		
		
		
		
	}
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * android.opengl.GLSurfaceView.Renderer#onSurfaceCreated(javax.
         * microedition.khronos.opengles.GL10, javax.microedition.khronos.
         * egl.EGLConfig)*/
	 
	
	
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {		
		// Set the background color to black ( rgba ).
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);  // OpenGL docs.
		// Enable Smooth Shading, default not really needed.
		gl.glShadeModel(GL10.GL_FLAT);// OpenGL docs.
		// Depth buffer setup.
		gl.glClearDepthf(1.0f);// OpenGL docs.
		// Enables depth testing.
		gl.glEnable(GL10.GL_DEPTH_TEST);// OpenGL docs.
		//gl.glDepthMask(true);
		// The type of depth testing to do.
		gl.glDepthFunc(GL10.GL_LEQUAL);// OpenGL docs.
		// Really nice perspective calculations.
		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, // OpenGL docs.
                          GL10.GL_NICEST);		
		//enable lighting
		//gl.glEnable(GL10.GL_LIGHTING);
		//gl.glEnable(GL10.GL_COLOR_MATERIAL);	
		
		//setting global ambient light
		//float global_ambient[] = { 0.3f, 0.3f, 0.3f, 1.0f };
		//gl.glLightModelfv(GL10.GL_LIGHT_MODEL_AMBIENT, global_ambient, 0);
		//setLight(gl);
		
	}
	
	public void setLight(GL10 gl)
	{
		gl.glEnable(GL10.GL_LIGHTING);
		FloatBuffer lightAmbientBuffer;
		FloatBuffer lightDiffuseBuffer;
		FloatBuffer lightSpecularBuffer;
		FloatBuffer lightPositionBuffer;
		
		// Create light components
		float ambientLight[] = {0.5f, 0.5f, 0.5f, 1.0f};
		float diffuseLight[] = { 0.8f, 0.8f, 0.8f, 1.0f };
		float specularLight[] = { 0.5f, 0.5f, 0.5f, 1.0f };
		
		
		ByteBuffer byteBuf = ByteBuffer.allocateDirect(ambientLight.length * 4);
		byteBuf.order(ByteOrder.nativeOrder());
		lightAmbientBuffer = byteBuf.asFloatBuffer();
		lightAmbientBuffer.put(ambientLight);
		lightAmbientBuffer.position(0);
		
		byteBuf = ByteBuffer.allocateDirect(specularLight.length * 4);
		byteBuf.order(ByteOrder.nativeOrder());
		lightSpecularBuffer = byteBuf.asFloatBuffer();
		lightSpecularBuffer.put(specularLight);
		lightSpecularBuffer.position(0);
		
		byteBuf = ByteBuffer.allocateDirect(diffuseLight.length * 4);
		byteBuf.order(ByteOrder.nativeOrder());
		lightDiffuseBuffer = byteBuf.asFloatBuffer();
		lightDiffuseBuffer.put(diffuseLight);
		lightDiffuseBuffer.position(0);
		
		float position[] = {-2.0f, 0.0f, 4.0f, 1.0f};
		byteBuf = ByteBuffer.allocateDirect(position.length * 4);
		byteBuf.order(ByteOrder.nativeOrder());
		lightPositionBuffer = byteBuf.asFloatBuffer();
		lightPositionBuffer.put(position);
		lightPositionBuffer.position(0);

		// Assign created components to GL_LIGHT0
		gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_AMBIENT, lightAmbientBuffer);
		gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_DIFFUSE, lightDiffuseBuffer);
		//gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_SPECULAR, lightSpecularBuffer);
		gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, lightPositionBuffer);
		
		gl.glEnable(GL10.GL_LIGHT0);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * android.opengl.GLSurfaceView.Renderer#onDrawFrame(javax.
         * microedition.khronos.opengles.GL10)
	 */
	public void onDrawFrame(GL10 gl) {
		
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		// Replace the current matrix with the identity matrix
		gl.glLoadIdentity();
		
		gl.glTranslatef(0, 0, -7); 	
		gl.glPushMatrix();
		//gl.glEnable(GL10.GL_LIGHTING);
		
		/*gl.glRotatef(angle, 0, 1, 0);
		FloatBuffer lightPositionBuffer;
		float positn[] = {4.0f, 0.0f, 5.0f, 0.0f};
		ByteBuffer byteBuf = ByteBuffer.allocateDirect(positn.length * 4);
		byteBuf.order(ByteOrder.nativeOrder());
		lightPositionBuffer = byteBuf.asFloatBuffer();
		lightPositionBuffer.put(positn);
		lightPositionBuffer.position(0);
		gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, lightPositionBuffer);*/
		
		gl.glPopMatrix();
		
		for (int i = 0; i < position; i++)
			if (m[i] != null)
			{
				
				gl.glPushMatrix();
				//m[i].rotateX(angle);
				m[i].rotateY(angle+=2);
				
				//m[i].rotateZ(angle);
				//m[0].translateZ(z-=0.01);
				m[i].render(gl);
				gl.glPopMatrix();
			}
		//gl.glLoadIdentity();
		//setLight(gl);
		//gl.glPopMatrix();
		//float position[] = { 1.0f, 0f, 1.0f, 1.0f };
		//gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, position,0);
		
		
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * android.opengl.GLSurfaceView.Renderer#onSurfaceChanged(javax.
         * microedition.khronos.opengles.GL10, int, int)
	 */
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		// Sets the current view port to the new size.
		gl.glViewport(0, 0, width, height);// OpenGL docs.
		// Select the projection matrix
		gl.glMatrixMode(GL10.GL_PROJECTION);// OpenGL docs.
		// Reset the projection matrix
		gl.glLoadIdentity();// OpenGL docs.
		// Calculate the aspect ratio of the window
		GLU.gluPerspective(gl, 45.0f,
                                   (float) width / (float) height,
                                   0.1f, 100.0f);
		// Select the modelview matrix
		gl.glMatrixMode(GL10.GL_MODELVIEW);// OpenGL docs.
		// Reset the modelview matrix
		gl.glLoadIdentity();// OpenGL docs.
	}



	public void addMesh(Mesh p) {
		m[position++] = p;		
	}
}





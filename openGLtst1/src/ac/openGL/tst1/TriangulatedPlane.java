package ac.openGL.tst1;

import java.io.IOException;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.ACE3D;
import ac.ace3d.Vector;
import ac.ace3d.collisions.PlaneCollider;
import ac.ace3d.collisions.RectangularPlateCollider;
import ac.ace3d.collisions.TriangularPlateCollider;
import ac.ace3d.meshes.ColliderMesh;
import ac.ace3d.meshes.Mesh;
import ac.ace3d.meshes.TextureManager;
import ac.ace3d.meshes.TexturedMesh;
import ac.ace3d.world.Ace3dObject;
import ac.ace3d.Orientation;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class TriangulatedPlane extends Ace3dObject {	
		//private Mesh m;
		
		
		public TriangulatedPlane(Resources res, Vector position, Orientation or, GL10 gl)
		{
			super(res, position);
			//meshes = new ArrayList<MeshFromOBJ>();
		
			ArrayList<Mesh>meshs = ac.ace3d.meshes.objReader.getObjectWithCollidersFromOBJ(R.raw.plane_triangulated3, res);
			for (int i=0; i < meshs.size(); i++)
			{
				meshes.add(meshs.get(i));
			}
			
			ColliderMesh m = (ColliderMesh) meshes.get(0);
			m.setFaceCulling(ACE3D.ACE3D_CULL_FACE_BACK);	
			Bitmap b = BitmapFactory.decodeResource(res, R.drawable.bricks1);
			m.setTexture(TextureManager.loadTexture(gl, b, "floor", GL10.GL_REPEAT, GL10.GL_REPEAT));
			//m.setTexture(BitmapFactory.decodeResource(res, R.drawable.bricks1));
			//meshes.add(m);
			/*PlaneCollider pc = new RectangularPlateCollider(this, position, 
					or, 2, 2);*/
			
			colliders = m.getColliders(this);
			//colliders.add(pc);
			orientation = or;
			
			Orientation or2 = new Orientation(or.getRotationY() /*- 90*/, 
					or.getRotationFromHorizont(), or.getRotationArounAxis());
			
			((TriangularPlateCollider)colliders.get(0)).changeOrientation(or2);
			//((TriangularPlateCollider)colliders.get(0)).setOrientation(or2);
			//((TriangularPlateCollider)colliders.get(1)).setOrientation(or2);
			
			applyOrientation(or);
			//moveColliders(position);
			
			//setOrientation(or);		
			//place(position);
		}
		
		 public void setCurrentTextureFrame(int frame)
		   {
				if (meshes.get(0) instanceof TexturedMesh)
					((TexturedMesh)meshes.get(0)).setCurrentTextureFrame(frame);
		   }
		
		
		/*public void setOrientation(Orientation or)
		{
			super.setOrientation(or);
			Orientation or2 = new Orientation(or.getRotationY() - 90, 
					or.getRotationFromHorizont(), or.getRotationArounAxis());
			((TriangularPlateCollider)colliders.get(0)).setOrientation(or2);
			//((TriangularPlateCollider)colliders.get(1)).setOrientation(or2);
		}*/
		
		public void setMovement(float time) {
			// TODO Auto-generated method stub
			
		}	
		
		/*public Vector getDirection() {
			
			return new Vector(direction.getiComponent(), direction.getjComponent(), direction.getkComponent());
		}	*/

	}
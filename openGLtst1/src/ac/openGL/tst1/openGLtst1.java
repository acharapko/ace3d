package ac.openGL.tst1;

import java.io.IOException;
import java.util.ArrayList;

import ac.ace3d.ACE3D;
import ac.ace3d.Vector;
import ac.ace3d.collisions.CollisionInfo;
import ac.ace3d.collisions.TriangularPlateCollider;
import ac.ace3d.meshes.Mesh;
import ac.ace3d.renderer.ace3dRenderer;
import ac.ace3d.scene.Scene;
import android.app.Activity;
import android.graphics.BitmapFactory;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

public class openGLtst1 extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);

		// Remove the title bar from the window.
		this.requestWindowFeature(Window.FEATURE_NO_TITLE); 

		// Make the windows into full screen mode.
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		Log.d("Native Heap Size" ,Debug.getNativeHeapSize() / 1024 + " kb:");
		Log.d("Native Heap Allocated Size" ,Debug.getNativeHeapAllocatedSize() / 1024 + " kb:");
		Log.d("Native Heap Free Memory" ,Debug.getNativeHeapFreeSize() / 1024 + " kb:");

		// Create a OpenGL view.
		
		Scene view = new gc2(this);  
		//view.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
		setContentView(view);
		
		// Creating and attaching the renderer.
		
		ace3dRenderer renderer = new ace3dRenderer();
		
		view.setRenderer(renderer);


		//gameController c = new gameController(getResources(), renderer);
		//view.init();
		/*long averageTime = 0; 
		TriangularPlateCollider tpc = 
			new TriangularPlateCollider(null, new Vector(), new Vector(0,1,0),
					new Vector(1,0,0), new Vector(0,0,1));
		Vector ray = new Vector(0, -0.5f, 0);
		Vector rayPos = new Vector(0, 0.8f, 0);
		int j;
		for (j = 0; j < 100; j++){
			java.util.Date clock  = new java.util.Date();
			long oldtime = clock.getTime();
		
			for (int i = 0; i < 1000; i++)
			{
				CollisionInfo cInfo = tpc.traceRay(ray, rayPos);
				float cDist = cInfo.getcDist();
				Vector collisionPoint = cInfo.getCollisionPoint();
			}
			clock  = new java.util.Date();
			long time = clock.getTime() - oldtime;
			//Log.d("time taken", time + "ms");
			averageTime += time;
		}
		averageTime /= j;
		Log.d("Average time taken", averageTime + "ms");*/
		
    }
}
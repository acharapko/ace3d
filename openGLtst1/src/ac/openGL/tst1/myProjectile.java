package ac.openGL.tst1;

import java.io.IOException;
import java.util.ArrayList;
import ac.ace3d.Vector;
import ac.ace3d.meshes.ColliderMesh;
import ac.ace3d.meshes.Mesh;
import ac.ace3d.world.Ace3dObject;
import ac.ace3d.world.Camera;
import ac.ace3d.world.MovingAce3dObject;
import ac.ace3d.collisions.Collidable;
import ac.ace3d.collisions.PointCollider;
import android.content.res.Resources;
import android.util.Log;

public class myProjectile extends MovingAce3dObject implements Collidable{
	
	private Mesh m;
	private float speed = 0; //1 unit per second	
	
	//private float mX, mY, mZ;
	
	public myProjectile(Resources res, Vector position)
	{
		super(res, position);
		//meshes = new ArrayList<MeshFromOBJ>();
		try {
			ArrayList<Mesh>meshs = ac.ace3d.meshes.objReader.processStream(R.raw.projectile, res);
			for (int i=0; i < meshs.size(); i++)
			{
				meshes.add(meshs.get(i));
			}
		} catch (IOException e) {
			e.printStackTrace();    
		}		
		m = meshes.get(0);
		//direction = new Vector(0, 0.00f, -1);
		//position = new Vector(0,0,0);
		PointCollider pc = new PointCollider(this, position);
		colliders.add(pc);
		//place(position);
	}	
	
	/*private void place(float x, float y, float z) {
		m.translateX(x);
		m.translateY(y);
		m.translateZ(z);		
	}
	*/
	
	public float getSpeed()
	{
		return speed;
	}
	
	public void setSpeed(float s)
	{
		Log.d("setSpeed", "setting speed");
		speed = s;
		setVelocity(new Vector(0, 0, s), MovingComponent.MOVING_0);
	}

	public void rotate(float rx, float ry, float rz) {
		m.rotateX(rx);
		m.rotateY(ry);
		m.rotateZ(rx);
		
	}	

	/*public void render(GL10 gl) {
		m.render(gl);
	}*/
	

	/*public void place(Vector position) {
		place(position.getiComponent(), position.getjComponent(), position.getkComponent());
		
	}	*/


}

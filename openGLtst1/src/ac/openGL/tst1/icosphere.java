package ac.openGL.tst1;

import ac.ace3d.ACE3D;
import ac.ace3d.meshes.ColoredMesh;
import ac.ace3d.meshes.Mesh;

public class icosphere extends ColoredMesh {
	
	public icosphere()
	{
		float[] v = {//red
					//-0.276385f, -0.447215f, 0.850640f,
					0.723600f, -0.447215f, 0.525720f, 
					0.000000f, -1.000000f, 0.000000f,   //<==common vertex
					-0.276385f, -0.447215f, 0.850640f,
					//0.723600f, -0.447215f, 0.525720f,
					
					//blue
					0.723600f, -0.447215f, 0.525720f,	
					0.000000f, -1.000000f, 0.000000f, //<==common vertex
					0.723600f, -0.447215f, -0.525720f,
					
					//green ok
					-0.276385f, -0.447215f, 0.850640f, 
					0.000000f, -1.000000f, 0.000000f, //<==common vertex
					-0.894425f, -0.447215f, 0.000000f,
					
					//putple
					-0.894425f, -0.447215f, 0.000000f, 
					0.000000f, -1.000000f, 0.000000f, //<==common vertex
					-0.276385f, -0.447215f, -0.850640f,
					
					//green-blue					
					-0.276385f, -0.447215f, -0.850640f, 
					0.000000f, -1.000000f, 0.000000f,  //<==common vertex
					0.723600f, -0.447215f, -0.525720f,
					
		};
		
		float[] c = {1.0f, 0.0f, 0.0f, 1.0f, //ok
					1.0f, 0.0f, 0.0f, 1.0f,
					1.0f, 0.0f, 0.0f, 1.0f,
				
					0.0f, 1.0f, 0.0f, 1.0f,	//ok
					0.0f, 1.0f, 0.0f, 1.0f,
					0.0f, 1.0f, 0.0f, 1.0f,
				
					0.0f, 0.0f, 1.0f, 1.0f, //ok
					0.0f, 0.0f, 1.0f, 1.0f,
					0.0f, 0.0f, 1.0f, 1.0f,
				
					1.0f, 0.0f, 1.0f, 1.0f, //ok
					1.0f, 0.0f, 1.0f, 1.0f,
					1.0f, 0.0f, 1.0f, 1.0f,
				
					0.0f, 1.0f, 1.0f, 1.0f,
					0.0f, 1.0f, 1.0f, 1.0f,
					0.0f, 1.0f, 1.0f, 1.0f,
				
	};
		super.setColors(c);
		setVertices(v);
		setWinding(ACE3D.ACE3D_CLOCKWISE);

	}

}

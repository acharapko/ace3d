package ac.ace3d.renderer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.Vector;
import ac.ace3d.intQueue;
import ac.ace3d.lighting.LightSource;
import ac.ace3d.meshes.Renderable;
import ac.ace3d.scene.SceneController;
import ac.ace3d.world.Camera;
import ac.ace3d.world.CameraAdjustable;
import ac.ace3d.Orientation;
import android.opengl.GLU;
import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;

public class ace3dRenderer implements Renderer, CameraAdjustable {
	
	private ArrayList<Renderable> meshes;
	private ArrayList<Renderable> nonSceneMeshes;
	private ArrayList<LightSource> lights;
	//private ArrayList<Bitmap> textures;
	private float[] rgba = {0.0f, 0.0f, 0.0f, 0.5f};
	private int shading = GL10.GL_SMOOTH;
	private intQueue lightsEnable = new intQueue();
	
	//private boolean NextFrame = true;
	private boolean isFirstFrame = true;
	private float targetFrameRate = 45f;
	private float upperThresholdFrameRate = 55f;
	
	private boolean isLightingEnabled = false;
	
	private int perspectiveHint = GL10.GL_NICEST;
	
	
	private SceneController sc;
	private Camera cam;
	
	private float[] deltaTimes = new float[3];
	private int dtCounter = 0;
	
	private long lastFrameTime = 0;

	//private long[] frameTimes;
	private Queue<Float> frameTimes;
	//private java.util.Date clock;

	public ace3dRenderer() {
		meshes = new ArrayList<Renderable>();
		nonSceneMeshes = new ArrayList<Renderable>();
		lights = new ArrayList<LightSource>();
		//frameTimes = new long[frameCount];
		frameTimes = new LinkedList<Float>();
		
		
	}
	/**
	 * setClearColor
	 * 
	 * @param color float array of size 4 describing red, green, blue and 
	 * alpha components of color	 *           
	 *            
	 */
	 public void setClearColor(float[] color)
	 {
		 rgba = color;
	 }
	 
	 public void setCamera(Camera c)
	 {
		 cam = c;
	 }
	 
	 /**
	  * setClearColor
	  * 
	  * @param red  red component of color	 * 
	  * @param green  green component of color	 * 
	  * @param blue  blue component of color	 * 
	  * @param alpha  alpha component of color	 *           
	  *            
	  */
	 
	 public void setClearColor(float red, float green, float blue, float alpha)
	 {
		 setClearColor(rgba);
		 rgba[0] = red;
		 rgba[1] = green;
		 rgba[2] = blue;
		 rgba[3] = alpha;
	 }
	 
	 public void setSmoothShading(){
		 shading = GL10.GL_SMOOTH;
	 }
	 
	 public void setFlatShading(){
		 shading = GL10.GL_FLAT;
	 }
	 
	 public void setFastestPerspectiveCalculations()
	 {
		 perspectiveHint = GL10.GL_FASTEST;
	 }
	 public void setNicestPerspectiveCalculations()
	 {
		 perspectiveHint = GL10.GL_NICEST;
	 }
	 public void enableLighting()
	 {
		 isLightingEnabled = true;
	 }
	 
	 public void disableLighting()
	 {
		 isLightingEnabled = false;
	 }
	 
	 public void enableLightSource(int lightID)
	 {
		 lightsEnable.push(lightID);
	 }
	 
	 
	 public synchronized void setSceneController(SceneController scn)
	 {
		 sc = scn;
	 }
	
	
	/**
	 * onSurfaceCreated
	 * 
	 *            
	 *            
	 */
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		// Set the background color
		gl.glClearColor(rgba[0], rgba[1], rgba[2], rgba[3]);  // OpenGL docs.
		// Set shading model
		gl.glShadeModel(shading);// OpenGL docs.
		// Depth buffer setup.
		gl.glClearDepthf(1.0f);// OpenGL docs.
		// Enables depth testing.
		gl.glEnable(GL10.GL_DEPTH_TEST);// OpenGL docs.
		// The type of depth testing to do.
		gl.glDepthMask(true);
		gl.glDepthFunc(GL10.GL_LEQUAL);// OpenGL docs.
		// Really nice perspective calculations.
		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, perspectiveHint);	
		
		
		//enable lighting
		gl.glEnable(GL10.GL_LIGHTING);
		gl.glEnable(GL10.GL_COLOR_MATERIAL);
		
		if (isFirstFrame)
		{
			isFirstFrame = false;
			sc.init(gl);
		}
		
	}


	/**
	 * onDrawFrame
	 * 
	 * @param gl the OpenGL context to render to.	 *           
	 *            
	 */
	public void onDrawFrame(GL10 gl) { //allocates new Date object on every frame
		
		if ( sc != null){			
			
			float cAverageTime = updateAverageFrameTime();
			sc.newFrame(cAverageTime, gl);
		}
			
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		// Replace the current matrix with the identity matrix
		gl.glLoadIdentity();		
		
		//apply or disable light sources, if any
		if (isLightingEnabled){
			gl.glEnable(GL10.GL_LIGHTING);
			gl.glEnable(GL10.GL_COLOR_MATERIAL);	
			for (int i = 0; i < lightsEnable.getSize(); i++ ){
				int enableLighting = lightsEnable.pop();			
				int signum = Integer.signum(enableLighting);
				enableLighting = Math.abs(enableLighting);
				if (enableLighting == 0)
					signum = 1;
				if ((enableLighting >= 0) && (enableLighting < 8) && (signum == 1))
					lights.get(enableLighting).applyLight(gl);
				else if (signum == -1)
					lights.get(enableLighting).disable(gl);
			}
		}
		
		
		gl.glEnable(GL10.GL_DEPTH_TEST);// OpenGL docs.
		// The type of depth testing to do.
		//gl.glDepthMask(true);
		gl.glDepthFunc(GL10.GL_LEQUAL);// OpenGL docs.
	
			
		
		//adjust to camera position
		if (cam != null){
			gl.glPushMatrix();
			Vector pos = cam.getPosition();
			Orientation or = cam.getOrientation();
			gl.glRotatef(or.getRotationY() - 90, 0, 1, 0);
			gl.glTranslatef(-pos.getiComponent(), -pos.getjComponent(), -pos.getkComponent());
			
		}
		
		//draw each mesh
		for (int i = 0; i < meshes.size(); i++){
			gl.glPushMatrix();
			meshes.get(i).render(gl);
			gl.glPopMatrix();
		}
		
		if (cam != null)
			gl.glPopMatrix();
		
		gl.glDisable(GL10.GL_DEPTH_TEST);
		//draw each non scene mesh before camera applies
		for (int i = 0; i < nonSceneMeshes.size(); i++){
			gl.glPushMatrix();
			nonSceneMeshes.get(i).render(gl);
			gl.glPopMatrix();
		}
		
		
		
	}
	
	private float updateAverageFrameTime()
	{
		long currentTime = System.currentTimeMillis();
		
		float currentDeltaTime = currentTime - lastFrameTime;
		float curretnFrameRate = 1.00f / (currentDeltaTime / 1000.0f);
		/*if (curretnFrameRate > upperThresholdFrameRate)
		{
			float tempdt = 1 / targetFrameRate;
			long s = (long) ((tempdt * 1000) - currentDeltaTime);
			//Log.d("sleep", s + ", " + (tempdt * 1000));
			try 
			{
				Thread.sleep(s);
			} 
			catch (InterruptedException e)
			{				
				e.printStackTrace();
			}
			currentTime = System.currentTimeMillis();
		}*/
		
		
		float average = getAverageFrameTime();
		deltaTimes[dtCounter] = (currentTime - lastFrameTime) / 1000.0f;		
		
		/*if (average > deltaTimes[dtCounter] * 5)
		{
			System.gc();
		}
		average = getAverageFrameTime();*/
		
		dtCounter++;
		dtCounter %= deltaTimes.length;
		//Log.d("frame_rate", curretnFrameRate + "");
		lastFrameTime = currentTime;
		
		return currentDeltaTime/1000.f;
		
	}
	
	private float getAverageFrameTime()
	{
		float average = 0.0f;
		for (int i = 0; i < deltaTimes.length; i++)
			average += deltaTimes[i];
		average /= deltaTimes.length;
		return average;
	}
	
	/**
	 * onDrawFrame
	 *				occurs when frame resizes 
	 *
	 * 
	 * @param gl the OpenGL context to render to.	 * 
	 * @param width new width.	 *  
	 * @param height new height.	 *            
	 *            
	 */
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		// Sets the current view port to the new size.
		gl.glViewport(0, 0, width, height);// OpenGL docs.
		// Select the projection matrix
		gl.glMatrixMode(GL10.GL_PROJECTION);// OpenGL docs.
		// Reset the projection matrix
		gl.glLoadIdentity();// OpenGL docs.
		// Calculate the aspect ratio of the window
		GLU.gluPerspective(gl, 45.0f,
                                   (float) width / (float) height,
                                   0.1f, 100.0f);
		
		// Really nice perspective calculations.
		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, perspectiveHint);
		// Select the modelview matrix
		gl.glMatrixMode(GL10.GL_MODELVIEW);// OpenGL docs.
		// Reset the modelview matrix
		gl.glLoadIdentity();// OpenGL docs.
	}



	public void addRenderable(Renderable m) {
		meshes.add(m);
	}
	
	public void addnonSceneRenderable(Renderable m) {
		nonSceneMeshes.add(m);
	}
	
	public void addLight(LightSource l) {
		lights.add(l);
	}

	public void applyCamera(Camera c) {
		cam = c;
		
	}
}



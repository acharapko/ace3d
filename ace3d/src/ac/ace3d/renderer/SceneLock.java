package ac.ace3d.renderer;

import ac.ace3d.scene.SceneController;

public class SceneLock {
	private boolean lock;
	SceneController scntrl;
	
	public SceneLock(SceneController sc)
	{
		scntrl = sc;
	}
	
	public void setLock()
	{
		lock = true;
	}
	public void releaseLock()
	{
		lock = false;
		synchronized (scntrl){
			scntrl.notify();
		}
	}
	
	public boolean isLocked()
	{
		return lock;
	}

}

package ac.ace3d.lighting;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

public class LightSource {
	
	private int sourceInd = -1;
	private float[] clrAmbient = {0.5f,0.5f,0.5f,1.0f};
	private float[] clrDiffuse = {1.0f,1.0f,1.0f,0.5f};
	private float[] clrSpecular = {1.0f,1.0f,1.0f,0.5f};
	private float[] pos = {-1.5f, 1.0f, -4.0f, 1.0f};
	private float[] posdir = {0.0f, 0.0f, 0.0f};
	
	
	private FloatBuffer clrAmbientb = null;
	private FloatBuffer clrDiffuseb = null;
	private FloatBuffer clrSpecularb = null;
	private FloatBuffer posb = null;
	private FloatBuffer posdirb = null;
	
	
	public LightSource(int sourceIndex) throws Exception
	{

		sourceInd = GL10.GL_LIGHT0 + sourceIndex;
		
		if ((sourceIndex < 0)||(sourceIndex > 7))
			throw new Exception("Light source index out of range. OpenGL spports up to 8 light sources");
		this.setAmbientLight(clrAmbient);
		this.setSpecularLight(clrSpecular);
		this.setDiffuseLight(clrDiffuse);
		this.setPosition(pos);
	}
	
	protected void makeDirectional()
	{
		pos[3] = 0;
	}
	
	protected void makePositional()
	{
		pos[3] = 1;
	}
	
	protected void setX(float x)
	{
		pos[0] = x;
	}
	protected void setY(float y)
	{
		pos[1] = y;
	}
	protected void setZ(float z)
	{
		pos[2] = z;
	}
	
	public void applyLight(GL10 gl)
	{
		gl.glEnable(sourceInd);
		
		gl.glLightfv(sourceInd, GL10.GL_AMBIENT, clrAmbientb);
		gl.glLightfv(sourceInd, GL10.GL_DIFFUSE, clrDiffuseb);
		gl.glLightfv(sourceInd, GL10.GL_SPECULAR, clrSpecularb);
		
		gl.glLightfv(sourceInd, GL10.GL_POSITION, posb);
		
		//spot light
		//gl.glLightfv(sourceInd, GL10.GL_SPOT_DIRECTION, posdir, 0);
		//gl.glLightf(sourceInd, GL10.GL_SPOT_CUTOFF, 20); // angle is 0 to 180
		//gl.glLightf(sourceInd, GL10.GL_SPOT_EXPONENT, 2); // exponent is 0 to 128
		
		//gl.glLightf(sourceInd, GL10., GL10.GL_LINEAR_ATTENUATION)
		
	}
	public void disable(GL10 gl)
	{
		gl.glDisable(sourceInd);
	}
	
	
	
	protected void setAmbientLight(float[] light) {
	   	ByteBuffer cbb = ByteBuffer.allocateDirect(light.length * 4);
	   	cbb.order(ByteOrder.nativeOrder());
	   	clrAmbientb = cbb.asFloatBuffer();
	   	clrAmbientb.put(light);
	   	clrAmbientb.position(0);
	}
	
	protected void setDiffuseLight(float[] light) {
	   	ByteBuffer cbb = ByteBuffer.allocateDirect(light.length * 4);
	   	cbb.order(ByteOrder.nativeOrder());
	   	clrDiffuseb = cbb.asFloatBuffer();
	   	clrDiffuseb.put(light);
	   	clrDiffuseb.position(0);
	}

	protected void setSpecularLight(float[] light) {
	   	ByteBuffer cbb = ByteBuffer.allocateDirect(light.length * 4);
	   	cbb.order(ByteOrder.nativeOrder());
	   	clrSpecularb = cbb.asFloatBuffer();
	   	clrSpecularb.put(light);
	   	clrSpecularb.position(0);
	}
	
	protected void setPosition(float[] position) {
	   	ByteBuffer cbb = ByteBuffer.allocateDirect(position.length * 4);
	   	cbb.order(ByteOrder.nativeOrder());
	   	posb = cbb.asFloatBuffer();
	   	posb.put(position);
	   	posb.position(0);
	}



}

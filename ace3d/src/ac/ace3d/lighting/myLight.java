package ac.ace3d.lighting;

public class myLight extends  LightSource {

	public myLight(int sourceIndex) throws Exception {
		super(sourceIndex);
		float[] clrAmbient = {0.3f,0.3f,0.3f,1.0f};
		float[] clrDiffuse = {0.4f,0.5f,0.5f,1.0f};
		float[] clrSpecular = {0.0f,0.0f,0.0f,1.0f};
		setAmbientLight(clrAmbient);
		setDiffuseLight(clrDiffuse);
		setSpecularLight(clrSpecular);
		//super.makeDirectional();
		super.setX(1.0f);
		super.setY(1.0f);
		super.setZ(4.0f);
	}

}

package ac.ace3d;

public class ace3dbytebuffer
	{
		int size;
		int len;
		byte[] b;
		public ace3dbytebuffer()
		{			
			this(128);
		}
		public ace3dbytebuffer(int size)
		{
			this.size = size;
			len = 0;
			b = new byte[size];
		}
		
		public byte[] readBuffer(){
			byte[] b2 = new byte[len];
			for (int i = 0; i < len; i++)
				b2[i]= b[i];
			return b2;
		}
		
		public byte[] readBufferWithNoTrim(){
			return b;
		}
		public int readBuffer(byte[] b)
		{
			b = this.b;
			return len;
		}
		public int getLength(){
			return b.length;
		}
		
		public int getPosition(){
			return len;
		}
		
		public byte get(int index){
			return b[index];
		}
		
		public void appendByte(byte appendByte)
		{
			b[len] = appendByte;
			if (++len == b.length)
			{
				byte[] b2 = new byte[b.length * 2];
				for (int i = 0; i < b.length; i++)
					b2[i] = b[i];
				b = b2;
			}
		}
		
		public void resetBuffer()
		{
			b = new byte[size];
			len = 0;
		}
		
		public void append(byte[] appendBytes)
		{
			for (int i = 0; i < appendBytes.length; i++)
				appendByte(appendBytes[i]);
		}
	}
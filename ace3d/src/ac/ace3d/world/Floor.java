package ac.ace3d.world;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.Vector;
import ac.ace3d.collisions.Collider;
import ac.ace3d.meshes.ColliderMesh;
import ac.ace3d.meshes.Mesh;
import ac.ace3d.meshes.Plane;
import ac.ace3d.meshes.Renderable;
import ac.ace3d.meshes.TexturedMesh;
import android.content.res.Resources;
import android.graphics.Bitmap;

public class Floor extends Ace3dObject{
	
	private float[] fllvl;
	private float scale = 1f;
	private float width, length;
	
	
	public Floor(Resources res, Vector pos)
	{
		super(res, pos, 1);
		Mesh floor = new Plane();
		width = length = 10;
		meshes = new ArrayList<Mesh>();
		meshes.add(floor);
		//super.moveColliders(pos);
		//place(pos);
	}
	
	public Floor(Resources res, Vector pos, Mesh m)
	{
		super(res, pos, 1);
		width = length = 10;
		if (m instanceof ColliderMesh)
			colliders = ((ColliderMesh)m).getColliders(this);
		meshes = new ArrayList<Mesh>();
		meshes.add(m);
		//super.moveColliders(pos);
		//place();
	}
	
	public void addCollider(Collider c)
	{
		colliders.add(c);
	}
	
	public Floor(Resources res, Vector pos, Mesh surface, float width, float length)
	{
		super(res, pos, 1);
		this.width = width;
		this.length = length;
		if (surface instanceof ColliderMesh)
			colliders = ((ColliderMesh)surface).getColliders(this);
		meshes = new ArrayList<Mesh>();
		meshes.add(surface);
		//place();
	}
	
	/*public void setTexture(Bitmap b)
	{
		meshes.get(0).setTexture(b);
		/*float textureCoordinates[] = {0.0f, 1.0f,
                1.0f, 1.0f,
                0.0f, 0.0f,
                1.0f, 0.0f };
		meshes.get(0).setUVmap(textureCoordinates);*/
	/*}*/
	
	public void setTexture(int tPointer)
	{
		if (meshes.get(0) instanceof TexturedMesh)
			((TexturedMesh)meshes.get(0)).setTexture(tPointer);
		/*float textureCoordinates[] = {0.0f, 1.0f,
                1.0f, 1.0f,
                0.0f, 0.0f,
                1.0f, 0.0f };
		meshes.get(0).setUVmap(textureCoordinates);*/
	}
	/**
	 * 
	 * @param x
	 * @param z
	 * @return
	 * 		returns y coordinate of the floor at particular x and z
	 */
	


	public void setMovement(float time) {
		// TODO Auto-generated method stub
		
	}

	
}

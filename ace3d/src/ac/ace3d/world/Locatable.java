package ac.ace3d.world;

import ac.ace3d.Vector;

public interface Locatable {

	public void getWorldPosition(Vector worldPos);
	
	
	
}

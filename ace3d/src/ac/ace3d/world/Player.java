package ac.ace3d.world;

import ac.ace3d.ACEMath;
import ac.ace3d.Orientation;
import ac.ace3d.Vector;
import ac.ace3d.collisions.Collider;
import ac.ace3d.collisions.CollisionInfo;
import ac.ace3d.collisions.PointCollider;
import android.util.Log;

public class Player extends MovingAce3dObject/*extends Camera*/{
	
	private float speed = 0.5f; //units/second
	private float angSpeed = 25f; //degrees / second
	private float maxAngSpeed = 50f; //degrees / second
	private Camera cam;
	private int mv = 0;
	//private float deltaTime = 0;
	private Vector direction = new Vector();//direction of the camera view
	
	private Vector V = new Vector();
	//private Orientation orientation;
	//private Vector worldPosition = new Vector();
	
	public Player()
	{
		this(new Vector());		
	}
	
	public Player(Vector position)
	{
		this(position, new Orientation(90, 0, 0));
	}
	
	public Player(Vector position, Orientation or)
	{
		super(null, position);//careful with null pointer
		cam = new Camera(position, or);
		orientation = or;
		worldPosition = position;		
		Orientation.getDirectionFormOrientation(cam.getOrientation(), direction);
		
		PointCollider pc = new PointCollider(this, new Vector(0,-0.5f,-1.0f));
		colliders.add(pc);
	}
	
	public void setAngularSpeed(float fractionFromMax)
	{
		fractionFromMax = Math.abs(fractionFromMax);
		if (fractionFromMax > 1)
			fractionFromMax = 1;
		angSpeed = fractionFromMax * maxAngSpeed;
	}
	
	public void setSpeed(float speed)
	{
		this.speed = speed;
	}
	
	/*public void setDeltaTime(float dTime)
	{
		deltaTime = dTime;
	}*/
	
	public void setMaxAngSpeed(float angSpeed)
	{
		maxAngSpeed = angSpeed;
	}
	
	public Camera getCamera()
	{
		return cam;
	}
	
	/*public void setPosition(Vector pos)
	{
		worldPosition = pos;
		cam.setPosition(pos);
	}*/
	
	public void processCollision(CollisionInfo ci)
	{ 
		//Vector collisionBounce = getPositionChange().subtractVector(ci.getCollisionVector()).multiplyByScalar(-1);
		//Vector collisionBounce = ci.getShortestDistanceVector().multiplyByScalar(-1);
		//Vector compundPosChange = getPositionChange().addVector(ci.getColliderRotationPosChange());
		
		//Vector collisionBounce = ACEMath.getShortestDistanceToPlane(ci.getPlaneNormal(), compundPosChange).multiplyByScalar(-1);
		//OLD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		Vector collisionBounce = ACEMath.getShortestDistanceToPlane(ci.getPlaneNormal(), getPositionChange()).multiplyByScalar(-1.01f);
		Vector collisionBounceDueToColliderRotation = ACEMath.getShortestDistanceToPlane(ci.getPlaneNormal(), ci.getColliderRotationPosChange()).multiplyByScalar(-1);
		collisionBounce.add(collisionBounceDueToColliderRotation);
		//collisionBounce.multiplySelfByScalar(1.01f);
		Log.d("COLLISION PROCESSING", "collision happened");
		Log.d("COLLISION PROCESSING CollisionVector", ci.getCollisionVector().toString());
		//stop();
		//Vector v = this.getVelocity(MovingComponent.MOVING_0);
		//v.add(collisionBounce);
		//setVelocity(v, MovingComponent.MOVING_0);
		//setMovement(0);
		Log.d("COLLISION PROCESSING Player posChange", getPositionChange().toString());
		////Log.d("COLLISION PROCESSING v0", this.getVelocity(MovingComponent.MOVING_0).multiplyByScalar(ci.getcDist()).toString() );
		this.translate(collisionBounce);
		Log.d("COLLISION PROCESSING collisionBounce", collisionBounce.toString());
		//v.subtract(collisionBounce);
		
	}
	
	private void stop()
	{
		V.resetVector();
		setVelocity(V, MovingComponent.MOVING_0);		
		/*for (Collider c : colliders)
			c.resetRotation();*/		
		cam.setPosition(worldPosition);			
	}
	
	private void stopRotation()
	{
		for (Collider c : colliders)
			c.resetRotation();
	}
	
	public void setStop(){
		mv = 0;
	}
	
	public void setMoveFront(){	
		mv = 1;
	}
	
	public void setMoveBack(){
		mv = -1;
	}
	
	private void moveFront()
	{
		V.multiplyVectorByScalar(direction, speed);
		setVelocity(V, MovingComponent.MOVING_0);
		//Log.d("MoveFRONT",V.toString());
		//cam.setPosition(worldPosition);		
	}
	
	private void moveBack()
	{
		V.multiplyVectorByScalar(direction, -speed);
		setVelocity(V, MovingComponent.MOVING_0);
		//cam.setPosition(worldPosition);		
	}
	
	@Override
	public void placeObject()
	{
		//Log.d("placeObject", "Override1"); 
		super.placeObject();
		cam.setPosition(worldPosition);	
		//Log.d("placeObject", "Override2");
	}
	
	public void turnLeft(float time)
	{
		//orientation is the same for cam and player, so change it only once
		//orientation.rotateAroundY(-angSpeed * time);
		super.changeOrientation(-angSpeed * time, 0, 0);
		Orientation.getDirectionFormOrientation(cam.getOrientation(), direction);
		//stop();
		//turnAndMove(time);
		//cam.setOrientation(orientation);
	}
		
	public void turnRight(float time)
	{
		//orientation is the same for cam and player, so change it only once
		super.changeOrientation(angSpeed * time, 0, 0);		
		Orientation.getDirectionFormOrientation(cam.getOrientation(), direction);
		//stop();
		//turnAndMove(time);
		
		
	}
	
	
	public void makeMovement(float time)
	{
		switch (mv){
		case 0:
			stop();
			break;
		case 1: 
			moveFront();
		    break;
		case -1:
			moveBack();
			break;
		default:
			
		}
	}
	
	
	public void turnUp(float time)
	{
		Orientation c = cam.getOrientation();
		c.rotateAroundHorizontal(angSpeed * time);
	}
	
	public void turnDown(float time)
	{
		Orientation c = cam.getOrientation();
		c.rotateAroundHorizontal(-angSpeed * time);
	}

}

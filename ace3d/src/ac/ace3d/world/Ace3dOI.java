package ac.ace3d.world;

import ac.ace3d.Orientation;
import ac.ace3d.Vector;
import ac.ace3d.meshes.Renderable;

public interface Ace3dOI extends Renderable, Locatable {
	
	/**
	 * 
	 * @return id of the object
	 */
	public int getID();
	
	/**
	 * performs rotation around y, z and x axis. by defaut in that order
	 * 
	 */
	public void rotate(float rx, float ry, float rz);
	
	/** changes the orientation of the object by Orientation or
	 *
	 * 
	 */
	public void changeOrientation(Orientation or);
	
	

}

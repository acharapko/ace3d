package ac.ace3d.world;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.Vector;
import ac.ace3d.collisions.CollisionInfo;
import ac.ace3d.collisions.CollisionInfo.CollisionType;
import ac.ace3d.collisions.GravityDetecter;
import ac.ace3d.meshes.Renderable;
import ac.ace3d.world.MovableAce3dOI.MovingComponent;
/**
 * 
 * @author AC
 * @deprecated
 */
public class Sector implements Renderable, CameraAdjustable, Locatable{
	
	private ArrayList<MovingAce3dObject> Objects = new ArrayList<MovingAce3dObject>();
	private ArrayList<Ace3dObject> StationaryObjects = new ArrayList<Ace3dObject>();
	private float dimX, dimY, dimZ;
	private Vector worldPosition = new Vector(0,0,0);
	private Vector tempObjWorldPosition = new Vector();
	private World myWorld;
	
	public Sector(World w)
	{
		this(new Vector(0,0,0), w);
	}
	
	public Sector(Vector position, World w)
	{
		this(10, 10, 10, position, w);
	}
	
	public Sector(float xDimension, float yDimension, float zDimension, Vector position, World w)
	{
		this.dimX = xDimension;
		this.dimY = yDimension;
		this.dimZ = zDimension;
		translate(position);
		myWorld = w;
		
	}
	
	public Vector getDimensions()
	{
		return new Vector(dimX, dimY, dimZ);
	}
	
	//problem happens when two colliding object are in different sectors( close 
	// to the border of sectors)
	public void checkCollidingObjects()
	{
		//check stationary objects with moving objects
		for (int i = 0; i < StationaryObjects.size(); i++)
			for (int j = 0; j< Objects.size(); j++)
				StationaryObjects.get(i).detectCollision(Objects.get(j));
		
		//check moving objects with moving objects
		for (int i = 0; i < Objects.size(); i++)
			for (int j = i; j < Objects.size(); j++)
				Objects.get(i).detectCollision(Objects.get(j));
			
	}
	
	public void applyGravity(float deltaTime)
	{
		//stationary objects with MovingObjects
		for (int i = 0; i < StationaryObjects.size(); i++)
			for (int j = 0; j < Objects.size(); j++)
				if (Objects.get(j).hasGravity())
				{
					//CollisionInfo ci = StationaryObjects.get(i).checkGravity(Objects.get(j));
					CollisionInfo gci = GravityDetecter.checkGravity(StationaryObjects.get(i), Objects.get(j));	
					Objects.get(j).processGravity(gci, deltaTime);
					
					if (gci.getType() == CollisionType.NO_COLLISION)
						Objects.get(j).setAcceleration(myWorld.getAccelerationDueToGravity(),
								MovingComponent.DUE_TO_GRAVITY);					
				}
		//moving objects with moving objects
		for (int i = 0; i < Objects.size(); i++)
			for (int j = 0; j < Objects.size(); j++)
				if ((Objects.get(j).hasGravity() && (i != j)))
				{
					//CollisionInfo ci = StationaryObjects.get(i).checkGravity(Objects.get(j));
					CollisionInfo gci = GravityDetecter.checkGravity(Objects.get(i), Objects.get(j));	
					Objects.get(j).processGravity(gci, deltaTime);
					
					if (gci.getType() == CollisionType.NO_COLLISION)
						Objects.get(j).setAcceleration(myWorld.getAccelerationDueToGravity(),
								MovingComponent.DUE_TO_GRAVITY);					
				}
		
	}
	
	
	
	private void checkBounds()
	{
		if (myWorld != null)
			for (int i = 0; i< Objects.size(); i++)
			{
				boolean transition = false;
				Objects.get(i).getWorldPosition(tempObjWorldPosition);
				if ((tempObjWorldPosition.getiComponent() > worldPosition.getiComponent() + dimX) ||
						(tempObjWorldPosition.getiComponent() < worldPosition.getiComponent()))
					transition = true;
				else if ((tempObjWorldPosition.getjComponent() > worldPosition.getjComponent() + dimY) ||
						(tempObjWorldPosition.getjComponent() < worldPosition.getjComponent()))
					transition = true;
				else if ((tempObjWorldPosition.getkComponent() > worldPosition.getkComponent() + dimZ) ||
						(tempObjWorldPosition.getkComponent() < worldPosition.getkComponent()))
					transition = true;			
				if(transition)
				{
					//myWorld.objectTransition(Objects.get(i));
					Objects.remove(i);
				}			
			}
	}
	public void addStationaryObject(Ace3dObject obj)
	{
		StationaryObjects.add(obj);
	}
	
	public void addObject(MovingAce3dObject obj)
	{
		Objects.add(obj);
	}
	public void removeStationaryObject(Ace3dObject obj)
	{
		StationaryObjects.remove(obj);
	}
	
	public void removeObject(Ace3dObject obj)
	{
		Objects.remove(obj);
	}
	
	public void render(GL10 gl) {
		// TODO Auto-generated method stub
		checkBounds();
		for (int i = 0; i < Objects.size(); i++){
			gl.glPushMatrix();
			Objects.get(i).render(gl);
			gl.glPopMatrix();
		}
		
		for (int i = 0; i < StationaryObjects.size(); i++){
			gl.glPushMatrix();
			StationaryObjects.get(i).render(gl);
			gl.glPopMatrix();
		}
		
	}
	
	public void translate(Vector v)
	{
		worldPosition = worldPosition.addVector(v);
		for (int i = 0; i < Objects.size(); i++)
			Objects.get(i).translate(v);
		
		for (int i = 0; i < StationaryObjects.size(); i++)
			StationaryObjects.get(i).translate(v);
	}

	
	public void applyCamera(Camera c) {
		//Vector cP = c.getPosition();		
		//Vector v = new Vector(-cP.getiComponent(), -cP.getjComponent(),
		//		-cP.getkComponent());
		//Orientation or = c.getOrientation();
		//this.worldPosition = this.worldPosition.addVector(v); 
		
		for (int i = 0; i < Objects.size(); i++)
			Objects.get(i).applyCamera(c);
		for (int i = 0; i < StationaryObjects.size(); i++)
			StationaryObjects.get(i).applyCamera(c);
		
	}

	
	public void getWorldPosition(Vector worldPos) {
		worldPos.copy(worldPosition);
		//return worldPosition.clone();
		//return worldPosition;
	}
	
	/*public void render(GL10 gl, Camera c) {
		// TODO Auto-generated method stub

		for (int i = 0; i < Objects.size(); i++)
			Objects.get(i).render(gl, c);
		
		for (int i = 0; i < StationaryObjects.size(); i++)
			StationaryObjects.get(i).render(gl, c);
		
	}*/
	

}

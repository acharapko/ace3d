package ac.ace3d.world;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.Vector;
import ac.ace3d.meshes.Renderable;


public abstract class Group{

	private ArrayList<Ace3dObject> StationaryObjects = new ArrayList<Ace3dObject>();
	private ArrayList<MovingAce3dObject> Objects = new ArrayList<MovingAce3dObject>();
	
	
	private Vector worldPosition = new Vector(0,0,0);
	
	public Group(GL10 gl)
	{}
	
	public void addStationaryObject(Ace3dObject obj)
	{
		StationaryObjects.add(obj);
	}
	
	public void addMovingObject(MovingAce3dObject obj)
	{
		Objects.add(obj);
	}
	
	public void removeStationaryObject(Ace3dObject obj)
	{
		StationaryObjects.remove(obj);
	}
	
	public void removeObject(Ace3dObject obj)
	{
		Objects.remove(obj);
	}
	
	public Object[] getMovingObjects()
	{
		return Objects.toArray();		
	}
	
	public Object[] getStationaryObjects()
	{
		return StationaryObjects.toArray();		
	}
	
	public void translate(Vector v)
	{
		worldPosition = worldPosition.addVector(v);
		for (int i = 0; i < Objects.size(); i++)
			Objects.get(i).translate(v);
		
		for (int i = 0; i < StationaryObjects.size(); i++)
			StationaryObjects.get(i).translate(v);
	}
	
	public Vector getWorldPosition() {
		return worldPosition.clone();
	}

}

package ac.ace3d.world;

import ac.ace3d.Vector;

public interface MovableAce3dOI {
	
	public enum MovingComponent {DUE_TO_GRAVITY, MOVING_0, MOVING_1, MOVING_2};
	/**
	 * puts Ace3dObject to the its world position with respect to camera
	 * @param position
	 * 		vector position
	 */
	
	public void placeObject();
	
	/** setMovement()
	 * 
	 *  sets Ace3dObject movement for the next frame according to its 
	 *  velocity vectors and acceleration vectors
	 *  @param time
	 *  	time interval for this movement
	 */
	public void setMovement(float time);
	/**
	 * sets the acceleration of the object. 
	 */
	public void setAcceleration(Vector a, MovingComponent component);	
	
	/**
	 * sets the velocity of the object. 
	 */
	public void setVelocity(Vector v, MovingComponent component);	
	
	
	/**
	 * gets velocity component
	 */
	public Vector getVelocity(MovingComponent component);
	
	/**
	 * gets acceleration component
	 */
	public Vector getAcceleration(MovingComponent component);
	
	/**
	 * 
	 * @return the change in position during the current frame.
	 */	
	public Vector getPositionChange();

}

package ac.ace3d.world;

import ac.ace3d.Orientation;
import ac.ace3d.Vector;
import ac.ace3d.collisions.RectangularPlateCollider;
import ac.ace3d.meshes.SimplePlane;
import android.content.res.Resources;
import android.graphics.Bitmap;

public class FlatWall extends Wall {

	public FlatWall(Resources res, Vector pos) {
		this(res, pos, 2,2);		
	}
	
	public FlatWall(Resources res, Vector pos, float width, float height)
	{
		super(res, pos, new SimplePlane(width, height), width, height);

		RectangularPlateCollider rpc = new RectangularPlateCollider(this, new Vector(), 
				new Orientation(0,0,0), width + 0.04f, height + 0.04f);
		addCollider(rpc);
		
	}
	/**
	 * @deprecated
	 * @param b
	 */
	public void setTexture(Bitmap b)
	{
		setTexture(b,1,1);
	}
	/**
	 * @deprecated
	 * @param b
	 * @param repeat_w
	 * @param repeat_h
	 */
	public void setTexture(Bitmap b, float repeat_w, float repeat_h)
	{
		//textureManager.
		if (meshes != null)
			if (meshes.size() == 1)				
				((SimplePlane)meshes.get(0)).setTexture(b, repeat_w, repeat_h);
	}
	
	/**
	 * 
	 * @param b
	 */
	public void setTexture(int tPointer)
	{
		setTexture(tPointer,1,1);
	}
	/**
	 * 
	 * @param b
	 * @param repeat_w
	 * @param repeat_h
	 */
	public void setTexture(int tPointer, float repeat_w, float repeat_h)
	{
		//textureManager.
		if (meshes != null)
			if (meshes.size() == 1)				
				((SimplePlane)meshes.get(0)).setTexture(tPointer, repeat_w, repeat_h);
	}
	
	
	
}

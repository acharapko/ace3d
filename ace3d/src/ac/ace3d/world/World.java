package ac.ace3d.world;

import ac.ace3d.Vector;
import ac.ace3d.meshes.Renderable;

public interface World extends Renderable{
	
	public void addGroup(Group p);
	
	public void addObject(Ace3dObject obj);
	
	public void addMovingObject(MovingAce3dObject obj);
	
	public void setVisionRadius(float r);
	
	public void prepareNewFrame(float deltaTime, Vector pos);
	
	public void prepareNewFrame(float deltaTime);
	
	public void setAccelerationDueToGravity(Vector accelerationDueToGravity);
	
	public Vector getAccelerationDueToGravity();
	

}

package ac.ace3d.world;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.Orientation;
import ac.ace3d.Vector;
import ac.ace3d.collisions.*;
import ac.ace3d.collisions.CollisionInfo.CollisionType;
import ac.ace3d.meshes.Mesh;
import android.content.res.Resources;
import android.util.Log;

public abstract class Ace3dObject implements Ace3dOI, Collidable {

	private Resources res;
	protected ArrayList<Mesh> meshes; //meshes of this object
	protected ArrayList<Collider> colliders; //colliders of this object	
	//protected ArrayList<GravityCollider> gravityColliders;
	protected Vector worldPosition;
	protected Vector tempWorldPositionofOtherObj = new Vector();
	//protected Vector direction;
	protected Orientation orientation = new Orientation(0,0,0);;
	
	//private static final Vector posChangeVector = new Vector();
	
	
	private float maxDimension = 2;
	private float collisionRadius = 0.0f;
	
	private Vector camPosAdj = new Vector(0,0,0);
	private Orientation camOrAdj = new Orientation(0,0,0);
	
	private String tag = "";
	private static int lastID;
	private int id;
	
	private Vector position;
	
	/*protected Ace3dObject(Resources res)
	{
		this.res = res;
	}*/	
	protected Ace3dObject(Resources res, Vector pos, Orientation or)
	{
		//sumdD = new Vector();
		//dD = new Vector();
		position = new Vector();
		worldPosition = pos;
		this.res = res;
		orientation = or;
		colliders = new ArrayList<Collider>();
		
		id = ++lastID;
		meshes = new ArrayList<Mesh>();
		//place();
		applyOrientation(or);
	}	
	
	protected Ace3dObject(Resources res, Vector pos, Orientation or, float maxDimension)
	{
		this(res, pos, or);		
		this.maxDimension = maxDimension;
	}	
	
	protected Ace3dObject(Resources res, Vector pos, float maxDimension)
	{
		this(res, pos, new Orientation(0,0,0), maxDimension);
		
	}
	
	protected Ace3dObject(Resources res, Vector pos)
	{
		this(res, pos, new Orientation(0,0,0));
	}	
		
	public int getID()
	{
		return id;
	}
	
	public String getTag()
	{
		return tag;
	}
	
	public void setTag(String tag)
	{
		this.tag =  tag;
	}

	
	public Resources getResources()
	{
		return res;
	}
	@SuppressWarnings("unchecked")
	public ArrayList<Collider> getColliders()
	{
		return colliders;
	}	
	
	
	/**
	 * returns an empty vector since this is a stationary object
	 */
	public Vector getPositionChange() {
		//return new Vector();
		return Vector.getBlancVector();
	}
	/**sets the change in  position for the current frame
	 * 
	 */
	/*public void changePosition(Vector dPos)
	{
		dD.add(dPos);
	}*/
	
	public Orientation getOrientation()
	{
		return orientation;
	}
	
	public void setMaxDimension(float maxDimension)
	{
		this.maxDimension = maxDimension;
	}
	/**
	 * 
	 * @return returns max dimension of the objects, since the object is stationary, such dimension
	 * object's no collision radius
	 */
	public float getNoCollisionRadius()
	{
		return maxDimension;
	}
	
	public float getCollisionRadius()
	{
		return collisionRadius;
	}	
	
	/*public void setDirection(Vector direction)
	{
		this.direction = direction; 
	}*/
	/*public void setDisplacementChange(Vector dD)
	{
		this.dD = dD;
	}*/
	
	/**
	 * @return
	 * copy of direction vector;
	 */
	/*public Vector getDirection() {
		
		return direction.clone();
	}*/
	
	public void applyCamera(Camera c)
	{
		Vector cP = c.getPosition();
		
		//camPosAdj = new Vector(-cP.getiComponent(), -cP.getjComponent(), 
		//			-cP.getkComponent());
		
		camPosAdj.resetVector(-cP.getiComponent(), -cP.getjComponent(), 
				-cP.getkComponent());
		
		Orientation or = c.getOrientation();
		
		/*camOrAdj = new Orientation(-or.getRotationY(), -or.getRotationFromHorizont(),
				-or.getRotationArounAxis());*/
		
		camOrAdj.resetOrientation(-or.getRotationY(), -or.getRotationFromHorizont(),
				-or.getRotationArounAxis());
		
		/*place(position.getiComponent() + camPosAdj.getiComponent(), 
				position.getjComponent() + camPosAdj.getjComponent(),
				position.getkComponent() + camPosAdj.getkComponent());	*/
	}	
	/**
	 * renders all meshes of the object
	 */
	public void render(GL10 gl) {
		//adjust to camera location
		/*Log.d("aceObj.camPosAdj.x", camPosAdj.getiComponent() + "");
		Log.d("aceObj.camPosAdj.y", camPosAdj.getjComponent() + "");
		Log.d("aceObj.camPosAdj.z", camPosAdj.getkComponent() + "");
		Log.d("aceObj.worldPosition.x", worldPosition.getiComponent() + "");
		Log.d("aceObj.worldPosition.y", worldPosition.getjComponent() + "");
		Log.d("aceObj.worldPosition.z", worldPosition.getkComponent() + "");*/
		//add movement for this frame
		/*worldPosition = worldPosition.addVector(dD);
		dD = new Vector();*/
		//adjust to camera position
		//translation
		//place();
		
		
		
		
		
		position.sumTwoVectors(worldPosition, camPosAdj);		
		place(position.getiComponent(), position.getjComponent(), position.getkComponent());
		
		
		
		
		
		//rotation
		/*float raaY = orientation.getRotationY() + (camOrAdj.getRotationY() + 270);
		float raaZ = orientation.getRotationFromHorizont() + camOrAdj.getRotationFromHorizont();
		
		//x axis of the object rotated after the call to rotateY. 
		//rotateY sets the rotation around Y axis, which causes X and Z axis to rotate
		//so we need to compensate for this rotation
		
		for(int i = 0; i < meshes.size(); i++)
		{
			Mesh m = meshes.get(i);
			//x axis of the object rotated after the call to rotateY. 
			//rotateY sets the rotation around Y axis, which causes X and Z axis to rotate
			//so we need to compensate for this rotation
			m.rotateY(raaY);			
			m.rotateZ(raaZ);
			Log.d("setOrientation.raaZ", raaZ + "");
			m.rotateX(orientation.getRotationArounAxis() + camOrAdj.getRotationArounAxis()); //rotation along objects length (x-axis)rotates around  its direction
		}*/
		//Log.d("ACEOBJECT", getTag());
		//for (Collider c : colliders)
			//c.applyRotation();
		
		for(int i = 0; i < meshes.size(); i++)
			meshes.get(i).render(gl);
	}
	
	/*public void render(GL10 gl, Camera c) {
		for(int i = 0; i < meshes.size(); i++)
			meshes.get(i).render(gl, c);
	}*/
	
	public void rotate(float rx, float ry, float rz)
	{
		for(int i = 0; i < meshes.size(); i++)
		{
			Mesh m = meshes.get(i);
			m.rotateX(rx);
			m.rotateY(ry);
			m.rotateZ(rz);
		}
	}
	/**
	 * adds y, Horizon and axis components to the current Orientation;
	 * @param y
	 * @param horizont
	 * @param axis
	 */
	public void changeOrientation(float y, float horizont, float axis)
	{
		orientation.rotateAroundY(y);
		orientation.rotateAroundHorizontal(horizont);
		orientation.rotateAroundAxis(axis);
		
		for (int i = 0; i < colliders.size(); i++)
		{
			Collider c = colliders.get(0);
			if (c instanceof PlaneCollider)
				((PlaneCollider)c).changeOrientation(y, horizont, axis);
			//change position of colliders according to objects rotation
			c.setRotation(y, horizont);
			
		}
		applyOrientation(orientation);
		
	}
	
	protected void applyOrientation(Orientation or){
		float raaY = or.getRotationY()/* + camOrAdj.getRotationY()*/;
		float raaZ = or.getRotationFromHorizont() /*+ camOrAdj.getRotationFromHorizont()*/;
		
		//x axis of the object rotated after the call to rotateY. 
		//rotateY sets the rotation around Y axis, which causes X and Z axis to rotate
		//so we need to compensate for this rotation
		
		for(int i = 0; i < meshes.size(); i++)
		{
			Mesh m = meshes.get(i);
			//x axis of the object rotated after the call to rotateY. 
			//rotateY sets the rotation around Y axis, which causes X and Z axis to rotate
			//so we need to compensate for this rotation
			m.rotateY(raaY);
			/*if ((raaY > 90) && (raaY < 270))
				raaZ = 180 - raaZ;*/
			m.rotateZ(raaZ);
			Log.d("setOrientation.raaZ", raaZ + "");
			m.rotateX(or.getRotationArounAxis()); /*+ camOrAdj.getRotationArounAxis());*/ //rotation along objects length (x-axis)rotates around  its direction
		}
	}
		
	
	
	public void changeOrientation(Orientation or)
	{
		
		//orientation = orientation.addOrientation(or);
		orientation.addToThisOrientation(or);
		applyOrientation(orientation);
		
		for (int i = 0; i < colliders.size(); i++)
		{
			Collider c = colliders.get(0);
			if (c instanceof PlaneCollider)
				((PlaneCollider)c).changeOrientation(or);
		}
	}
	/**
	 * @deprecated
	 * does nothing
	 */
	public void moveColliders()
	{
		
	}
	 /**
	  * @deprecated
	  * @param object
	  * @param ci
	  */	
	public CollisionInfo detectCollision(Collidable object) {
		CollisionInfo ci = new CollisionInfo();
		detectCollision(object, ci);
		return ci;
	}
 /**
  * @deprecated
  * @param object
  * @param ci
  */
	public void detectCollision(Collidable object, CollisionInfo ci) {
		
		//CollisionInfo ci = new CollisionInfo();
		//if object is of type Ace3dObject than it has noCollisionRadius
		//normally all Collidable passed to this method objects will be Ace3dObject
		
		if (object instanceof Ace3dObject)
		{
			float ncr = ((Ace3dObject)object).getNoCollisionRadius();
			
			//Vector pos = object.getWorldPosition();
			object.getWorldPosition(tempWorldPositionofOtherObj);
			float distance = (worldPosition.subtractVector(tempWorldPositionofOtherObj)).getLength();
			
			Log.d("ACE3d.detectCollision.distance", distance + "");
			Log.d("ACE3d.detectCollision.ncr_x2", (ncr + maxDimension) + "");
			
			//if distance between objects is less then sum of noCollisionRadii, 
			//then collision is possible
			if (distance < (ncr + maxDimension)) 
			{
				float cr = ((Ace3dObject)object).getCollisionRadius();
				//two object are close enough for us not to check colliders
				if ((distance < (cr + collisionRadius)) && (cr > 0) && (collisionRadius > 0)) 
				{							
						ci = new CollisionInfo(tempWorldPositionofOtherObj, 0.001f);					
				}
				else{
				//since two objects are close, place colliders to the right spot
					moveColliders();					
					((Ace3dObject)object).moveColliders();
					//check for collision from all colliders of the Ace3dObject
					ArrayList<Collider> oCldrs = ((Ace3dObject)object).getColliders();
					for (int i = 0; i < colliders.size(); i++)
						for (int j = 0; j < oCldrs.size(); j++)
						{
							Collider cl = oCldrs.get(j); 
							//CollisionDetecter.detectCollision(cl, colliders.get(i), ci);
							if (ci.getType() != CollisionType.NO_COLLISION){
								i = colliders.size();
								j = oCldrs.size();
							}
						}
				}					
			}
			
		}
		else
		{
			
		}
		//return ci;
	}	
	
	public void processCollision(CollisionInfo ci)
	{
		
	}
	
	/*public CollisionInfo checkGravity(Collidable object) {
		//default CollisionInfo with no collision flag
		CollisionInfo ci = new CollisionInfo();
		
		Vector pos = object.getWorldPosition();
		float distance = (worldPosition.subtractVector(pos)).getLength();
		
		if (object instanceof MovingAce3dObject)
			if (((MovingAce3dObject)object).getNoCollisionRadius()  > distance){
				
				moveColliders();					
				((MovingAce3dObject)object).moveColliders();
		
				//check for collision from all colliders of the Ace3dObject
				ArrayList<GravityCollider> oCldrs = ((MovingAce3dObject)object).getGravityColliders();
				for (int i = 0; i < colliders.size(); i++)
					for (int j = 0; j < oCldrs.size(); j++)
					{
						Collider cl = oCldrs.get(j); 
						ci = colliders.get(i).detectCollision(cl);
						if (ci.getType() != CollisionType.NO_COLLISION){
							i = colliders.size();
							j = oCldrs.size();
						}
					}
			}
		return ci;
	}	*/
	
	/**
	 * @return a world position vector
	 */

	public void getWorldPosition(Vector worldPos) {
		worldPos.copy(worldPosition);
		//return worldPosition.clone();
		//return worldPosition;
	}
	

	
	
	/**
	 * is it correct?
	 * @param x
	 * @param y
	 * @param z
	 */
	private void place(float x, float y, float z) {
		for(int i = 0; i < meshes.size(); i++)
		{
			//Mesh m = meshes.get(i);
			meshes.get(i).translateX(x);
			meshes.get(i).translateY(y);
			meshes.get(i).translateZ(z);	
		}
	}
	
	/*public void place(Vector position) {
		place(position.getiComponent(), position.getjComponent(), position.getkComponent());		
	}	*/
	
	
	/**
	 * Not recommended to use.
	 * sets position of the object Vector pos
	 */
	/*public void setWorldPosition(Vector pos)
	{
		sumdD = pos.subtractVector(worldPosition);
		worldPosition = pos;
		recalculateColliderPositions();
		
	}
	/**
	 * Not recommended to use.
	 * changes position of the object by Vector pos
	 */
	/*public void changeWorldPosition(Vector pos)
	{
		sumdD = sumdD.addVector(pos);
		dD = new Vector(pos.getiComponent(), pos.getjComponent(),
				pos.getkComponent());	
	}*/
	
	public void translate(Vector dPosition) {
		worldPosition.add(dPosition);
		//moving colliders to the right spot
		//for (int i = 0; i < colliders.size(); i++)
			//colliders.get(i).changeWorldPosition(dPosition);
		/*for (int i = 0; i < gravityColliders.size(); i++)
			gravityColliders.get(i).changeWorldPosition(dPosition);*/
		//sumdD = sumdD.addVector(dPosition);
		//place(position.getiComponent(), position.getjComponent(), position.getkComponent());		
	}	

}

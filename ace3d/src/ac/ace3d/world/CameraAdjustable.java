package ac.ace3d.world;

public interface CameraAdjustable {
	
	public void applyCamera(Camera c);

}

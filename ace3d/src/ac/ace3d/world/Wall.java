package ac.ace3d.world;

import java.util.ArrayList;

import ac.ace3d.Vector;
import ac.ace3d.collisions.Collidable;
import ac.ace3d.collisions.Collider;
import ac.ace3d.collisions.CollisionInfo;
import ac.ace3d.meshes.ColliderMesh;
import ac.ace3d.meshes.Mesh;
import android.content.res.Resources;

public class Wall extends Ace3dObject {

	
	private float[] fllvl;
	private float scale = 1f;
	private float width, length;
	
	protected Wall(Resources res, Vector pos) {
		super(res, pos);
		// TODO Auto-generated constructor stub
	}
	
	public Wall(Resources res, Vector pos, ColliderMesh surface)
	{
		super(res, pos, 10);
		
		width = length = 10;
		colliders = surface.getColliders(this);
		meshes = new ArrayList<Mesh>();
		meshes.add(surface);
		//this.
		//place(pos);
	}
	
	public Wall(Resources res, Vector pos, ColliderMesh surface, float width, float length)
	{
		super(res, pos, (width > length ? width: length));

		this.width = width;
		this.length = length;
		colliders = surface.getColliders(this);
		meshes = new ArrayList<Mesh>();
		meshes.add(surface);
		//place(pos);
	}
	public Wall(Resources res, Vector pos, Mesh surface, float width, float length)
	{
		super(res, pos, (width > length ? width: length));

		this.width = width;
		this.length = length;
		meshes = new ArrayList<Mesh>();
		meshes.add(surface);
		//place(pos);
	}
	
	
	public Wall(Resources res, Vector pos, Mesh surface, Collider c, float width, float length)
	{
		super(res, pos, 1);

		this.width = width;
		this.length = length;
		colliders.add(c);
		meshes = new ArrayList<Mesh>();
		meshes.add(surface);
		//place(pos);
	}
	
	public void addCollider(Collider c)
	{
		colliders.add(c);
	}

	
	public void setMovement(float time) {
		// TODO Auto-generated method stub
		
	}


}

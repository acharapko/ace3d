package ac.ace3d.world;

import ac.ace3d.Orientation;
import ac.ace3d.Vector;

public class Camera {

	private Vector position;
	private Orientation orientation;
	private float spin = 0;
	
	public Camera()
	{
		this(new Vector(0,1,0), new Orientation(90, 0, 0));
	}
	
	public Camera(Vector pos, Orientation or)
	{
		position = pos;
		orientation = or;
		
	}
	
	public Vector getPosition()
	{
		return position;
	}
	
	public void setPosition(Vector pos)
	{
		position = pos;
	}

	public void setOrientation(Orientation or) {
		orientation = or;
	}

	public Orientation getOrientation() {
		return orientation;
	}
}

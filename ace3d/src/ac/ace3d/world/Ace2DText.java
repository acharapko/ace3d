package ac.ace3d.world;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.Vector;
import ac.ace3d.meshes.TextureManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.opengl.GLUtils;
import android.util.Log;

public class Ace2DText extends Sprite{

	private int swidth, sheight;
	private String text = "";
	private Bitmap background;
	private int textX, textY;
	
	public Ace2DText(Resources res, int width, int height, int textX, int textY) {
		this(res, new Vector(0, 0, -3.10f), width, height, textX, textY);		
	}	
	
	public Ace2DText(Resources res, Vector pos, int width, int height, int textX, int textY) {
		super(res, pos);	
		swidth = width;
		sheight = height;
		this.textX = textX;
		this.textY = textY;
	}	
	
		
	public void setBackground(Bitmap background)
	{
		this.background = background;
	}
	
	public void setText(String txt, GL10 gl)
	{
		text = txt;
		int tSize = (swidth > sheight) ? swidth : sheight;
		
		Bitmap bitmap = Bitmap.createBitmap(tSize, tSize, Bitmap.Config.ARGB_4444);
		
		bitmap.eraseColor(Color.TRANSPARENT);
		
		// get a canvas to paint over the bitmap
		Canvas canvas = new Canvas(bitmap);			
		Paint textPaint = new Paint();
		textPaint.setTextSize(32);
		textPaint.setAntiAlias(true);
		textPaint.setARGB(255, 0,0,200);
		
		if (background != null)
			canvas.drawBitmap(background, 0, 0, textPaint);
		// draw the text centered
		canvas.drawText(text, 16,112, textPaint);
		int texturePointer = TextureManager.loadTexture(gl, bitmap, "text", GL10.GL_REPEAT, GL10.GL_REPEAT);		
		super.setTexture(texturePointer, 1, 1);
		
		
	}
	
}

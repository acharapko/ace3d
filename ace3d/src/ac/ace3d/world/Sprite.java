package ac.ace3d.world;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.Orientation;
import ac.ace3d.Vector;
import ac.ace3d.meshes.Mesh;
import ac.ace3d.meshes.SimplePlane;
import ac.ace3d.meshes.TextureManager;
import android.content.res.Resources;
import android.graphics.Bitmap;

public class Sprite extends Ace3dObject{

	SimplePlane sp;
	
	public Sprite(Resources res, Vector pos) {
		this(res, pos, 1, 1);		
	}
	
	public Sprite(Resources res, Vector pos, float width, float height) {		
		super(res, pos);
		sp = new SimplePlane(width, height);
		orientation = new Orientation(0,0,0);
		meshes.add(sp);			
	}	
	
	public void setCurrentFrame(int frameID)
	{
		sp.setCurrentTextureFrame(frameID);
	}
	
	public void setTexture(int texturePointer)
	{
		setTexture(texturePointer,1,1);
	}
	
	public void setTextures(int[] texturePointer)
	{
		setTexture(texturePointer,1,1);
	}
	
	public void setTexture(int texturePointer, float repeat_w, float repeat_h)
	{
		sp.setTexture(texturePointer, repeat_w, repeat_h);		
	}
	
	public void setTexture(int[] texturePointer, float repeat_w, float repeat_h)
	{
		sp.setTexture(texturePointer, repeat_w, repeat_h);		
	}
	
	/**
	 * @deprecated
	 * @param b
	 */
	public void setTexture(Bitmap b)
	{
		setTexture(b,1,1);
	}
	/**
	 * @deprecated
	 * @param b
	 * @param repeat_w
	 * @param repeat_h
	 */
	public void setTexture(Bitmap b, float repeat_w, float repeat_h)
	{
		sp.setTexture(b, repeat_w, repeat_h);		
	}

	public void render(GL10 gl)
	{		
		//blend in the background color
		gl.glEnable(GL10.GL_BLEND);	//Enable blending
		//gl.glDisable(GL10.GL_DEPTH_TEST);				
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		//sp.render(gl);
		super.render(gl);
		gl.glDisable(GL10.GL_BLEND);		
		
	}
	//new SimplePlane(width, height)
	public void setMovement(float time) {
		// TODO Auto-generated method stub
		
	}

}

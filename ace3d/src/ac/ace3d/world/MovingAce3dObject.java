package ac.ace3d.world;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.Orientation;
import ac.ace3d.Vector;
import ac.ace3d.collisions.Collider;
import ac.ace3d.collisions.CollisionInfo;
import ac.ace3d.collisions.Gravity;
import ac.ace3d.collisions.GravityCollider;
import ac.ace3d.collisions.CollisionInfo.CollisionType;
import ac.ace3d.meshes.Mesh;
import ac.ace3d.world.MovableAce3dOI.MovingComponent;
import android.content.res.Resources;
import android.util.Log;

public abstract class MovingAce3dObject extends Ace3dObject implements MovableAce3dOI{

	
	private Vector[] velocity = new Vector[4];
	
	private Vector[] acceleration = new Vector[4];
	protected ArrayList<GravityCollider> gravityColliders; //gravityColliders of this object
	
	private Vector dD, sumdD; 
	
	Vector tempAccel = new Vector();
	Vector tempVelocity = new Vector();
	private float previoiusFrameTime = 0.0f;
	
	public MovingAce3dObject(Resources res, Vector pos) {
		this(res, pos, new Orientation(0,0,0));
		
	}
	protected MovingAce3dObject(Resources res, Vector pos, Orientation or)
	{
		super(res, pos, or);
		init();
	}	
	
	protected MovingAce3dObject(Resources res, Vector pos, Orientation or, float maxDimension)
	{
		super(res, pos, or, maxDimension);
		init();
		
	}	
	
	public Vector getColliderVelocityDueToGravity()
	{
		return velocity[0];
	}
	
	public Vector getColliderVelocityDueToGravity(int ColliderIndex)
	{
		return velocity[0];
	}
	
	private void init(){
		gravityColliders = new ArrayList<GravityCollider>();
		dD = new Vector();
		sumdD = new Vector();
		
		for (int i = 0; i < 4; i++){
			acceleration[i] = new Vector();
			velocity[i] = new Vector();
		}
	}
	
	protected MovingAce3dObject(Resources res, Vector pos, float maxDimension)
	{
		this(res, pos, new Orientation(0,0,0), maxDimension);
		
	}
	
	
	protected void addMainGravityCollider(GravityCollider gc)
	{
		gravityColliders.add(0, gc);
	}
	
	protected void addGravityCollider(GravityCollider gc)
	{
		gravityColliders.add(gc);
	}
	/**
	 * 
	 * @return passing height of the first gravity Collider
	 */
	public float getGCpassingHeight()
	{		
		return gravityColliders.get(0).getPassingHeight();
	}
	
	/**
	 * 
	 * @return height of the first gravity Collider
	 */
	
	
	public float getGCpassingHeight(int GravityColliderIndex)
	{
		return gravityColliders.get(GravityColliderIndex).getPassingHeight();
	}
	
	public float getGCHeight(int GravityColliderIndex)
	{
		return gravityColliders.get(GravityColliderIndex).getHeight();
	}
	public float getGCHeight()
	{
		return gravityColliders.get(0).getHeight();
	}
	
	public float getLastElevation(int GravityColliderIndex)
	{
		return gravityColliders.get(GravityColliderIndex).getLastElevation();
	}
	public float getLastElevation()
	{
		return gravityColliders.get(0).getLastElevation();
	}
	
	
	/**
	 * @return max dimension from the parent object added to the length of 
	 * the change in position for the frame
	 */
	public float getNoCollisionRadius()
	{
		return super.getNoCollisionRadius() + dD.getLength();
	}
	
	/**
	 * @return max dimension from the parent object added to the length of 
	 * the change in position for the frame
	 */
	public float getNoCollisionRadiusForGravityCheck()
	{
		return /*super.getNoCollisionRadius() +*/ velocity[0].getLength();
	}
	
	public boolean hasGravity()
	{
		if (gravityColliders.size() > 0)
			return true;
		else 
			return false;	
	}
	
	/*public Vector TstMethodGetGCPosition()
	{
		return gravityColliders.get(0).getWorldPosition();
	}*/
	/**
	 * this method provides default gravity handling mechanism for MovingAce3dObjects
	 * it is assumes that such an object has a simple gravity model based only on one,
	 * main gravity collider.
	 * 
	 * @param ci collision info
	 * @param deltaTime time for the frame
	 */
	public void processGravity(CollisionInfo ci, float deltaTime)
	{
		Log.d("processGravity", "Gravity Processing");
		//Log.d("processGravity", "cDist" + cDist + "");
		Log.d("processGravity", "cDist " + ci.getcDist() + "");
		Log.d("processGravity", "speed " + getColliderVelocityDueToGravity().getLength() + "");
		if (ci.getType() == CollisionType.GRAVITY_TO_PLANE)
		{
			Log.d("processGravity", "GRAVITY_TO_PLANE");
			float cDist = ci.getcDist();
			float speed = getColliderVelocityDueToGravity().getLength();	
			
			//falling
			if ((cDist * speed > 0)){
				Log.d("processGravity", "cDist * speed (" + cDist*speed + ") > 0");
				
				//speed is greater than height
				if (speed > getGCHeight())
					//distance to the surface is greater than height of the object
					if (cDist * speed > getGCHeight())
						//put object on surface
						translate(new Vector(0, -((speed * cDist) - getGCHeight()), 0));				
					else 		
						//push object to the surface
						translate(new Vector(0, getGCHeight() - (speed * cDist), 0));
					
				//speed is less than height
				else
					translate(new Vector(0, getGCHeight() * (1 - cDist), 0));
					
				setAcceleration(new Vector(0, 0.0f, 0), MovingComponent.DUE_TO_GRAVITY);
				setVelocity(new Vector(0,-0.0f,0), MovingComponent.DUE_TO_GRAVITY);
				//Recalculate position change
				setMovement(deltaTime);				
						
				
				/*if (cDist * speed < getGCpassingHeight())
				{
					//dD.addVector(new Vector(0, cDist,0));
				
					translate(new Vector(0, (cDist  * speed),0));
					setAcceleration(new Vector(0, 0.0f, 0), MovingComponent.DUE_TO_GRAVITY);
					setVelocity(new Vector(0,-0.0f,0), MovingComponent.DUE_TO_GRAVITY);
					//Recalculate position change
					setMovement(deltaTime);
				}
				else 
				{
					//Log.d("cDist - PG", cDist + "");
					//Log.d("speed - PG", speed + "");
					//Log.d("processGravity","hh");
					//rework translation for this two conditions. more conditions?
					if (cDist * speed > getGCHeight())
						translate(new Vector(0, getGCHeight() - cDist * speed,0));
					else 
						translate(new Vector(0, getGCHeight() - (cDist * speed),0));
					setAcceleration(new Vector(0, 0.0f, 0), MovingComponent.DUE_TO_GRAVITY);
					setVelocity(new Vector(0, 0.0f, 0), MovingComponent.DUE_TO_GRAVITY);
					//Recalculate position change
					setMovement(deltaTime);
				}
				*/	
			}
			//check climbing
			else
			{
				
			}
		}		
			//box.translate(new Vector(0,-0.04128f,0));
			//box.setVelocity(new Vector(0,-0.05f,0), MovingComponent.DUE_TO_GRAVITY);
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<GravityCollider> getGravityColliders()
	{
		return gravityColliders;
	}
	
	/*protected void moveColliders(Vector dPosition)
	{
		super.moveColliders(dPosition);
		for (int i = 0; i < gravityColliders.size(); i++)
			gravityColliders.get(i).changeWorldPosition(dPosition);
		//sumdD = new Vector();
	}*/
	
	public final void setAcceleration(Vector a, MovingComponent component)
	{
		switch (component){		
			case DUE_TO_GRAVITY: acceleration[0] = a;
			break;
			case MOVING_0: acceleration[1] = a;
			break;
			case MOVING_1: acceleration[2] = a;
			break;
			case MOVING_2: acceleration[3] = a;
			break;
			
		} 
	}
	
	public final void setVelocity(Vector v, MovingComponent component)
	{
		switch (component){		
			case DUE_TO_GRAVITY: velocity[0] = v;
				break;
			case MOVING_0: velocity[1] = v;
			//Log.d("Vector Vel:", velocity[1].toString());
				break;
			case MOVING_1: velocity[2] = v;
				break;
			case MOVING_2: velocity[3] = v;
				break;			
		} 
	}
	
	public final Vector getAcceleration(MovingComponent component)
	{
		Vector a;
		switch (component){		
			case DUE_TO_GRAVITY: a = acceleration[0];
				break;
			case MOVING_0: a = acceleration[1];
				break;
			case MOVING_1: a = acceleration[2];
				break;
			case MOVING_2: a = acceleration[3];
				break;
			default: a = new Vector();
		} 		
		return a;
	}
	
	public final Vector getVelocity(MovingComponent component)
	{
		Vector v;
		switch (component){		
			case DUE_TO_GRAVITY: v = velocity[0];
				break;
			case MOVING_0: v = velocity[1];
				break;
			case MOVING_1: v = velocity[2];
				break;
			case MOVING_2: v = velocity[3];
				break;
			default: v = new Vector();
		} 		
		return v;
	}	
	
	
	public void placeObject() {			
		//sumdD = sumdD.addVector(dD);
		sumdD.add(dD);
		//worldPosition = worldPosition.addVector(dD);		
		worldPosition.add(dD);
	}
	
	//public void moveColliders()
	//{
		/*for (int i = 0; i < colliders.size(); i++)
			colliders.get(i).changeWorldPosition(sumdD);
		for (int i = 0; i < gravityColliders.size(); i++)
			gravityColliders.get(i).changeWorldPosition(sumdD);
		sumdD = new Vector();*/
	//}

	/**
	 * Recalculates position of all collider of the object according to the change in position 
	 * made by object since last recalculation or movement. 
	 */
	/*public void recalculateColliderPositions()
	{
		for (int i = 0; i < colliders.size(); i++)
			colliders.get(i).changeWorldPosition(sumdD);
		//sumdD = new Vector();
	}*/
	
	public void render(GL10 gl) {
		placeObject();
		super.render(gl);
		//for (Collider c : colliders)
		//	c.applyRotation();
		for (int i = 0; i < colliders.size(); i++)
		{
			colliders.get(i).applyRotation();
		}
	}

	/**
	 * sets movement of the object for the next frame
	 * @param time time for the frame.
	 */
	public void setMovement(float time) {		
		previoiusFrameTime = time;
		//dD = new Vector();
		dD.resetVector();
		for (int i = 0; i < 4; i++)
		{
			//calculate how much object accelerated in the frame's interval
			tempAccel.multiplyVectorByScalar(acceleration[i], time);
			//calculate change in objects velocity
			//Vector dV = velocity[i].addVector(tA);
			//compute new velocity
			velocity[i].add(tempAccel);
			//calculate change in object's position
			//Log.d("setMovement", "Vel" + i + ": " + velocity[i]);
			tempVelocity.multiplyVectorByScalar(velocity[i], time);
			dD.add(tempVelocity);
			//sumdD.add(dD);
		}

	}
	/**
	 * sets movement of the object for the next frame
	 * assumed that the time for the frame was set by previous call to 
	 * setMovement(float time)
	 */
	/*public void setMovement() {			
		setMovement(previoiusFrameTime);
	}*/
	
	public Vector getPositionChange() {
		//return dD.clone();
		return dD; 
	}	
		

}

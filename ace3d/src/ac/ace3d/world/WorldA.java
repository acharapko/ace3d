package ac.ace3d.world;

import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.Vector;
import ac.ace3d.collisions.CollisionDetecter;
import ac.ace3d.collisions.CollisionInfo;
import ac.ace3d.collisions.GravityDetecter;
import ac.ace3d.collisions.CollisionInfo.CollisionType;
import ac.ace3d.meshes.Renderable;
import ac.ace3d.world.MovableAce3dOI.MovingComponent;
import android.util.Log;

public class WorldA implements World{
	
	//private ArrayList<Sector> sectors;
	
	private ArrayList<MovingAce3dObject> Objects = new ArrayList<MovingAce3dObject>();
	private ArrayList<Ace3dObject> StationaryObjects = new ArrayList<Ace3dObject>();
	private CollisionInfo ci = new CollisionInfo();
	//private ArrayList<Ace3dObject> outOfSectorObjects;
	private Vector AccelerationDueToGravity;
	
	private Vector tempObjWorldPosition1 = new Vector();
	private Vector tempObjWorldPosition2 = new Vector();
	
	private float visionRadius;
	//private boolean renderOutOfSectorObjects = false;
	
	public WorldA()
	{		
		this(new Vector (0, -9.81f, 0));
	}
	
	public WorldA(Vector gravity)
	{
		AccelerationDueToGravity = gravity;
		visionRadius = 20.0f;
		//sectors = new ArrayList<Sector>();
	}
	
	/*public void addSector(Sector sector)
	{
		sectors.add(sector);
	}*/
	
	public void setVisionRadius(float r)
	{
		visionRadius = r;
	}
	
	public void addGroup(Group p)
	{
		Object[] mo  = p.getMovingObjects();
		Object[] so  = p.getStationaryObjects();
		
		for (int i = 0; i < mo.length; i++)
			Objects.add((MovingAce3dObject)mo[i]);
		for (int i = 0; i < so.length; i++)
			StationaryObjects.add((Ace3dObject)so[i]);
	}
	
	public void addObject(Ace3dObject obj)
	{
		StationaryObjects.add(obj);
	}
	
	public void addMovingObject(MovingAce3dObject obj)
	{
		Objects.add(obj);
	}
	
	/*public void renderOutOfSector(boolean r)
	{
		renderOutOfSectorObjects = r;lf 
	}*/
		
	public void render(GL10 gl) {

		/*for (int i = 0; i < sectors.size(); i++)
			sectors.get(i).render(gl);
		
		//render out of Sector if enabled
		if (renderOutOfSectorObjects)
			for (int i = 0; i < outOfSectorObjects.size(); i++)
				outOfSectorObjects.get(i).render(gl);*/
		
		for (int i = 0; i < Objects.size(); i++){
			gl.glPushMatrix();
			Objects.get(i).render(gl);
			gl.glPopMatrix();
		}
		for (int i = 0; i < StationaryObjects.size(); i++){
			gl.glPushMatrix();
			StationaryObjects.get(i).render(gl);
			gl.glPopMatrix();
		}
		
		
	}
	/**
	 * check collision in sectors around point pointed by Vector pos
	 * @param pos position of the "player"
	 * @param radius max distance for "visible" sector
	 */
	private void detectCollisions(Vector pos, float radius)
	{
		/*for (int i = 0; i < sectors.size(); i++)
			if (sectors.get(i).getWorldPosition().subtractVector(pos).getLength() < radius)
				sectors.get(i).checkCollidingObjects();		*/
		//stationary with moving objects	
		for (int i = 0; i < StationaryObjects.size(); i++)
		{
			StationaryObjects.get(i).getWorldPosition(tempObjWorldPosition1);
			if (tempObjWorldPosition1.subtractVector(pos).getLength() < radius)
				for (int j = 0; j < Objects.size(); j++)
				{
					Objects.get(j).getWorldPosition(tempObjWorldPosition2);
					if (tempObjWorldPosition2.subtractVector(pos).getLength() < radius)
					{
						CollisionInfo ci = StationaryObjects.get(i).detectCollision(Objects.get(j));
					
					}
				}
		}
		//moving with moving objects
		for (int i = 0; i < Objects.size(); i++)
		{
			Objects.get(i).getWorldPosition(tempObjWorldPosition1);
			if (tempObjWorldPosition1.subtractVector(pos).getLength() < radius)
				for (int j = 0; j < Objects.size(); j++)
				{
					Objects.get(j).getWorldPosition(tempObjWorldPosition2);
					if (tempObjWorldPosition2.subtractVector(pos).getLength() < radius)
					{
						CollisionInfo ci = Objects.get(i).detectCollision(Objects.get(j));
					
					}
				}
		}
				
					
	}	
	
	/**
	 * checks collision in all Objects of the world
	 */
	private void detectCollisions()
	{
		CollisionDetecter.testCCounter = 0;
		for (int i = 0; i < StationaryObjects.size(); i++)
			for (int j = 0; j < Objects.size(); j++)
			{
				//CollisionInfo ci = new CollisionInfo();
				ci.reset();
				CollisionDetecter.detectCollision(StationaryObjects.get(i), Objects.get(j), ci);
				//Log.d("collisionType", ci.getType() + "");
				if (ci.getType() == CollisionInfo.CollisionType.POINT_TO_PLANE){
					StationaryObjects.get(i).processCollision(ci);
					Objects.get(j).processCollision(ci);
				}
			}
		//moving with moving objects
		for (int i = 0; i < Objects.size(); i++)
			for (int j = 0; j < Objects.size(); j++)
			{						
				//CollisionInfo ci = new CollisionInfo();
				ci.reset();
				CollisionDetecter.detectCollision(Objects.get(i),Objects.get(j), ci);
				if (ci.getType() == CollisionInfo.CollisionType.POINT_TO_PLANE){
					Objects.get(i).processCollision(ci);
					Objects.get(j).processCollision(ci);
				}
			}
	}
	
	private void applyGravity(float deltaTime)
	{
		//stationary objects with MovingObjects
		for (int i = 0; i < StationaryObjects.size(); i++)
			for (int j = 0; j < Objects.size(); j++)
				if (Objects.get(j).hasGravity())
				{
					//CollisionInfo ci = StationaryObjects.get(i).checkGravity(Objects.get(j));
					CollisionInfo gci = GravityDetecter.checkGravity(StationaryObjects.get(i), Objects.get(j));	
					Objects.get(j).processGravity(gci, deltaTime);
					
					if (gci.getType() == CollisionType.NO_COLLISION)
						Objects.get(j).setAcceleration(getAccelerationDueToGravity(),
								MovingComponent.DUE_TO_GRAVITY);					
				}
		//moving objects with moving objects
		for (int i = 0; i < Objects.size(); i++)
			for (int j = 0; j < Objects.size(); j++)
				if ((Objects.get(j).hasGravity() && (i != j)))
				{
					//CollisionInfo ci = StationaryObjects.get(i).checkGravity(Objects.get(j));
					CollisionInfo gci = GravityDetecter.checkGravity(Objects.get(i), Objects.get(j));	
					Objects.get(j).processGravity(gci, deltaTime);
					
					if (gci.getType() == CollisionType.NO_COLLISION)
						Objects.get(j).setAcceleration(getAccelerationDueToGravity(),
								MovingComponent.DUE_TO_GRAVITY);					
				}
	}
	
	private void applyGravity(float deltaTime, Vector pos, float radius)
	{
		//stationary objects with MovingObjects
		for (int i = 0; i < StationaryObjects.size(); i++)
		{
			StationaryObjects.get(i).getWorldPosition(tempObjWorldPosition1);
			if (tempObjWorldPosition1.subtractVector(pos).getLength() < radius)
				for (int j = 0; j < Objects.size(); j++)
				{
					Objects.get(j).getWorldPosition(tempObjWorldPosition2);
					if (tempObjWorldPosition2.subtractVector(pos).getLength() < radius)
						if (Objects.get(j).hasGravity())
						{
							//CollisionInfo ci = StationaryObjects.get(i).checkGravity(Objects.get(j));
							CollisionInfo gci = GravityDetecter.checkGravity(StationaryObjects.get(i), Objects.get(j));	
							Objects.get(j).processGravity(gci, deltaTime);
							
							if (gci.getType() == CollisionType.NO_COLLISION)
								Objects.get(j).setAcceleration(getAccelerationDueToGravity(),
										MovingComponent.DUE_TO_GRAVITY);					
						}
				}
		}
		//moving objects with moving objects
		for (int i = 0; i < Objects.size(); i++)
		{
			Objects.get(i).getWorldPosition(tempObjWorldPosition1);
			if (tempObjWorldPosition1.subtractVector(pos).getLength() < radius)
				for (int j = 0; j < Objects.size(); j++)
				{
					Objects.get(j).getWorldPosition(tempObjWorldPosition2);
					if (tempObjWorldPosition2.subtractVector(pos).getLength() < radius)
						if ((Objects.get(j).hasGravity() && (i != j)))
						{
							//CollisionInfo ci = StationaryObjects.get(i).checkGravity(Objects.get(j));
							CollisionInfo gci = GravityDetecter.checkGravity(Objects.get(i), 
									Objects.get(j));	
							Objects.get(j).processGravity(gci, deltaTime);
						
							if (gci.getType() == CollisionType.NO_COLLISION)
								Objects.get(j).setAcceleration(getAccelerationDueToGravity(),
										MovingComponent.DUE_TO_GRAVITY);					
						}
				}
		}
	}
	
	/**
	 * prepares objects for the new frame
	 * @param deltaTime the time used for creating previous frame
	 */
	public void prepareNewFrame(float deltaTime, Vector pos)
	{
		for(MovingAce3dObject o : Objects)
			o.setMovement(deltaTime);
		applyGravity(deltaTime, pos, visionRadius);
		detectCollisions(pos, visionRadius);
		
	}
	
	/**
	 * prepares objects for the new frame
	 * @param deltaTime the time used for creating previous frame
	 */
	public void prepareNewFrame(float deltaTime)
	{
		//for(MovingAce3dObject o : Objects)
			//o.setMovement(deltaTime);
		for (int i = 0; i < Objects.size(); i++)
		{
			Objects.get(i).setMovement(deltaTime);
		}
		applyGravity(deltaTime);
		detectCollisions();
		
	}
	
	/*public void objectTransition(MovingAce3dObject obj)
	{
		boolean isInNewSector = false;
		for (int i = 0; i < sectors.size(); i++)
		{
			Vector sectorPosition = sectors.get(i).getWorldPosition();
			Vector objectPosition = obj.getWorldPosition();
			Vector dimension = sectors.get(i).getDimensions();
			
			if ((objectPosition.getiComponent() < 
				sectorPosition.getiComponent() + dimension.getiComponent() &&
				objectPosition.getiComponent() > sectorPosition.getiComponent()))
				if ((objectPosition.getjComponent() < 
						sectorPosition.getjComponent() + dimension.getjComponent() &&
						objectPosition.getjComponent() > sectorPosition.getjComponent()))
					if ((objectPosition.getkComponent() < 
							sectorPosition.getkComponent() + dimension.getkComponent() &&
							objectPosition.getkComponent() > sectorPosition.getkComponent())){
						sectors.get(i).addObject(obj);
						i = sectors.size();
						isInNewSector = true;
					}
		}
		if (!isInNewSector)
			outOfSectorObjects.add(obj);
		
	}*/
	
	public void setAccelerationDueToGravity(Vector accelerationDueToGravity) {
		AccelerationDueToGravity = accelerationDueToGravity;
	}

	public Vector getAccelerationDueToGravity() {
		return AccelerationDueToGravity;
	}	

	/*public void applyCamera(Camera c) {
		for (int i = 0; i< sectors.size(); i++)
			sectors.get(i).applyCamera(c);
		
	}*/
}

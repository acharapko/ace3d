package ac.ace3d;

import javax.microedition.khronos.opengles.GL10;

public interface ACE3D {
	//constants
	public static final int ACE3D_CLOCKWISE = GL10.GL_CW;
	public static final int ACE3D_COUNTERCLOCKWISE = GL10.GL_CCW;
	public static final int ACE3D_CULL_FACE_BACK = GL10.GL_BACK;
	public static final int ACE3D_CULL_FACE_FRONT = GL10.GL_FRONT;
	public static final int ACE3D_CULL_FACE_NONE = 10001;
	public static final int ACE3D_USE_VERTEX_INDICES = 10002;
	public static final int ACE3D_NOT_USE_VERTEX_INDICES = 10003;
	public static final int ACE3D_AMBIENT = 10004;
	public static final int ACE3D_DIFFUSE = 10005;
	public static final int ACE3D_SPECULAR = 10006;
}


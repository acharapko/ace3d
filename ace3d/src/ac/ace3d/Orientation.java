package ac.ace3d;


public class Orientation {
	private float rotationY, rotationFromHorizont, rotationAroundAxis;
		
	public Orientation()
	{
		setRotationY(0);
		setRotationFromHorizont(0);
		setRotationAroundAxis(0);
	}
	
	public Orientation(float aroundY, float fromHorizont, float aroundAxis)
	{
		setRotationY(aroundY);
		setRotationFromHorizont(fromHorizont);
		setRotationAroundAxis(aroundAxis);
	}
	
	/**
	 * resets orientation to a specified value
	 * @param v
	 * Vector parameter.
	 */	
	
	public void resetOrientation(float rotationY, float rotationFromHorizont, float rotationAroundAxis)
	{	
		this.rotationY = rotationY;
		this.rotationFromHorizont = rotationFromHorizont;
		this.rotationAroundAxis = rotationAroundAxis;
	}
	
	public static Orientation getOrientationFromVector(Vector v)
	{
		float raaY = v.getRotationAngleAroundYaxis();
		float raaZ = v.getRotationAngleAroundZXaxis(); 			
		if ((raaY > 90) && (raaY < 270))
			raaZ = 180 - raaZ;
		return new Orientation(raaY, raaZ, 0);
	}
	
	public static Orientation getOrientationFromVectorAndAxisRotation(Vector v, float rotation)
	{
		float raaY = v.getRotationAngleAroundYaxis();
		float raaZ = v.getRotationAngleAroundZXaxis(); 			
		if ((raaY > 90) && (raaY < 270))
			raaZ = 180 - raaZ;
		return new Orientation(raaY, raaZ, rotation);
	}
	public static void getDirectionFormOrientation(Orientation or, Vector direction)
	{
		//float Vect[] = new float[3];
		float pi180 = (float) (Math.PI / 180);
		//angle = (float)(angle * Math.PI / 180);
		double angleY = or.getRotationY() * pi180;
		float vc0 = -(float) Math.cos(angleY);
		float vc2 = -(float) Math.sin(angleY);		
		float vc1 = (float) Math.tan(or.getRotationFromHorizont() * pi180);
		
		direction.resetVector(vc0,  vc1, vc2);
		//return new Vector(vc0, vc1, vc2).normilize();
	}
	
	public Orientation addOrientation(Orientation or)
	{
		return new Orientation(rotationY + or.getRotationY(),
				rotationFromHorizont + or.getRotationFromHorizont(),
				rotationAroundAxis + or.getRotationFromHorizont());
	}
	
	public Orientation SubtractOrientation(Orientation or)
	{
		return new Orientation(rotationY - or.getRotationY(),
				rotationFromHorizont - or.getRotationFromHorizont(),
				rotationAroundAxis - or.getRotationFromHorizont());
	}
	
	public Orientation negateOrientation(Orientation or)
	{
		return new Orientation(-or.getRotationY(),
				-or.getRotationFromHorizont(),
				-or.getRotationFromHorizont());
	}
	
	
	public void addToThisOrientation(Orientation or)
	{
		rotationY = rotationY + or.getRotationY();
		rotationFromHorizont =	rotationFromHorizont + or.getRotationFromHorizont();
		rotationAroundAxis = rotationAroundAxis + or.getRotationFromHorizont();
	}
	
	public void subtractFromThisOrientation(Orientation or)
	{
		rotationY = rotationY - or.getRotationY();
		rotationFromHorizont =	rotationFromHorizont - or.getRotationFromHorizont();
		rotationAroundAxis = rotationAroundAxis - or.getRotationFromHorizont();
	}
	
	public void negateThisOrientation()
	{
		rotationY = - rotationY;
		rotationFromHorizont = -rotationFromHorizont;
		rotationAroundAxis = -rotationAroundAxis;
	}
	
	
	public void rotateAroundY(float angle)
	{
		rotationY += angle;
		if (Math.abs(rotationY) >= 360)
			rotationY = rotationY % 360;
		if (rotationY < 0)
			rotationY = 360 + rotationY;
	}
	
	public void rotateAroundHorizontal(float angle)
	{
		rotationFromHorizont += angle;
		
		if (Math.abs(rotationFromHorizont) >= 360)
			rotationFromHorizont = rotationFromHorizont % 360;
		if (rotationFromHorizont < 0)
			rotationFromHorizont = 360 + rotationFromHorizont;
	}
	
	public void rotateAroundAxis(float angle)
	{
		rotationAroundAxis += angle;
		
		if (Math.abs(rotationAroundAxis) >= 360)
			rotationAroundAxis = rotationAroundAxis % 360;
		if (rotationAroundAxis < 0)
			rotationAroundAxis = 360 + rotationAroundAxis;
	}
	
	
	private void setRotationY(float rotationY) {
		this.rotationY = rotationY % 360;
		
	}

	public float getRotationY() {
		return rotationY;
	}

	private void setRotationFromHorizont(float rotationFromHorizont) {
		this.rotationFromHorizont = rotationFromHorizont % 360;
	}

	public float getRotationFromHorizont() {
		return rotationFromHorizont;
	}

	private void setRotationAroundAxis(float rotationArounAxis) {
		this.rotationAroundAxis = rotationArounAxis % 360;
	}

	public float getRotationArounAxis() {
		return rotationAroundAxis;
	}
}

package ac.ace3d;

public class shortList {
		private int size;
		private int len, index;
		private short[] b;
		
		public shortList()
		{			
			this(128);
		}
		public shortList(int size)
		{
			this.size = size;
			len = 0;
			b = new short[size];
		}
		public short[] readBuffer(){
			short[] b2 = new short[len];
			for (int i = 0; i < len; i++)
				b2[i]= b[i];
			return b2;
		}
		public int getLength(){
			return len;
		}
		
		public int getPosition(){
			return index;
		}
		public void setPosition(int position){
			index = position;
		}
		
		public short get(int index){
			return b[index];
		}
		
		public void appendShort(short appendshort)
		{
			b[index++] = appendshort;
			if (++len == b.length)
			{
				short[] b2 = new short[b.length * 2];
				for (int i = 0; i < b.length; i++)
					b2[i] = b[i];
				b = b2;
			}
		}
		
		public void resetBuffer()
		{
			b = new short[size];
			len = 0;
		}
		
		public void append(short[] appendShortes)
		{
			for (int i = 0; i < appendShortes.length; i++)
				appendShort(appendShortes[i]);
		}
}
package ac.ace3d.scene;

import ac.ace3d.renderer.ace3dRenderer;
import ac.ace3d.scene.Sensors.MyBinder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.os.IBinder;
import android.util.Log;

public abstract class Scene extends GLSurfaceView implements SceneController{

	private Context context;
	private Resources res;
	protected ace3dRenderer ace3dR;	
	protected float[] orientationValues = new float[3];
	private Thread taskThread;
	private Runnable task;
		
	private boolean mIsBound;
	
	private Sensors mBoundService;

	
	private ServiceConnection mConnection = new ServiceConnection() {
	    public void onServiceConnected(ComponentName className, IBinder service) {
	        // This is called when the connection with the service has been
	        // established, giving us the service object we can use to
	        // interact with the service.  Because we have bound to a explicit
	        // service that we know is running in our own process, we can
	        // cast its IBinder to a concrete class and directly access it.
	        mBoundService = ((MyBinder)service).getService();
	        
	        
	        Log.d("Scene", "Service Connected");
	        
	    }

	    public void onServiceDisconnected(ComponentName className) {
	        // This is called when the connection with the service has been
	        // unexpectedly disconnected -- that is, its process crashed.
	        // Because it is running in our same process, we should never
	        // see this happen.
	        mBoundService = null;	       
	    }
	};
	
	
	void doBindService() {
	    // Establish a connection with the service.  We use an explicit
	    // class name because we want a specific service implementation that
	    // we know will be running in our own process (and thus won't be
	    // supporting component replacement by other applications).
	    this.getContext().bindService(new Intent(this.getContext(), 
	            Sensors.class), mConnection, Context.BIND_AUTO_CREATE);
	    mIsBound = true;
	    Log.d("Scene", "Service Bind");
	}

	void doUnbindService() {
	    if (mIsBound) {
	        // Detach our existing connection.
	    	this.getContext().unbindService(mConnection);
	        mIsBound = false;
	    }
	}


	
	protected Scene(final Context context) {
		super(context);
		
		
		 task = new Runnable() {
	            public void run(){
	            	Intent service = new Intent(context, Sensors.class);
	        	    context.startService(service);         
	            }
	        };
	       
        taskThread = new Thread(task);
		taskThread.start();        
		
		
		
		doBindService();
		res = context.getResources();
		this.context = context;
		this.setEGLConfigChooser(8 , 8, 8, 8, 16, 0);
		this.requestFocus();
		this.setFocusableInTouchMode(true);		
		
	}
	
	public void setRenderer(ace3dRenderer rend)
	{
		super.setRenderer(rend); 
		ace3dR = rend;		
		rend.setSceneController(this);
	}
	
	public Resources getResources()
	{
		return res;
	}
	
	public ace3dRenderer getRenderer()
	{
		return ace3dR;
	}
	

	public boolean haveSensorsValues()
	{
		return mBoundService.haveValues();
	}
	
	
	public float getDefRoll()
	{
		return mBoundService.getDefRoll();
	}
	
	protected float[] getRotationMatrix()
	{
		
		/*SensorManager.getRotationMatrix(RotationMatrix, I, gravitySensorVals, geomagneticSensorVals);
		return RotationMatrix;*/
		return mBoundService.getRotationMatrix();
	}
	

}

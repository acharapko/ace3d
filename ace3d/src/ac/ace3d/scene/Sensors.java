package ac.ace3d.scene;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class Sensors extends Service implements SensorEventListener {

	private final IBinder mBinder = new MyBinder();
	protected SensorManager mySensorManager;
	private Sensor myGraitySensor, myMagneticSensor;
	private Thread taskThread;
	private Runnable task;
	
	private float[] orientationValues = new float[3];
	private float[] RotationMatrix = new float[16];
	private float[] gravitySensorVals;
	private float[] geomagneticSensorVals;	
	private float defRoll = -100.0f;
	private float[] I = new float[16];
	
	protected void stopListeners()
	{
		mySensorManager.unregisterListener(this);
	}
	protected void startListeners()
	{
		
		mySensorManager.registerListener(this, myGraitySensor, SensorManager.SENSOR_DELAY_NORMAL);
		mySensorManager.registerListener(this, myMagneticSensor, SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	@Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("Sensors", "Received start id " + startId + ": " + intent);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        
       // task = new Runnable() {
      //      public void run(){
            	mySensorManager = (SensorManager)getBaseContext().getSystemService(Context.SENSOR_SERVICE);
        		myGraitySensor = mySensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        		myMagneticSensor = mySensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        		startListeners();               
       //     }
       // };
       
       // taskThread = new Thread(task);
		//taskThread.start();        

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
    	stopListeners();	
    }

	
	@Override
	public IBinder onBind(Intent arg0) {
	    return mBinder;
	}

	public class MyBinder extends Binder {
	    Sensors getService() {
	      return Sensors.this;
	    }
	}
	
	public boolean haveValues()
	{
		return ((gravitySensorVals != null) && (geomagneticSensorVals != null));
	}
	
	protected float[] getRotationMatrix()
	{		
		SensorManager.getRotationMatrix(RotationMatrix, I, gravitySensorVals, geomagneticSensorVals);
		return RotationMatrix;
	}
	
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		//left blank
		
	}

	public void onSensorChanged(SensorEvent event) {
		//left blank
		if (event.sensor.equals(myGraitySensor))
			gravitySensorVals = event.values;	
		if (event.sensor.equals(myMagneticSensor))
			geomagneticSensorVals = event.values;
		
		if (defRoll == -100.0f)
			calcdefRoll();
	}
		
	private void calcdefRoll()
	{		

		if ((gravitySensorVals != null) && (geomagneticSensorVals != null))
			if (SensorManager.getRotationMatrix(RotationMatrix, I, gravitySensorVals, geomagneticSensorVals)){
				SensorManager.getOrientation(RotationMatrix, orientationValues);				
				defRoll = orientationValues[2]; 
				
				//SensorManager.getRotationMatrix(RotationMatrix, I, gravity, geomagnetic))
				
			}
	}
	
	public float getDefRoll()
	{
		return defRoll;
	}

}

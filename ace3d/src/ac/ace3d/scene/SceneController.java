package ac.ace3d.scene;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import android.hardware.SensorEventListener;

public interface SceneController{
		
	/** init()
	 * scene initialization. loading meshes, setting up the scene.
	 */
	public void init(GL10 gl);
	
	public void newFrame(float deltaTime, GL10 gl);
	

}

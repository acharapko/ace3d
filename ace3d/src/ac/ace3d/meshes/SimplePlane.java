package ac.ace3d.meshes;

import android.graphics.Bitmap;
import android.util.Log;

public class SimplePlane extends TexturedMesh{
	
	public SimplePlane(float width, float height)
	{
		float halfWidth = width / 2.0f;
		float halfHeight = height / 2.0f;
		Log.d("SP.halfHeight", halfHeight + "");
		Log.d("SP.halfWidtht", halfWidth + "");
		/*float[] vertices = {0, halfHeight, halfWidth,
							0, -halfHeight, halfWidth,
							0, halfHeight, -halfWidth,
							
							0, halfHeight, -halfWidth,
							0, -halfHeight, halfWidth,
							0, -halfHeight, -halfWidth 			
		};*/
		float[] vertices = {0, halfHeight, halfWidth,
				0, -halfHeight, halfWidth,
				0, halfHeight, -halfWidth,
				
				0, halfHeight, -halfWidth,
				0, -halfHeight, halfWidth,
				0, -halfHeight, -halfWidth 			
			};
		
		setVertices(vertices);
		
	}
	/**
	 * @deprecated
	 */
	public void setTexture(Bitmap b)
	{
		setTexture(b,1,1);
	}
	/**
	 * @deprecated
	 * @param b
	 * @param repeat_w
	 * @param repeat_h
	 */
	public void setTexture(Bitmap b, float repeat_w, float repeat_h)
	{
		super.setTexture(b);
		float textureCoordinates[] = {0.0f, 0.0f,
                0.0f, repeat_h,
                repeat_w, 0.0f,
                repeat_w, 0.0f,
                0.0f, repeat_h,
                repeat_w, repeat_h};
		super.setUVmap(textureCoordinates);
	}
	
	public void setTexture(int tPointer, float repeat_w, float repeat_h)
	{
		super.setTexture(tPointer);
		float textureCoordinates[] = {0.0f, 0.0f,
                0.0f, repeat_h,
                repeat_w, 0.0f,
                repeat_w, 0.0f,
                0.0f, repeat_h,
                repeat_w, repeat_h};
		super.setUVmap(textureCoordinates);
	}
	
	public void setTexture(int[] tPointers, float repeat_w, float repeat_h)
	{
		super.setTextures(tPointers);
		float textureCoordinates[] = {0.0f, 0.0f,
                0.0f, repeat_h,
                repeat_w, 0.0f,
                repeat_w, 0.0f,
                0.0f, repeat_h,
                repeat_w, repeat_h};
		super.setUVmap(textureCoordinates);
	}

}

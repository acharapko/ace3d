package ac.ace3d.meshes;

import javax.microedition.khronos.opengles.GL10;

public class StaticPlane extends SimplePlane{

	
	public StaticPlane(float width, float height) {
		super(width, height);
	}

	public void render(GL10 gl) {
		// Counter-clockwise winding.
		gl.glFrontFace(winding);
		
		//setting material properties
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_AMBIENT, mtlAmbient, 0);
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_DIFFUSE, mtlDiffuse, 0);
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SPECULAR, mtlSpecular, 0);
		gl.glMaterialf(GL10.GL_FRONT_AND_BACK, GL10.GL_SHININESS, 20.0f);		
		
		gl.glEnable(GL10.GL_CULL_FACE);
		// What faces to remove with the face culling.
		gl.glCullFace(faceCulling);		
		// Enabled the vertices buffer for writing and to be used during
		// rendering.
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		// Specifies the location and data format of an array of vertex
		// coordinates to use when rendering.
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, verticesBuffer);
		// Set flat color
		
		if (UseNormals == true){
			gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);
			gl.glNormalPointer(GL10.GL_FLOAT, 0, NormalBuffer);
		}		
		
		if (UVmap != null) {
			
			//------------------
			gl.glEnable(GL10.GL_TEXTURE_2D);
			// Enable the texture state
			gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

			// Point to our buffers
			gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, UVmap);
			//bind texture. make sure texture is loaded
			gl.glBindTexture(GL10.GL_TEXTURE_2D, texturePointers[currentFrame]);
		}
		
		
		//gl.glTranslatef(tx, ty, tz);
		gl.glTranslatef(tx, ty, -1.0f);
		
		gl.glRotatef(ry, 0, 1, 0);
		//Log.d("mesh.ry", ry + "");
		gl.glRotatef(rz, 0, 0, 1);
		//Log.d("mesh.rz", rz + "");
		
		gl.glRotatef(rx, 1, 0, 0);
		//Log.d("mesh.rx", rx + "");
		//gl.glRotatef(rv, rVector.getiComponent(), rVector.getjComponent(), rVector.getkComponent());
		
		//draw vertices
		if (!useIndices)
			gl.glDrawArrays(drawingMode, 0, verticesBuffer.capacity() / 3);
		else
			gl.glDrawElements(GL10.GL_TRIANGLES, numOfIndices,
				GL10.GL_UNSIGNED_SHORT, indicesBuffer);
		
		// Disable the vertices buffer.		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		//Disable blending
		gl.glDisable(GL10.GL_BLEND);							
		
		
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glDisable(GL10.GL_TEXTURE_2D);
		

		// Disable face culling.
		gl.glDisable(GL10.GL_CULL_FACE);
		//disable normals
		gl.glDisableClientState(GL10.GL_NORMAL_ARRAY);
		
	}

}

package ac.ace3d.meshes;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import android.graphics.Bitmap;

import java.util.HashMap;
import java.util.Map;
import android.opengl.GLU;
import android.opengl.GLUtils;
import android.util.Log;

public class TextureManager {

	private static Map<String, Integer> textMap = new HashMap<String, Integer>();
	/**
	 * 
	 * @param gl open gl context
	 * @param b texture bitmap
	 * 
	 * @param wraps 
	 * @param wrapt
	 * sets texture wrap parameters.
     * see OpenGL ES
     * GL_REPEAT 
     * GL_CLAMP_TO_EDGE
	 * @return pointer to texture
	 */
	public static int loadTexture(GL10 gl,  Bitmap b, String tag, int wraps, int wrapt)
	{
		if (tag == "")
			tag = "texture" + textMap.size();
		// Generate texture pointers
		
		
		
		
		
		  int texturePointers[] = new int[1];
		  gl.glGenTextures(1, texturePointers, 0);
	   		
	   	  gl.glBindTexture(GL10.GL_TEXTURE_2D, texturePointers[0]);
	   	 
	   	  
	   	  //gl.glBindTexture(GL10.GL_TEXTURE_2D, textureID);
	   
	   
	   		
	   	  gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
	   					GL10.GL_LINEAR_MIPMAP_NEAREST);
	   	  gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
	   					GL10.GL_LINEAR);
	   	  
	  	  
	   	 
	   	  gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
	   							wraps);
	   	  gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
	   							wrapt);
	   	 
	   	  // load bitmap
	   	  try{
	   		  //if openGL es 1.1 or newer, make mipmaps
	   		  if(gl instanceof GL11) {
	   			  //Log.d("textManager", "MAKING MIPMAPS!!!!!");
			   	  gl.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_GENERATE_MIPMAP, GL11.GL_TRUE);
			   	  GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, b, 0);
			  }
	   		  //if not v1.1, just load bitmap and hope for the best
	   		  else
	   		     GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, b, 0);
	   		  //texture[i].recycle();
	   	  }
	   	  catch (Exception e){
	   		  int err = gl.glGetError();
	   		  if (err != 0) {
	   			  // err == 1280, prints "invalid enum":
	   			  System.err.println(GLU.gluErrorString(err));
	   		  }
	   		  System.err.println(e.getMessage());
	   	  }
	   	  b.recycle();
	   	  textMap.put(tag, texturePointers[0]);
	   	  return texturePointers[0];
	}
	
	/**
	 * 
	 * @param gl open gl context
	 * @param texture texture bitmaps
	 * 
	 * @param wraps 
	 * @param wrapt
	 * sets texture wrap parameters.
     * see OpenGL ES
     * GL_REPEAT 
     * GL_CLAMP_TO_EDGE
	 * @return pointers to texture
	 */
	public static int[] loadTextureArray(GL10 gl,  Bitmap texture[], String tag, int wraps, int wrapt)
	{
		if (tag == "")
			tag = "texture" + textMap.size();
		// Generate texture pointers
		  int texturePointers[] = new int[texture.length];
		  gl.glGenTextures(texture.length, texturePointers, 0);
	   	
	   	 
	   	for (int i = 0; i < texture.length; i++){
	   		
	   		gl.glBindTexture(GL10.GL_TEXTURE_2D, texturePointers[i]);
	   	 
	   		
	   		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
	   					GL10.GL_LINEAR_MIPMAP_NEAREST);
	   		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
	   					GL10.GL_LINEAR);
	   	 
	   		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
	   							wraps);
	   		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
	   							wrapt);
	   	 
	   		// load bitmap
	   		try{
	   			//if openGL es 1.1 or newer, make mipmaps
	   			if(gl instanceof GL11) {
				   	  gl.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_GENERATE_MIPMAP, GL11.GL_TRUE);
				   	  GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, texture[i], 0);
				  }
	   			//if not v1.1, just load bitmap and hope for the best
	   			else
	   				GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, texture[i], 0);
	   			texture[i].recycle(); //need to recycle?
	   			textMap.put(tag + "i", texturePointers[i]);
	   		}
	   		catch (Exception e){
	   			int err = gl.glGetError();
	   			if (err != 0) {
	   				// err == 1280, prints "invalid enum":
	   				System.err.println(GLU.gluErrorString(err));
	   			}
	   			System.err.println(e.getMessage());
	   		}
	   	}
	   	
	   	return texturePointers;
	}
	/**
	 * gets texture pointer with the corresponding tag
	 * @param tag 
	 * @return texture pointer
	 */
	public static int getTexturePointer(String tag)
	{
		return textMap.get(tag);
	}
	
	/**
	 * removes all texture pointers from textureManager storage
	 * doesn't remove actual texture, so if texture is used in a mesh,
	 * that mesh will still hold the texture 
	 */
	public static void clearTextures()
	{
		textMap.clear();
	}
	
	/*class texture{
		int tPointer;
		String tag;
		public texture(int pointer)
		{
			this(pointer, "pic");
		}	
		public texture(int pointer, String tag)
		{
			tPointer = pointer;
			this.tag = tag;
		}
		
		public boolean isTag(String tag){
			return this.tag.equals(tag);
		}
		
		public int getTexturePointer(){
			return tPointer;
		}
		
	}*/
	
}

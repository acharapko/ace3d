package ac.ace3d.meshes;

import java.util.ArrayList;

import ac.ace3d.collisions.Collider;
import ac.ace3d.world.Ace3dObject;

public class ColliderMesh extends TexturedMesh {
	private ArrayList<Collider> colliders;
	
	public ColliderMesh()
	{
		colliders = new ArrayList<Collider>();
	}
	
	public ColliderMesh(String name)
	{
		colliders = new ArrayList<Collider>();
		setName(name);
	}
		
	public void addCollider(Collider c)
	{
		colliders.add(c);
	}
	
	public void setColliders(ArrayList<Collider> c)
	{
		colliders = c;
	}
	
	public int getNumberOfColliders()
	{
		return colliders.size();
	}
	
	public Collider getCollider(int index)
	{
		return colliders.get(index);
	}
	
	public ArrayList<Collider> getColliders(Ace3dObject aceObj)//creates a lot of objects
	{
		ArrayList<Collider> cl = (ArrayList<Collider>) colliders.clone();
		for (int i = 0; i < cl.size(); i++)
			cl.get(i).setAce3dObject(aceObj);
		return cl;
	}
	
	public void useNormalsBuffer(boolean b)
	{
		setUseNormals(b);
	}
}

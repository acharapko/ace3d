package ac.ace3d.meshes;

import java.util.ArrayList;

import ac.ace3d.Vector;
import ac.ace3d.collisions.PlaneCollider;

public class floorPlane extends ColoredMesh {
	
	private float x, y, z;
	private ArrayList<PlaneCollider> colliders = new ArrayList<PlaneCollider>(); 
	public floorPlane() {		
    	//this(10, 10, 1, 1);
    }
 
    public floorPlane(float width, float length) {
		//this(width, length, 1, 1);
    }
    
    /**
     * constructs a floorPlane
     * 
     * 		  l e n g t h   (x-axis)
     * 		w * * * * * *
     * 		i * . . * * *
     * 		d * * - * * *
     * 		t * * - - * *
     * 		h * * * * * *
     * 
     * 		(z)
     * 
     * 		length * scale must be integer
     * 		width * scale must be integer
     */		
 
    public floorPlane(float width, float length, float scale, float[] fc) {    	
    	int wdth = (int) (width * scale);
    	int lngth = (int) (length * scale);
    	float iScale = 1 / scale;
    	float[] vertices = new float[fc.length * 18];
    	//float[] tVertices = new float[6];
    	float l = 0;
    	float h = 0;	
    	for (int i = 0; i <= fc.length - lngth - wdth;  i++)
    	{
    		//first triangle
    		//1 - first
    		int j = i * 18;
    		Vector v1 = new Vector(l, fc[i], h);
    		Vector v2 = new Vector(l + iScale, fc[i + 1], h);
    		Vector v3 = new Vector(l, fc[i + lngth], h + iScale);
    		Vector v4 = new Vector(l + iScale, fc[(i + 1) + lngth], h + iScale);
    		
    		Vector v12 = v2.subtractVector(v1);
    		Vector v13 = v3.subtractVector(v1);
    		
    		Vector v42 = v2.subtractVector(v4);
    		Vector v43 = v3.subtractVector(v4);
    		
    		//used for collider
    		Vector n1 = v12.crossProduct(v13);
    		Vector n2 = v42.crossProduct(v43);
    		
    		//triangle 1
    		vertices[j] = v1.getiComponent();
    		vertices[j + 1] = v1.getjComponent();
    		vertices[j + 2] = v1.getkComponent();
    		
    		vertices[j + 3] = v3.getiComponent();
    		vertices[j + 4] = v3.getjComponent();
    		vertices[j + 5] = v3.getkComponent();
    		
    		vertices[j + 6] = v2.getiComponent();
    		vertices[j + 7] = v2.getjComponent();
    		vertices[j + 8] = v2.getkComponent();
    		
    		//triangle 2
    		vertices[j + 9] = v2.getiComponent();
    		vertices[j + 10] = v2.getjComponent();
    		vertices[j + 11] = v2.getkComponent();
    		
    		vertices[j + 12] = v3.getiComponent();
    		vertices[j + 13] = v3.getjComponent();
    		vertices[j + 14] = v3.getkComponent();
    		
    		vertices[j + 15] = v4.getiComponent();
    		vertices[j + 16] = v4.getjComponent();
    		vertices[j + 17] = v4.getkComponent();
    		
    		/*
    		vertices[j] = l;    		 
    		vertices[j + 1] = fc[i];
    		vertices[j + 2] = h;    		
    		//2
    		//tVertices[0] = l + iScale;
    		//tVertices[1] = fc[i + 1];
    		//tVertices[2] = h;    		
    		//3
    		//tVertices[3] = l;
    		//tVertices[4] = fc[i + lngth];
    		//tVertices[5] = h + iScale;        		
    		//4 - sixth
    		vertices[j + 15] = l + iScale;
    		vertices[j + 16] = fc[(i + 1) + lngth];
    		vertices[j + 17] = h + iScale;    		
    		//second, fifth
    		vertices[j + 3] = vertices[j + 12] = l;
    		vertices[j + 4] = vertices[j + 13] = fc[i + lngth];
    		vertices[j + 5] = vertices[j + 14] = h + iScale;    		
    		//third, forth
    		vertices[j + 6] = vertices[j + 9] = l + iScale;
    		vertices[j + 7] = vertices[j + 10] = fc[i + 1];
    		vertices[j + 8] = vertices[j + 11] = h;    		
    		//fourth
    		//vertices[i + 9] = tVertices[0];
    		//vertices[i + 10] = tVertices[1];
    		//vertices[i + 11] = tVertices[2];    		
    		//fifth
    		//vertices[i + 12] = tVertices[0];
    		//vertices[i + 13] = tVertices[1];
    		//vertices[i + 14] = tVertices[2];    	*/    		
    		
    		
    		
    		l += iScale;
    		if (l + iScale >= width){
    			l = 0;
    			h += iScale;
    		}
    		
    	}
 
    	//setIndices(indices);
    	setVertices(vertices);
    }
    
    //public float[] createQuad()
}

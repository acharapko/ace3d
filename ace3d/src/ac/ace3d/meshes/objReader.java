package ac.ace3d.meshes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.microedition.khronos.opengles.GL10;


import ac.ace3d.Vector;
import ac.ace3d.collisions.Collider;
import ac.ace3d.collisions.TriangularPlateCollider;
import ac.ace3d.floatList;
import ac.ace3d.shortList;
import ac.ace3d.ace3dbytebuffer;
import android.content.res.AssetManager;
import android.content.res.Resources;

public class objReader{
	
	public static  ArrayList<Mesh> processStream(String asset, AssetManager am) throws IOException
	{
		InputStream is = am.open(asset, 1);
		return processStream2(is, false);
	}
	
	public static ArrayList<Mesh> processStream(int resourceID, Resources res) throws IOException
	{
		InputStream is = res.openRawResource(resourceID);
		return processStream2(is, false);
	}
	
	private static ArrayList<Mesh> processStream2(InputStream is, boolean MakeColliders) throws IOException
	{		
		boolean firstMesh = true;
		ArrayList<Mesh> meshObjects = new ArrayList<Mesh>();
		//ace3dbytebuffer mybb = new ace3dbytebuffer();
		String meshName = "";
		floatList vertices = new floatList();
		floatList UVcoordinates = new floatList();
		floatList Normals = new floatList();
		shortList indices = new shortList();
		shortList UVindices = new shortList();
		shortList Normalindices = new shortList();
				
		BufferedReader b = new BufferedReader(new InputStreamReader(is));
		String line = b.readLine();
		//skipping header junk
		
		while (line != null){
			
			//skip comments
			if (line.charAt(0) != '#'){
				
				//if new object
				if (line.charAt(0) == 'o')
				{
					meshName = line.replace("o ", "");
					//if it is not the first mesh, than save previous.
					if (!firstMesh){
						if (UVcoordinates.getLength() > 0){							
							//floatBuffer UVmap = createUVmap(UVcoordinates, UVindices);
							if (!MakeColliders)
								meshObjects.add(createMesh(meshName, vertices, indices, UVindices, 
									UVcoordinates, Normalindices, Normals));	
							else{
								meshObjects.add(createCollidderMesh(meshName, vertices, indices, UVindices, 
										UVcoordinates, Normalindices, Normals));	
							}
						}
					}
					firstMesh = false;
				}
				//if new vertex
				
				else if (line.startsWith("v "))
				{
					line = line.replace("v ", "");	
					StringTokenizer lineTokens = new StringTokenizer(line, " ");
					while(lineTokens.hasMoreTokens())
					{
						float x = Float.parseFloat(lineTokens.nextToken());
						//float z = Float.parseFloat(lineTokens.nextToken());
						float y = Float.parseFloat(lineTokens.nextToken());
						float z = Float.parseFloat(lineTokens.nextToken());
						vertices.appendFloat(x);
						vertices.appendFloat(y);
						vertices.appendFloat(z);
					}
				}
				//uv mapping
				else if  (line.startsWith("vt"))
				{
					line = line.replace("vt ", "");	
					StringTokenizer lineTokens = new StringTokenizer(line, " ");
					//while(lineTokens.hasMoreTokens())
					UVcoordinates.appendFloat(Float.parseFloat(lineTokens.nextToken()));
					UVcoordinates.appendFloat(1.0f - Float.parseFloat(lineTokens.nextToken()));
				}
				//normals
				else if  (line.startsWith("vn"))
				{
					line = line.replace("vn ", "");	
					StringTokenizer lineTokens = new StringTokenizer(line, " ");
					while(lineTokens.hasMoreTokens())
						Normals.appendFloat(Float.parseFloat(lineTokens.nextToken()));					
				}
				
				//if new triangle
				else if (line.charAt(0) == 'f')
				{
					line = line.replace("f ", "");	
					line = line.replace("//", "/n/");
					StringTokenizer lineTokens = new StringTokenizer(line, " ");
					
					//int c = 0;
					while(lineTokens.hasMoreTokens()){
						
						StringTokenizer token = new StringTokenizer(lineTokens.nextToken(), "/");						
						indices.appendShort((short) (Short.parseShort(token.nextToken()) - 1));
						if (token.hasMoreTokens()){	
							String strtmp = token.nextToken();
							if (!strtmp.equals("n"))
								UVindices.appendShort((short) (Short.parseShort(strtmp) - 1));
							if (token.hasMoreTokens())
								Normalindices.appendShort((short) (Short.parseShort(token.nextToken()) - 1));
						}
							
					}					
				}
			}
			line = b.readLine();
		}
		//saving last mesh from the file.
		//if (UVcoordinates.getLength() > 0){							
			//floatBuffer UVmap = createUVmap(UVcoordinates, UVindices);
			if (!MakeColliders)
				meshObjects.add(createMesh(meshName, vertices, indices, UVindices, 
					UVcoordinates, Normalindices, Normals));	
			else{
				meshObjects.add(createCollidderMesh(meshName, vertices, indices, UVindices, 
						UVcoordinates, Normalindices, Normals));	
			}
		//}
		
		return meshObjects;
	}
	
	
	public static  ArrayList<Mesh> getObjectWithCollidersFromOBJ(int resourceID, Resources res){
		InputStream is = res.openRawResource(resourceID);
		try {
			return processStream2(is, true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	/*private static ArrayList<ColliderMesh> getmwc(InputStream is) throws IOException
	{		
		boolean firstMesh = true;
		ColliderMesh ob;
		ArrayList<ColliderMesh> obj = new ArrayList<ColliderMesh>();
		String meshName = "";
		floatList vertices = new floatList();
		floatList UVcoordinates = new floatList();
		floatList Normals = new floatList();
		shortList indices = new shortList();
		shortList UVindices = new shortList();
		shortList Normalindices = new shortList();
		
		
		BufferedReader b = new BufferedReader(new InputStreamReader(is));
		String line = b.readLine();
		//skipping header junk		
		while (line != null){
			
			//skip comments
			if (line.charAt(0) != '#'){
				
				//if new object
				if (line.charAt(0) == 'o')
				{
					meshName = line.replace("o ", "");
					//if it is not the first mesh, than save previous.
					if (!firstMesh){
						if (UVcoordinates.getLength() > 0){							
							//floatBuffer UVmap = createUVmap(UVcoordinates, UVindices);
							ob = createCollidderMesh(meshName, vertices, indices, UVindices, 
									UVcoordinates, Normalindices, Normals);	
							obj.add(ob);
						}
					}
					firstMesh = false;
				}
				//if new vertex
				
				else if (line.startsWith("v "))
				{
					line = line.replace("v ", "");	
					StringTokenizer lineTokens = new StringTokenizer(line, " ");
					while(lineTokens.hasMoreTokens())
					{
						float x = Float.parseFloat(lineTokens.nextToken());
						//float z = Float.parseFloat(lineTokens.nextToken());
						float y = Float.parseFloat(lineTokens.nextToken());
						float z = Float.parseFloat(lineTokens.nextToken());
						vertices.appendFloat(x);
						vertices.appendFloat(y);
						vertices.appendFloat(z);
					}
				}
				//uv mapping
				else if  (line.startsWith("vt"))
				{
					line = line.replace("vt ", "");	
					StringTokenizer lineTokens = new StringTokenizer(line, " ");
					//while(lineTokens.hasMoreTokens())
					UVcoordinates.appendFloat(Float.parseFloat(lineTokens.nextToken()));
					UVcoordinates.appendFloat(1.0f - Float.parseFloat(lineTokens.nextToken()));
				}
				//normals
				else if  (line.startsWith("vn"))
				{
					line = line.replace("vn ", "");	
					StringTokenizer lineTokens = new StringTokenizer(line, " ");
					while(lineTokens.hasMoreTokens())
						Normals.appendFloat(Float.parseFloat(lineTokens.nextToken()));					
				}
				
				//if new triangle
				else if (line.charAt(0) == 'f')
				{
					line = line.replace("f ", "");	
					line = line.replace("//", "/n/");
					StringTokenizer lineTokens = new StringTokenizer(line, " ");
					
					//int c = 0;
					while(lineTokens.hasMoreTokens()){
						
						StringTokenizer token = new StringTokenizer(lineTokens.nextToken(), "/");						
						indices.appendShort((short) (Short.parseShort(token.nextToken()) - 1));
						if (token.hasMoreTokens()){	
							String strtmp = token.nextToken();
							if (!strtmp.equals("n"))
								UVindices.appendShort((short) (Short.parseShort(strtmp) - 1));
							if (token.hasMoreTokens())
								Normalindices.appendShort((short) (Short.parseShort(token.nextToken()) - 1));
						}
							
					}					
				}
			}
			line = b.readLine();
		}
		//saving last mesh from the file.
		//if (UVcoordinates.getLength() > 0){							
			//floatBuffer UVmap = createUVmap(UVcoordinates, UVindices);
			ob = createCollidderMesh(meshName, vertices, indices, UVindices, 
					UVcoordinates, Normalindices, Normals);	
			obj.add(ob);
		//}
		
		return obj;
	}*/
	
	
	
	
	
	private static floatList createUVmap(floatList uvc, shortList uvi)
	{
		floatList uvm = new floatList();
		for (int i = 0; i < uvi.getLength(); i++)
		{
			int j = uvi.get(i);
			j *= 2;
			uvm.appendFloat(uvc.get(j));
			uvm.appendFloat(uvc.get(j + 1));
		}
		return uvm;
		
	}	
	
	
	
	/*private static MeshFromOBJ createMesh(String name, floatBuffer vertices, 
			shortBuffer indices, floatBuffer uvm)
	{
		MeshFromOBJ m = new MeshFromOBJ(name);
		//setting vertices of the last created mesh
		float[] v = new float[vertices.getLength()];
		short[] s = new short[indices.getLength()];
		v = vertices.readBuffer();
		s = indices.readBuffer();
		m.setVertices(v);
		m.setIndices(s);
		if (uvm != null)
			if (uvm.getLength() > 0)
				m.setUVmap(uvm.readBuffer());
		return m;
	}*/

	
	private static Mesh createMesh(String name, floatList vertices, 
			shortList indices, shortList uvIndices, floatList uvm, 
			shortList normIndices, floatList Norm)
	{
		Mesh m;
		//MeshFromOBJ m = new MeshFromOBJ(name);
		floatList orderedVertices = new floatList();
		floatList orderedUVvertices = new floatList();
		floatList fNormals = new floatList();
		//don't need to order it now, but might be useful later for some optimization
		shortList orderedIndices = new shortList();  
		for (int i = 0; i < indices.getLength(); i++)
		{
			int j = indices.get(i); //vertex number in unsorted buffer.
			int k = j * 3;
			orderedVertices.appendFloat(vertices.get(k));
			orderedVertices.appendFloat(vertices.get(k + 1));
			orderedVertices.appendFloat(vertices.get(k + 2));
			//orderedVertices.appendFloat(vertices.get(k + 2));
			
			if (uvIndices != null)
				if (uvIndices.getLength() > 0){
					j = uvIndices.get(i); //UV vertex number in unsorted buffer
					orderedUVvertices.appendFloat(uvm.get(j * 2));
					orderedUVvertices.appendFloat(uvm.get(j * 2 + 1));			
					orderedIndices.appendShort((short)i);
					//m.setUVmap(orderedUVvertices.readBuffer());		
				}
			if (normIndices != null)
				if (normIndices.getLength() > 0){
					j = normIndices.get(i); //normal index
					fNormals.appendFloat(Norm.get(j * 3));
					fNormals.appendFloat(Norm.get(j * 3 + 1));
					fNormals.appendFloat(Norm.get(j * 3 + 2));
				}
		}
		if (orderedUVvertices.getLength() > 0){
			m = new TexturedMesh();
			((TexturedMesh)m).setUVmap(orderedUVvertices.readBuffer());	
		}
		else
			m = new ColoredMesh();
		
		m.setVertices(orderedVertices.readBuffer());
		
		if (fNormals.getLength() > 0){
			m.setNormals(fNormals.readBuffer());	
			m.setUseNormals(true);
		}
		
		//m.setIndices(orderedIndices.readBuffer());		
		return m;
		
	}
	
	
	private static ColliderMesh createCollidderMesh(String name, floatList vertices, 
			shortList indices, shortList uvIndices, floatList uvm, 
			shortList normIndices, floatList Norm)
	{
		ColliderMesh m = new ColliderMesh();
		
		//ArrayList<Collider> col = new ArrayList<Collider>();
		floatList orderedVertices = new floatList();
		floatList orderedUVvertices = new floatList();
		floatList fNormals = new floatList();
		//don't need to order it now, but might be useful later for some optimization
		shortList orderedIndices = new shortList();  
		for (int i = 0; i < indices.getLength(); i+=3)
		{
			
			int j = indices.get(i) * 3; //vertex number in unsorted buffer.
			
			Vector v1 = new Vector(vertices.get(j++), vertices.get(j++), 
					vertices.get(j++));
			j = indices.get(i + 1) * 3; //vertex number in unsorted buffer.
			Vector v2 = new Vector(vertices.get(j++), vertices.get(j++), 
					vertices.get(j++));
			j = indices.get(i + 2) * 3; //vertex number in unsorted buffer.
			Vector v3 = new Vector(vertices.get(j++), vertices.get(j++), 
					vertices.get(j++));
			
			orderedVertices.appendFloat(v1.getiComponent());
			orderedVertices.appendFloat(v1.getjComponent());
			orderedVertices.appendFloat(v1.getkComponent());
			
			orderedVertices.appendFloat(v2.getiComponent());
			orderedVertices.appendFloat(v2.getjComponent());
			orderedVertices.appendFloat(v2.getkComponent());
			
			orderedVertices.appendFloat(v3.getiComponent());
			orderedVertices.appendFloat(v3.getjComponent());
			orderedVertices.appendFloat(v3.getkComponent());
			//orderedVertices.appendFloat(vertices.get(k + 2));
			
			if (uvIndices != null)
				if (uvIndices.getLength() > 0){
					j = uvIndices.get(i); //UV vertex number in unsorted buffer
					orderedUVvertices.appendFloat(uvm.get(j * 2));
					orderedUVvertices.appendFloat(uvm.get(j * 2 + 1));			
					orderedIndices.appendShort((short)i);
					
					j = uvIndices.get(i + 1); //UV vertex number in unsorted buffer
					orderedUVvertices.appendFloat(uvm.get(j * 2));
					orderedUVvertices.appendFloat(uvm.get(j * 2 + 1));			
					orderedIndices.appendShort((short)(i + 1));
					
					j = uvIndices.get(i + 2); //UV vertex number in unsorted buffer
					orderedUVvertices.appendFloat(uvm.get(j * 2));
					orderedUVvertices.appendFloat(uvm.get(j * 2 + 1));			
					orderedIndices.appendShort((short)(i + 2));
					//m.setUVmap(orderedUVvertices.readBuffer());		
				}
			if (normIndices != null)
				if (normIndices.getLength() > 0){
					j = normIndices.get(i); //normal index
					float h1 = Norm.get(j * 3);
					float h2 = Norm.get(j * 3 + 1);
					float h3 = Norm.get(j * 3 + 2);
					fNormals.appendFloat(h1);
					fNormals.appendFloat(h2);
					fNormals.appendFloat(h3);
					
					//repeat since triangle has same normals at each vertex
					fNormals.appendFloat(h1);
					fNormals.appendFloat(h2);
					fNormals.appendFloat(h3);
					
					fNormals.appendFloat(h1);
					fNormals.appendFloat(h2);
					fNormals.appendFloat(h3);
					
					/*j = normIndices.get(i); //normal index
					fNormals.appendFloat(Norm.get(j * 3));
					fNormals.appendFloat(Norm.get(j * 3 + 1));
					fNormals.appendFloat(Norm.get(j * 3 + 2));*/
				
					
					Vector n = new Vector(h1, h2, h3);	
					Vector leg1 = v2.subtractVector(v1); 
					Vector leg2 = v3.subtractVector(v1); 
					TriangularPlateCollider rtpc = 
						new TriangularPlateCollider(null, v1, n, leg1, leg2);
					//rtpc.setMaxDimension((leg1.getLength() > leg2.getLength()) ? leg1.getLength() : leg2.getLength());
					m.addCollider(rtpc);
				}
		}
		m.setVertices(orderedVertices.readBuffer());
		if (orderedUVvertices.getLength() > 0)
			m.setUVmap(orderedUVvertices.readBuffer());	
		if (fNormals.getLength() > 0){
			m.setNormals(fNormals.readBuffer());	
			m.useNormalsBuffer(true);
		}
		//o.setMesh(m);
		
		//m.setIndices(orderedIndices.readBuffer());		
		return m;
		
	}
}

package ac.ace3d.meshes;

import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.world.Camera;

public interface Renderable {

	/** render
	 * 		renders the mesh of the Renderable object
	 * 
	 * @param gl
	 * 		OpenGL ES context
	 */			
	public void render(GL10 gl);
	
	//public void render(GL10 gl, Camera c);
}

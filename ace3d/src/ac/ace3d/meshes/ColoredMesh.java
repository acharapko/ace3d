package ac.ace3d.meshes;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

public class ColoredMesh extends Mesh {

	protected FloatBuffer colorBuffer = null;
	

	private boolean useFlatColor = false;
	private boolean useBlending = false;
	// Flat Color
	private float[] rgba = new float[]{1.0f, 0.0f, 0.0f, 1.0f};     
	   
	   
	public ColoredMesh()
	{
		super();
	}
	
	public void useBlending(boolean blend)
	{
		useBlending = blend;
	}
	   
	public void useFlatColor(boolean useColor)
	{
		useFlatColor = useColor;
	}
	   
	@Override
	public void render(GL10 gl) {
		// Counter-clockwise winding.
		//gl.glFrontFace(winding);
		
		//setting material properties
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_AMBIENT, mtlAmbient, 0);
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_DIFFUSE, mtlDiffuse, 0);
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SPECULAR, mtlSpecular, 0);
		gl.glMaterialf(GL10.GL_FRONT_AND_BACK, GL10.GL_SHININESS, 20.0f);
		
		
		gl.glEnable(GL10.GL_CULL_FACE);
		// What faces to remove with the face culling.
		gl.glCullFace(faceCulling);
		
		// Enabled the vertices buffer for writing and to be used during
		// rendering.
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		// Specifies the location and data format of an array of vertex
		// coordinates to use when rendering.
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, verticesBuffer);
		// Set flat color
		if (useFlatColor)
			gl.glColor4f(rgba[0], rgba[1], rgba[2], rgba[3]);
		if (useBlending)
		{
			gl.glEnable(GL10.GL_BLEND);							//Enable blending
			gl.glColor4f(rgba[0], rgba[1], rgba[2], rgba[3]); //Flat Color blending
		}
		// Smooth color
		if (colorBuffer != null) {
			// Enable the color array buffer to be used during rendering.
			gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
			gl.glColorPointer(4, GL10.GL_FLOAT, 0, colorBuffer);
		}
		
		if (UseNormals == true){
			gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);
			gl.glNormalPointer(GL10.GL_FLOAT, 0, NormalBuffer);
		}		
		
		/*Log.d("tx", tx + "");
		Log.d("ty", ty + "");
		Log.d("tz", tz + "");
		Log.d("mesh","-----");*/
		gl.glTranslatef(tx, ty, tz);
		
		gl.glRotatef(ry, 0, 1, 0);
		//Log.d("mesh.ry", ry + "");
		gl.glRotatef(rz, 0, 0, 1);
		//Log.d("mesh.rz", rz + "");
		
		gl.glRotatef(rx, 1, 0, 0);
		//Log.d("mesh.rx", rx + "");
		//gl.glRotatef(rv, rVector.getiComponent(), rVector.getjComponent(), rVector.getkComponent());

		if (!useIndices)
			gl.glDrawArrays(drawingMode, 0, verticesBuffer.capacity() / 3);
		else
			gl.glDrawElements(GL10.GL_TRIANGLES, numOfIndices,
				GL10.GL_UNSIGNED_SHORT, indicesBuffer);
		// Disable the vertices buffer.
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		//Disable blending
		gl.glDisable(GL10.GL_BLEND);		
		
		// Disable face culling.
		gl.glDisable(GL10.GL_CULL_FACE);

	}
	
	protected void setColor(float red, float green, float blue, float alpha) {
		// Setting the flat color.
		rgba[0] = red;
		rgba[1] = green;
		rgba[2] = blue;
		rgba[3] = alpha;
	}
	    
	protected void setColors(float[] colors) {
		// float has 4 bytes.
	   	ByteBuffer cbb = ByteBuffer.allocateDirect(colors.length * 4);
	   	cbb.order(ByteOrder.nativeOrder());
	   	colorBuffer = cbb.asFloatBuffer();
	   	colorBuffer.put(colors);
	   	colorBuffer.position(0);
	}

}

package ac.ace3d.meshes;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import javax.microedition.khronos.opengles.GL10;

import ac.ace3d.ACE3D;
import ac.ace3d.Vector;
import android.graphics.*;
import android.opengl.GLU;
import android.opengl.GLUtils;
import android.util.Log;

public abstract class Mesh implements Renderable{	
	
	// Translate params.
	protected float tx = 0;
	protected float ty = 0; 
	protected float tz = 0;

   // Rotate params.
	protected float rx = 0;
	protected float ry = 0;
	protected float rz = 0;
	protected float rv = 0;
   
   private Vector rVector = new Vector();
	
	// buffer.
   protected FloatBuffer verticesBuffer = null;
   protected ShortBuffer indicesBuffer = null; //not used
   protected FloatBuffer NormalBuffer = null;
      
   
   private String meshName = "";
   //private int textureID = -1;
   
   private int wraps, wrapt;

   // The number of indices.
   protected int numOfIndices = -1;
   protected boolean useIndices = false;
   protected  boolean UseNormals = false; 
   
 //Material properties
   protected float mtlAmbient[] = { 0.6f, 0.6f, 0.6f, 1.0f };
   protected float mtlDiffuse[] = { 0.64f, 0.64f, 0.64f, 1.0f };
   protected float mtlSpecular[] = { 0.5f, 0.5f, 0.5f, 1.0f };
   
   protected int winding;
   protected int drawingMode;   
   protected int faceCulling;
   
   protected Mesh()
   {
   		winding = ACE3D.ACE3D_COUNTERCLOCKWISE;
   		drawingMode = GL10.GL_TRIANGLES; 
   		faceCulling = ACE3D.ACE3D_CULL_FACE_BACK;
   		wraps = GL10.GL_REPEAT;
   		wrapt = GL10.GL_REPEAT;
   }
   
   protected void setDrawingMode(int dm)
   {
	   drawingMode = dm; 
   }
   
   protected void setUseNormals(boolean UseNormals)
   {
	   this.UseNormals = UseNormals;
   }  
   
   public void setFaceCulling(int parameter)
   {	   
	   switch(parameter){
	   		case ACE3D.ACE3D_CULL_FACE_BACK:
	   			faceCulling = ACE3D.ACE3D_CULL_FACE_BACK;
	   			break;
	   		case ACE3D.ACE3D_CULL_FACE_FRONT:
	   			faceCulling = ACE3D.ACE3D_CULL_FACE_FRONT;
	   			break;
	   		case ACE3D.ACE3D_CULL_FACE_NONE:
	   			faceCulling = ACE3D.ACE3D_CULL_FACE_NONE;
	   			break;      		
	   }	   
   }
   
   public void setName(String name)
   {
	   meshName = name;;
   }
   public String getName(){
		return meshName;
	}
   
   public void setWinding(int parameter)
   {	   
	   switch(parameter){
	   		case ACE3D.ACE3D_CLOCKWISE:
	   			winding = ACE3D.ACE3D_CLOCKWISE;
	   			break;
	   		case ACE3D.ACE3D_COUNTERCLOCKWISE:
	   			winding = ACE3D.ACE3D_COUNTERCLOCKWISE;
	   			break;	   		   		
	   }	   
   }
   
   public void setParam(int parameter)
   {
	   switch(parameter){
  		case ACE3D.ACE3D_USE_VERTEX_INDICES:
  			useIndices = true;
  			break;
  		case ACE3D.ACE3D_NOT_USE_VERTEX_INDICES:
  			useIndices = false;
  			break;
	   }
   }
   /*/**
    * @deprecated
    * @param b
    */
   /*public void setTexture(Bitmap b)
   {
	    texture = new Bitmap[1];
   		//texture[0] = b.copy(b.getConfig(), false);    	
	    texture[0] = b;
   }*/
   /*/**
    * @deprecated
    * @param b
    */
   /*public void setTexture(Bitmap[] b)
   {
	   texture = b;
   }*/
   
  
   
   /*/**
    * @deprecated
    * might not work
    * @param gl
    * @param frame
    * @param b
    */
   /* public void reloadFrame(GL10 gl, int frame, Bitmap b)
   {
	   texture[frame] = b;
	   gl.glBindTexture(GL10.GL_TEXTURE_2D, texturePointers[frame]);  		
	   gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
			   GL10.GL_LINEAR);
	   gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
			   GL10.GL_LINEAR);  	 
	   gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
			   wraps);
	   gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
			   wrapt);  	 
	   // load bitmap
	   try{
		   GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, texture[frame], 0);
	   }
	   catch (Exception e){
		   int err = gl.glGetError();
		   if (err != 0) {
			   // err == 1280, prints "invalid enum":
			   System.err.println(GLU.gluErrorString(err));
		   }
		   System.err.println(e.getMessage());
	   }
   }*/
   
   /*/**
    * @deprecated
    * sets texture wrap parameters.
    * see OpenGL ES
    * GL_REPEAT 
    * GL_CLAMP_TO_EDGE
    * @param wrap_s
    * @param wrap_t
    */
  /* public void setTextureParameter(int wrap_s, int wrap_t)
   {
    	wraps = wrap_s;
    	wrapt = wrap_t;
   }*/
   
   public void translate(float tx, float ty, float tz)
   {
	   this.tx = tx;
	   this.ty = ty;
	   this.tz = tz;
   }
   
   public void translateX(float units)
   {
   	tx = units;
   }
   public void translateY(float units)
   {
   	ty = units;
   }
   public void translateZ(float units)
   {
   	tz = units;
   }
   
   public void rotateV(Vector v, float angle)
   {
	   rVector = v.normilize();
	   rv = angle;
   }
   
   public void rotateX(float angle)
   {
   	rx = angle;
   }
   
   public void rotateY(float angle)
   {
   	ry = angle;
   }
   public void rotateZ(float angle)
   {
   	rz = angle;
   }   

   /*public void render(GL10 gl) {
		// Counter-clockwise winding.
		//gl.glFrontFace(winding);
		
		//setting material properties
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_AMBIENT, mtlAmbient, 0);
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_DIFFUSE, mtlDiffuse, 0);
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SPECULAR, mtlSpecular, 0);
		gl.glMaterialf(GL10.GL_FRONT_AND_BACK, GL10.GL_SHININESS, 20.0f);
		
		
		
		// Enable face culling if needed.
		//change1------
		//if ((faceCulling == ACE3D.ACE3D_CULL_FACE_BACK)||(faceCulling == ACE3D.ACE3D_CULL_FACE_FRONT)){
		//------------
			gl.glEnable(GL10.GL_CULL_FACE);
			// What faces to remove with the face culling.
			gl.glCullFace(faceCulling);
		//------	
		//}
		//------
		// Enabled the vertices buffer for writing and to be used during
		// rendering.
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		// Specifies the location and data format of an array of vertex
		// coordinates to use when rendering.
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, verticesBuffer);
		// Set flat color
		if (useFlatColor)
			gl.glColor4f(rgba[0], rgba[1], rgba[2], rgba[3]);
		if (useBlending)
		{
			gl.glEnable(GL10.GL_BLEND);							//Enable blending
			gl.glColor4f(rgba[0], rgba[1], rgba[2], rgba[3]); //Flat Color blending
		}
		// Smooth color
		if (colorBuffer != null) {
			// Enable the color array buffer to be used during rendering.
			gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
			gl.glColorPointer(4, GL10.GL_FLOAT, 0, colorBuffer);
		}
		
		if (UseNormals == true){
			gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);
			gl.glNormalPointer(GL10.GL_FLOAT, 0, NormalBuffer);
		}		
		
		if (UVmap != null) {
			//change 2----------
			/*if (!textureloaded) {
				loadTexture(gl);
				textureloaded = true;
			}*/
			//------------------
			/*gl.glEnable(GL10.GL_TEXTURE_2D);
			// Enable the texture state
			gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

			// Point to our buffers
			gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, UVmap);
			gl.glBindTexture(GL10.GL_TEXTURE_2D, texturePointers[currentFrame]);
		}
		
		/*Log.d("tx", tx + "");
		Log.d("ty", ty + "");
		Log.d("tz", tz + "");
		Log.d("mesh","-----");*/
		/*gl.glTranslatef(tx, ty, tz);
		
		gl.glRotatef(ry, 0, 1, 0);
		//Log.d("mesh.ry", ry + "");
		gl.glRotatef(rz, 0, 0, 1);
		//Log.d("mesh.rz", rz + "");
		
		gl.glRotatef(rx, 1, 0, 0);
		//Log.d("mesh.rx", rx + "");
		//gl.glRotatef(rv, rVector.getiComponent(), rVector.getjComponent(), rVector.getkComponent());

		if (!useIndices)
			gl.glDrawArrays(drawingMode, 0, verticesBuffer.capacity() / 3);
		else
			gl.glDrawElements(GL10.GL_TRIANGLES, numOfIndices,
				GL10.GL_UNSIGNED_SHORT, indicesBuffer);
		// Disable the vertices buffer.
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisable(GL10.GL_BLEND);							//Disable blending
		
		if (UVmap != null) {
			gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
			gl.glDisable(GL10.GL_TEXTURE_2D);
		}
		

		// Disable face culling.
		gl.glDisable(GL10.GL_CULL_FACE);
	}*/
   
   public abstract void render(GL10 gl);
   
   /*/**
    * @deprecated
    * @param gl
    */
   /*private void loadTexture(GL10 gl)
   {
   	// Generate texture pointers
	  texturePointers = new int[texture.length];
	  gl.glGenTextures(texture.length, texturePointers, 0);
   	
   	 
   	for (int i = 0; i < texture.length; i++){
   		
   		gl.glBindTexture(GL10.GL_TEXTURE_2D, texturePointers[i]);
   	 
   		
   		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
   					GL10.GL_LINEAR);
   		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
   					GL10.GL_LINEAR);
   	 
   		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
   							wraps);
   		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
   							wrapt);
   	 
   		// load bitmap
   		try{
   			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, texture[i], 0);
   			//texture[i].recycle();
   		}
   		catch (Exception e){
   			int err = gl.glGetError();
   			if (err != 0) {
   				// err == 1280, prints "invalid enum":
   				System.err.println(GLU.gluErrorString(err));
   			}
   			System.err.println(e.getMessage());
   		}
   	}
   	//texture.recycle();
   	textureloaded = true;
   }*/
   
   protected void setVertices(float[] vertices) {
   	ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
   	vbb.order(ByteOrder.nativeOrder());
   	verticesBuffer = vbb.asFloatBuffer();
   	verticesBuffer.put(vertices);
   	verticesBuffer.position(0);
   }
    
   protected void setIndices(short[] indices) {
	   	ByteBuffer ibb = ByteBuffer.allocateDirect(indices.length * 2);
	   	ibb.order(ByteOrder.nativeOrder());
	   	indicesBuffer = ibb.asShortBuffer();
	   	indicesBuffer.put(indices);
	   	indicesBuffer.position(0);
	   	numOfIndices = indices.length;
   }   
   
   
   protected void setNormals(float[] normals) {
	   	ByteBuffer ibb = ByteBuffer.allocateDirect(normals.length * 4);
	   	ibb.order(ByteOrder.nativeOrder());
	   	NormalBuffer = ibb.asFloatBuffer();
	   	NormalBuffer.put(normals);
	   	NormalBuffer.position(0);
	   	
   }    
   

}




/*public class Mesh {
	
	
	
	 // Translate params.
	private float tx = 0;
   private float ty = 0;
   private float tz = 0;

   // Rotate params.
   private float rx = 0;
   private float ry = 0;
   private float rz = 0;
	
	// Our vertex buffer.
   private FloatBuffer verticesBuffer = null;
   private FloatBuffer UVmap = null;
   private ShortBuffer indicesBuffer = null;
   private FloatBuffer colorBuffer = null;
   
   private Bitmap texture;
   private boolean textureloaded = false;
   private int textureID = -1;

   
   private Face[] faces;
   private int faceIndex = 0;
   // The number of indices.
   private int numOfIndices = -1;

   // Flat Color
   private float[] rgba = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
   
   float textureCoordinates[] = { 0.75f, 0.5f, //
								   0.75f, 0.75f, //
								   1.0f, 0.75f, //
								   1.0f, 0.5f, //
								   0.5f, 0.5f,
								   0.25f, 0.5f,
								   0.25f, 0.75f,
								   0.5f, 0.75f,
								   0.1f, 0.1f,
								   0.5f, 0.5f,
								   0.5f, 0.75f,
								   0.75f, 0.75f,
	};

   // Smooth Colors
   
   
   private int winding;
   private int drawingMode;
   
   public Mesh()
   {
   	winding = GL10.GL_CCW;
   	drawingMode = GL10.GL_TRIANGLES;    	
   }
   
   public void setTexture(Bitmap b)
   {
   	texture = b;    	
   }
   
   public void translateX(float units)
   {
   	tx = units;
   }
   public void translateY(float units)
   {
   	ty = units;
   }
   public void translateZ(float units)
   {
   	tz = units;
   }
   
   public void rotateX(float angle)
   {
   	rx = angle;
   }
   
   public void rotateY(float angle)
   {
   	ry = angle;
   }
   public void rotateZ(float angle)
   {
   	rz = angle;
   }

   public void draw(GL10 gl) {
		// Counter-clockwise winding.
		gl.glFrontFace(GL10.GL_CCW);
		// Enable face culling.
		gl.glEnable(GL10.GL_CULL_FACE);
		// What faces to remove with the face culling.
		gl.glCullFace(GL10.GL_BACK);
		// Enabled the vertices buffer for writing and to be used during
		// rendering.
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		// Specifies the location and data format of an array of vertex
		// coordinates to use when rendering.
		////gl.glVertexPointer(3, GL10.GL_FLOAT, 0, verticesBuffer);
		// Set flat color
		gl.glColor4f(rgba[0], rgba[1], rgba[2], rgba[3]);
		// Smooth color
		if (colorBuffer != null) {
			// Enable the color array buffer to be used during rendering.
			gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
			gl.glColorPointer(4, GL10.GL_FLOAT, 0, colorBuffer);
		}

		// New part...
		if (!textureloaded) {
			loadTexture(gl);
			textureloaded = true;
		}
		if (textureID != -1) {
			gl.glEnable(GL10.GL_TEXTURE_2D);
			// Enable the texture state
			gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

			// Point to our buffers
			////gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, UVmap);
			gl.glBindTexture(GL10.GL_TEXTURE_2D, textureID);
		}
		// ... end new part.

		gl.glTranslatef(tx, ty, tz);
		gl.glRotatef(rx, 1, 0, 0);
		gl.glRotatef(ry, 0, 1, 0);
		gl.glRotatef(rz, 0, 0, 1);

		for (int i = 0; i < faces.length; i++)
			if (faces[i] != null)
				faces[i].draw(gl);
		////gl.glDrawElements(GL10.GL_TRIANGLES, numOfIndices,
				////GL10.GL_UNSIGNED_SHORT, indicesBuffer);
		// Disable the vertices buffer.
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

		// New part...
		if (textureID != -1 && UVmap != null) {
			gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		}
		// ... end new part.

		// Disable face culling.
		gl.glDisable(GL10.GL_CULL_FACE);
	}
   
   
   private void loadTexture(GL10 gl)
   {
   	// Generate one texture pointer...
   	int[] textures = new int[1];
   	gl.glGenTextures(1, textures, 0);
   	textureID = textures[0];
   	 
   	// ...and bind it to our array
   	gl.glBindTexture(GL10.GL_TEXTURE_2D, textureID);
   	 
   	// Create Nearest Filtered Texture
   	gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
   				GL10.GL_LINEAR);
   	gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
   				GL10.GL_LINEAR);
   	 
   	// Different possible texture parameters, e.g. GL10.GL_CLAMP_TO_EDGE
   	gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
   				GL10.GL_CLAMP_TO_EDGE);
   	gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
   				GL10.GL_REPEAT);
   	 
   	// Use the Android GLUtils to specify a two-dimensional texture image
   	// from our bitmap 
   	try{
   		GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, texture, 0);
   	}
   	catch (Exception e){
   		int err = gl.glGetError();
   		if (err != 0) {
           // err == 1280, prints "invalid enum":
   			System.err.println(GLU.gluErrorString(err));
   		}
   		System.err.println(e.getMessage());
   	}
   	
   	textureloaded = true;
   }
   public void setNumberOfFaces(int number)
   {
   	faces = new Face[number];
   }
   
   public void setFace(float[] vertices, short[] indices, float[] uvmap, float[] colors)
   {
   	Face f = new Face();
   	f.setVertices(vertices);
   	f.setIndices(indices);
   	if (uvmap != null)
   		f.setUVmap(uvmap);
   	if (colors != null)
   		f.setColors(colors);
   	faces[faceIndex++] = f;
   }
   public void setFaces(Face[] f)
    {
   	 faces = f;
    }
    
   public void addFace(Face f)
    {
   	 faces[faceIndex++] = f;
    }
   
   protected void setVertices(float[] vertices) {
   	ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
   	vbb.order(ByteOrder.nativeOrder());
   	verticesBuffer = vbb.asFloatBuffer();
   	verticesBuffer.put(vertices);
   	verticesBuffer.position(0);
   }
    
   protected void setIndices(short[] indices) {
   	ByteBuffer ibb = ByteBuffer.allocateDirect(indices.length * 2);
   	ibb.order(ByteOrder.nativeOrder());
   	indicesBuffer = ibb.asShortBuffer();
   	indicesBuffer.put(indices);
   	indicesBuffer.position(0);
   	numOfIndices = indices.length;
   }
   
   protected void setUVmap(float[] uvmap) {
   	uvmap = textureCoordinates;
   	ByteBuffer ibb = ByteBuffer.allocateDirect(uvmap.length * 4);
   	ibb.order(ByteOrder.nativeOrder());
   	UVmap = ibb.asFloatBuffer();
   	UVmap.put(uvmap);
   	UVmap.position(0);
   	
   }
    
   protected void setColor(float red, float green, float blue, float alpha) {
       // Setting the flat color.
       rgba[0] = red;
       rgba[1] = green;
       rgba[2] = blue;
       rgba[3] = alpha;
   }
    
   protected void setColors(float[] colors) {
   	// float has 4 bytes.
   	ByteBuffer cbb = ByteBuffer.allocateDirect(colors.length * 4);
   	cbb.order(ByteOrder.nativeOrder());
   	colorBuffer = cbb.asFloatBuffer();
   	colorBuffer.put(colors);
   	colorBuffer.position(0);
   }

}*/










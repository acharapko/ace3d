package ac.ace3d;

public class intQueue{
	
	private int[] elements;
	int pushIndex, popIndex;
	
	public intQueue()
	{
		elements = new int[16];
	}
	
	public void push(int i)
	{
		elements[pushIndex++] = i;
		if (pushIndex > elements.length)
		{
			int[] el = new int[elements.length * 2];
			for (int j = popIndex; j < elements.length; j++){
				el[j - popIndex] = elements[j];
				popIndex = 0;
			}
			elements = el;
			pushIndex = 0;
		}
		
	}
	
	public int pop()
	{
		return elements[popIndex++];
	}
	
	public int peek()
	{
		return elements[popIndex];
	}
	
	public int getSize()
	{
		return pushIndex - popIndex;
	}

}

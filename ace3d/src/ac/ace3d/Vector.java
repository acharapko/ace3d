package ac.ace3d;

import android.util.Log;

public class Vector implements Cloneable {

	private static final Vector bVec = new Vector();
	private float i, j, k;
	
	private static float[] tempV = new float[3];
	
	public Vector()
	{
		this(0,0,0); //default Vector
	}
	
	public Vector(float[] v)
	{
		if (v.length == 3){
			i = v[0];
			j = v[1];
			k = v[2];
		}
		else
		{
			i = 0;
			j = 0;
			j = 1;
		}
				
	}
	public Vector(float x, float y, float z)
	{
		i = x;
		j = y;
		k = z;
	}
	
	public static final Vector getBlancVector()
	{
		return bVec;
	}
	/*
	private float[] Vect;
	public Vector()
	{
		this(0,0,0); //default Vector
	}
	
	public Vector(float[] v)
	{
		if (v.length == 3)
			Vect = v;
		else
		{
			Vect = new float[3];
			i = 0;
			Vect[1] = 0;
			Vect[2] = 1;
		}
				
	}
	public Vector(float x, float y, float z)
	{
		Vect = new float[3];
		i = x;
		Vect[1] = y;
		Vect[2] = z;
	}*/
	/**
	 * resets vector to be a Zero-vector
	 */
	public void resetVector()
	{
		i = 0;
		j = 0;
		k = 0;
	}
	/**
	 * sets vector if it is a Zero-vector
	 * @param v
	 * Vector parameter.
	 */
	public void setVector(float[] v)
	{
		if (getLength() == 0)
			if (v.length == 3){
				i = v[0];
				j = v[1];
				k = v[2];
			}	
	}
	
	
	/**
	 * resets vector to a specified value
	 * @param v
	 * Vector parameter.
	 */
	public void resetVector(float[] v)
	{		
		if (v.length == 3){
			i = v[0];
			j = v[1];
			k = v[2];
		}	
	}
	
	public void resetVector(float iComponent, float jComponent, float kComponent)
	{	
		i = iComponent;
		j = jComponent;
		k = kComponent;
	}
	
	/**
	 * 
	 * @return
	 * true if vector has no length, false if vector is not zero
	 */
	
	public boolean isZero()
	{
		boolean r = true;
		if (getLength() > 0)
			r = false;		
		return r;
	}
	
	/**getiComponent()
	 * 
	 * @return
	 * 		returns i component of a vector (along the x-axis)
	 */
	public float getiComponent()
	{
		return i;
	}
	/**getjComponent()
	 * 
	 * @return
	 * 		returns j component of a vector (along the y-axis)
	 */
	public float getjComponent()
	{
		return j;
	}
	/**getkComponent()
	 * 
	 * @return
	 * 		returns k component of a vector (along the z-axis)
	 */
	public float getkComponent()
	{
		return k;
	}
	/** dotProduct(Vector v)
	 * 
	 * @param v
	 * 		Second Vector used to calculate dot product
	 * @return
	 * 		dot product of the vector and the vector v
	 */
	public float dotProduct(Vector v)
	{
		return v.getiComponent() * i + v.getjComponent() * j 
		            + v.getkComponent() * k;
	}
	
	
	/** crossProduct(Vector v)
	 * 
	 * @param v
	 * 		Second Vector used to calculate cross product. 
	 * 		Order of vectors matter for cross product. 
	 * @return
	 * 		cross product of the vector and the vector v
	 */
	
	public Vector crossProduct(Vector v)
	{		
		tempV[0] = j * v.getkComponent() - k * v.getjComponent();
		tempV[1] = k * v.getiComponent() - i * v.getkComponent();
		tempV[2] = i * v.getjComponent() - j * v.getiComponent();		
		return new Vector(tempV);
		
	}
	/** dotProduct(Vector v1, Vector v2)
	 * 
	 * @param v1
	 * 		First vector for calculating dot product
	 * @param v2
	 * 		Second vector for calculating dot product
	 * @return
	 * 		dot product of v1 and v2
	 */
	public static float dotProduct(Vector v1, Vector v2)
	{
		return v1.getiComponent() * v2.getiComponent() + v1.getjComponent() * v2.getjComponent() 
		         + v1.getkComponent() * v2.getkComponent();
	}
	/** crossProduct(Vector v1, Vector v2)
	 * 
	 * @param v1
	 * 		First vector for calculating cross product
	 * @param v2
	 *		Second vector for calculating cross product
	 * @return
	 * 		Cross product (Vector multiplication) vector of v1 x v2. 
	 * 		order matters. v1 x v2 != v2 x v1
	 */
	public static Vector crossProduct(Vector v1, Vector v2)
	{
		
		tempV[0] = v1.getjComponent() * v2.getkComponent() - v1.getkComponent() * v2.getjComponent();
		tempV[1] = v1.getkComponent() * v2.getiComponent() - v1.getiComponent() * v2.getkComponent();
		tempV[2] = v1.getiComponent() * v2.getjComponent() - v1.getjComponent() * v2.getiComponent();
		
		return new Vector(tempV);
	}
	
	public static float getLength(float i, float j, float k)
	{
		return (float)(Math.sqrt(Math.pow(i, 2) + Math.pow(j, 2) + Math.pow(k, 2)));
	}
	
	/**
	 * 
	 * @return
	 * 		Length of the Vector
	 */
	
	public float getLength()
	{
		return (float)(Math.sqrt(Math.pow(i, 2) + Math.pow(j, 2) + Math.pow(k, 2)));
	}
	
	/**
	 * 
	 * @return
	 * 	length of the the projection on XY plane
	 */
	public float getLengthXY()
	{
		return (float)(Math.sqrt(Math.pow(i, 2) + Math.pow(j, 2)));
	}
	/**
	 * 
	 * @return
	 * 	length of the the projection on XZ plane
	 */
	
	public float getLengthXZ()
	{
		return (float)(Math.sqrt(Math.pow(i, 2) + Math.pow(k, 2)));
	}
	
	/**
	 * 
	 * @return
	 * 	length of the the projection on YZ plane
	 */	
	public float getLengthYZ()
	{
		return (float)(Math.sqrt(Math.pow(j, 2) + Math.pow(k, 2)));
	}
	/**
	 * not correct!!!
	 * @param angle
	 */
	public void rotateX(float angle)
	{
		angle = (float)(angle * Math.PI / 180);
		float length = getLengthYZ();
		if (length != 0){
			float cosTheta = j / length;
			float sinTheta = k / length;
			//float theta = (float)(Math.acos(cosTheta)); //in radians
			float theta = getArcCosine(cosTheta, sinTheta); 
			theta = theta + angle;
			cosTheta = (float) (Math.cos(theta));
			sinTheta = (float) (Math.sin(theta));
			j = cosTheta * length;
			k = sinTheta * length;
		}
	}
	/**
	 * not correct!!!
	 * @param angle in degrees
	 */
	public void rotateY(float angle)
	{
		angle = (float)(angle * Math.PI / 180);
		float length = getLengthXZ();
		if (length != 0){
			float cosTheta = i / length;
			float sinTheta = k / length;			
			//float theta = (float)(Math.acos(cosTheta)); //in radians
			float theta = getArcCosine(cosTheta, sinTheta);
			//Log.d("length:", length + "");
			//Log.d("thetaOld:", theta + "");
			theta = theta + angle;		
			cosTheta = (float) (Math.cos(theta));
			sinTheta = (float) (Math.sin(theta));			
			i = cosTheta * length;
			k = sinTheta * length;
		}
	}
	/**
	 * not correct!!!
	 * @param angle
	 * 		angle, in degrees to rotate around z-axis
	 */
	public void rotateZ(float angle)
	{
		angle = (float)(angle * Math.PI / 180);
		float length = getLengthXY();
		if (length != 0){
			float cosTheta = i / length;
			float sinTheta = j / length;
			//float theta = (float)(Math.acos(cosTheta)); //in radians
			float theta = getArcCosine(cosTheta, sinTheta); 
			theta = theta + angle;
			cosTheta = (float) (Math.cos(theta));
			sinTheta = (float) (Math.sin(theta));
			i = cosTheta * length;
			j = sinTheta * length;
		}
	}
	
	// not correct!!!???
	public float getRotationAngleAroundYaxis()
	{
		float angle = 0.0f;
		float length = getLengthXZ();
		if (length != 0){
			float cosTheta = i / length;
			float sinTheta = k / length;		
			angle = (float) (/*(Math.PI * 2)*/ - getArcCosine(cosTheta, sinTheta)); 			
		}
		return (float) (angle / Math.PI * 180);
		
		/*float angle = 0.0f;
		float lengthXY = getLengthXY();
		if (lengthXY != 0){
			float tanTheta = k / lengthXY;			
			angle = (float) Math.atan(tanTheta);
		}
		else if (k > 0)
			angle = (float) (Math.PI / 2);
		else if (k < 1)
			angle = (float) (- Math.PI / 2);
		angle = (float) (Math.PI * 2 - angle);
		angle = (float) (angle / Math.PI * 180);
		Log.d("Vector.graay.angle", angle + "");
		return angle;*/
	}
	
	// not correct!!!???
	public float getRotationAngleAroundZXaxis()
	{
		float angle = 0.0f;
		float lengthXZ = getLengthXZ();
		if (lengthXZ != 0){
			float tanTheta = j / lengthXZ;
			//float sinTheta = j / length;
			//Log.d("Vector.graaz.i", i + "");
			//Log.d("Vector.graaz.j", j + "");
			//Log.d("Vector.graaz.LengthXZ", lengthXZ + "");
			//angle = getArcCosine(cosTheta, sinTheta); 
			//if (j >= 0)
			angle = (float) Math.atan(tanTheta);
			//else
				
		}
		else if (k >= 0)
			angle = (float) (Math.PI / 2);
		else if (k < 0)
			angle = (float) (Math.PI / 2);
		angle = (float) (angle / Math.PI * 180);
		//Log.d("Vector.graaz.angle", angle + "");
		return angle;
	}
	
	private float getArcCosine(float cosValue, float sinValue) 
	{
		double c = Math.acos(cosValue);
		if (sinValue < 0){
			/*if ((c > 0)&&(c < (Math.PI / 2)))
				c = (2 * Math.PI) - c;
			else if ((c > Math.PI / 2) && (c < (Math.PI)))
			{
				double c2 = (2 * Math.PI) - c;
				c = (2 * Math.PI) + c2;
			}*/
			c = (2 * Math.PI) - c;
		}		
		return (float) c;
	}
	/**
	 * 
	 * @param v
	 * @return
	 * returns the smallest angle between two vectors in radians.
	 */
	public float getAngleBetweenVectors(Vector v)
	{
		float dt = dotProduct(v);
		float lengthProduct = getLength() * v.getLength();
		float cosPhi = dt / lengthProduct;
		return (float) Math.acos(cosPhi);
		
	}
	
	/**
	 * 
	 * @param v
	 * @return
	 * returns the smallest angle between two vectors in degrees.
	 */
	public float getAngleBetweenVectorsInDegres(Vector v)
	{
		float dt = dotProduct(v);
		float lengthProduct = getLength() * v.getLength();
		float cosPhi = dt / lengthProduct;
		return (float) (Math.acos(cosPhi)* 180 / Math.PI);
		
	}
	/**
	 * 
	 * @return
	 * 	Normalized vector (vector of length 1 and the same direction as original)
	 */
	public Vector normilize()
	{
		//float[] v2 = new float[3];
		float ii, jj, kk;
		float length = getLength();
		ii = i / length;
		jj = j / length;
		kk = k / length;
		
		return new Vector(ii, jj, kk);
		
	}
	/**
	 * 
	 * @param v
	 * Vector to be added to the current Vector
	 * @return
	 *  sum of two vectors
	 */
	public Vector addVector(Vector v)
	{
		//float[] v2 = new float[3];
		float ii, jj, kk;
		ii = i + v.getiComponent();
		jj = j + v.getjComponent();
		kk = k + v.getkComponent();
		
		return new Vector(ii, jj, kk);
	}
	
	/**
	 * 
	 * @param v1
	 * @param v2
	 * Sums two vectors and makes it this vector 
	 */
	public void sumTwoVectors(Vector v1, Vector v2)
	{
		
		i = v1.getiComponent() + v2.getiComponent();
		j = v1.getjComponent() + v2.getjComponent();
		k = v1.getkComponent() + v2.getkComponent();		
		
	}
	
	/**
	 * add to this vector, changing its values to the sum
	 * @param v
	 */
	public void add(Vector v)
	{
		
		i += v.getiComponent();
		j += v.getjComponent();
		k += v.getkComponent();
		
		
	}
	/**
	 * subtracts from this vector, changing its values to the difference
	 * @param v
	 */
	public void subtract(Vector v)
	{
		
		i -= v.getiComponent();
		j -= v.getjComponent();
		k -= v.getkComponent();
		
		
	}
	
	/**
	 * 
	 * @param v1
	 * @param v2
	 * subtracts v2 from v1 and assignes the result to this vector 
	 */
	public void subtractTwoVectors(Vector v1, Vector v2)
	{
		
		i = v1.getiComponent() - v2.getiComponent();
		j = v1.getjComponent() - v2.getjComponent();
		k = v1.getkComponent() - v2.getkComponent();		
		
	}
	
	/**
	 * 
	 * @param v
	 * Vector to be subtracted from the current Vector
	 * @return
	 *  difference of two vectors
	 */
	public Vector subtractVector(Vector v)
	{
		//float[] v2 = new float[3];
		float ii, jj, kk;
		ii = i - v.getiComponent();
		jj = j - v.getjComponent();
		kk = k - v.getkComponent();
		
		return new Vector(ii, jj, kk);
	}
	/**
	 * multiplies this vector by a scalar and returns new vector as a result;
	 * @param scalar
	 *  
	 * @return Vector after multiplication
	 */
	public Vector multiplyByScalar(float scalar)
	{
		//float[] v2 = new float[3];
		float ii, jj, kk;
		ii = i * scalar;
		jj = j * scalar;
		kk = k * scalar;
		
		return new Vector(ii, jj, kk);
	}
	
	/**
	 * multiplies vector by a scalar and makes this vector a result;
	 * substitute for 
	 * 	Vector new_v = v.multiplyByScalar(scalar);
	 * without the need to allocate new vector object;
	 * @param v - vector for scalar multiplication
	 * @param scalar - scalar
	 *  
	 * @return Vector after multiplication
	 */
	public void multiplyVectorByScalar(Vector v, float scalar)
	{
		//float[] v2 = new float[3];
		
		i = v.getiComponent() * scalar;
		j = v.getjComponent() * scalar;
		k = v.getkComponent() * scalar;	
		
	}
	/**
	 * multiplies this vector by a scalar
	 * @param scalar
	 */
	public void multiplySelfByScalar(float scalar)
	{
		//float[] v2 = new float[3];
		i *= scalar;
		j *= scalar;
		k *= scalar;
		
		//return new Vector(v2);
	}
	
	/**
	 * 
	 * @param scalar
	 * @return
	 */
	public Vector divideByScalar(float scalar)
	{
		//float[] v2 = new float[3];
		float ii, jj, kk;
		ii = i / scalar;
		jj = j / scalar;
		kk = k / scalar;
		
		return new Vector(ii, jj, kk);
	}
	
	
	public String toString()
	{
		return getiComponent() + "i " +
				getjComponent() + "j "+
				getkComponent() +"k ";
	}
	
	public Vector clone()
	{
		return new Vector(i, j, k);
		
	}
	
	public void copy(Vector v)
	{
		i = v.getiComponent();
		j = v.getjComponent();
		k = v.getkComponent();
	}
}

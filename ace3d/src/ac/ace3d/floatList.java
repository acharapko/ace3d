package ac.ace3d;

public class floatList {
		private int size;
		private int len;
		private int index;
		private float[] b;
		
		public floatList()
		{			
			this(128);
		}
		public floatList(int size)
		{
			this.size = size;
			len = 0;
			b = new float[size];
		}
		public float[] readBuffer(){
			float[] b2 = new float[len];
			for (int i = 0; i < len; i++)
				b2[i]= b[i];
			return b2;
		}
		public int getLength(){
			return len;
		}
		
		public int getPosition(){
			return index;
		}
		
		public float get(int index){
			return b[index];
		}
		
		public void setPosition(int position){
			index = position;
		}
		
		public void appendFloat(float appendfloat)
		{
			b[index++] = appendfloat;
			if (++len == b.length)
			{
				float[] b2 = new float[b.length * 2];
				for (int i = 0; i < b.length; i++)
					b2[i] = b[i];
				b = b2;
			}
						
		}
		
		public void resetBuffer()
		{
			b = new float[size];
			len = 0;
		}
		
		public void append(float[] appendFloates)
		{
			for (int i = 0; i < appendFloates.length; i++)
				appendFloat(appendFloates[i]);
		}
}
package ac.ace3d;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.Log;

public class bitmapPlaneReader {

	public static float[] getVertices(Resources res, int fileID, float scale)
	{
		float[] vertices;
		
		return null;		
	}
	
	/**
	 * 
	 * @param b bitmap
	 * @param scale number sectors per one unit of height
	 * @return
	 * 	 a set of heights form the bitmap
	 */
	public static float[] getVertices(Bitmap b, float scale)
	{
		int x = 1; 
		int y = 0;
		int vSize = b.getHeight() * b.getWidth();
		float[] vertices = new float[vSize];
		vSize -= 1;
		int refVal = b.getPixel(0, 0);
		for (int i = 0; i < vSize; i++)
		{
			//Log.d("vSize", vSize + "");
			//Log.d("getVertices.x", x + "");
			//Log.d("getVertices.y", y + "");
			float tmp = b.getPixel(x, y) - refVal; //reduce number of vars
			tmp = tmp / scale;
			vertices[i] = tmp;
			x++;
			if (x >= b.getWidth())
			{
				x = 0;
				y++;
			}
		}
		return vertices;		
	}
	
	public static float[] getVerticesAdj(Bitmap b, float scale)
	{
		int x = 0; 
		int y = 0;
		int biggestVal, smallestVal;
		float biggestAdjVal = 0;
		float smallestAdjVal = 0;
		float rScale = 1 / scale;
		int vSize = b.getHeight() * b.getWidth();		
		float[] vertices = new float[vSize];
		int refVal = b.getPixel(0, 0);
		biggestVal = refVal;
		smallestVal = refVal - 1;
		vSize -= 1;
		for (int i = 0; i < vSize; i++)
		{
			int tmp = b.getPixel(x, y); /*- refVal;*/ //reduce number of vars
			if (tmp > biggestVal)
			{
				biggestVal = tmp;
				biggestAdjVal += rScale;
				vertices[i] = biggestAdjVal;				
			}
			else if (tmp < smallestVal)
			{
				smallestAdjVal = tmp;
				smallestAdjVal -= rScale;
				vertices[i] = smallestAdjVal;		
			}
			else if (tmp == biggestVal)
			{
				vertices[i] = biggestAdjVal;
			}
			else if (tmp == smallestVal)
			{
				vertices[i] = smallestAdjVal;
			}
			
			x++;
			if (x >= b.getWidth())
			{
				x = 0;
				y++;
			}
		}
		return vertices;		
	}
}

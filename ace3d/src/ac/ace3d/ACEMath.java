package ac.ace3d;

public final class ACEMath {
	
	public static int sigfig = 4;
	
	public static float roundf(float r){
		int mult = (int)Math.pow(10, sigfig);
		int t = (int)(r * mult);
		return t /(float)mult; 
		
	}
	
	public static Vector getShortestDistanceToPlane(Vector Normal, Vector vectorToPlane)
	{
		float dp;
		dp = Normal.dotProduct(vectorToPlane);
		return Normal.multiplyByScalar(dp);
	}
	
	public static float getLengthBetweenVectors(Vector v1, Vector v2)
	{
		float ii = v1.getiComponent() - v2.getiComponent();
		float jj = v1.getjComponent() - v2.getjComponent();
		float kk = v1.getkComponent() - v2.getkComponent();		
		
		return Vector.getLength(ii, jj, kk);
	}

}

package ac.ace3d;

import android.util.Log;

public class CopyOfVector implements Cloneable {

	private float[] Vect;
	public CopyOfVector()
	{
		this(0,0,0); //default Vector
	}
	
	public CopyOfVector(float[] v)
	{
		if (v.length == 3)
			Vect = v;
		else
		{
			Vect = new float[3];
			Vect[0] = 0;
			Vect[1] = 0;
			Vect[2] = 1;
		}
				
	}
	public CopyOfVector(float x, float y, float z)
	{
		Vect = new float[3];
		Vect[0] = x;
		Vect[1] = y;
		Vect[2] = z;
	}
	/**
	 * sets vector if it is a Zero-vector
	 * @param v
	 * Vector parameter.
	 */
	public void setVector(float[] v)
	{
		if (getLength() == 0)
			if (v.length == 3)
				Vect = v;
		
	}
	
	/**
	 * 
	 * @return
	 * true if vector has no length, false if vector is not zero
	 */
	
	public boolean isZero()
	{
		boolean r = true;
		if (getLength() > 0)
			r = false;		
		return r;
	}
	
	/**getiComponent()
	 * 
	 * @return
	 * 		returns i component of a vector (along the x-axis)
	 */
	public float getiComponent()
	{
		return Vect[0];
	}
	/**getjComponent()
	 * 
	 * @return
	 * 		returns j component of a vector (along the y-axis)
	 */
	public float getjComponent()
	{
		return Vect[1];
	}
	/**getkComponent()
	 * 
	 * @return
	 * 		returns k component of a vector (along the z-axis)
	 */
	public float getkComponent()
	{
		return Vect[2];
	}
	/** dotProduct(Vector v)
	 * 
	 * @param v
	 * 		Second Vector used to calculate dot product
	 * @return
	 * 		dot product of the vector and the vector v
	 */
	public float dotProduct(CopyOfVector v)
	{
		return v.getiComponent() * Vect[0] + v.getjComponent() * Vect[1] 
		            + v.getkComponent() * Vect[2];
	}
	
	
	/** crossProduct(Vector v)
	 * 
	 * @param v
	 * 		Second Vector used to calculate cross product. 
	 * 		Order of vectors matter for cross product. 
	 * @return
	 * 		cross product of the vector and the vector v
	 */
	
	public CopyOfVector crossProduct(CopyOfVector v)
	{
		float[] newv = new float[3];
		newv[0] = Vect[1] * v.getkComponent() - Vect[2] * v.getjComponent();
		newv[1] = Vect[2] * v.getiComponent() - Vect[0] * v.getkComponent();
		newv[2] = Vect[0] * v.getjComponent() - Vect[1] * v.getiComponent();
		
		return new CopyOfVector(newv);
		
	}
	/** dotProduct(Vector v1, Vector v2)
	 * 
	 * @param v1
	 * 		First vector for calculating dot product
	 * @param v2
	 * 		Second vector for calculating dot product
	 * @return
	 * 		dot product of v1 and v2
	 */
	public static float dotProduct(CopyOfVector v1, CopyOfVector v2)
	{
		return v1.getiComponent() * v2.getiComponent() + v1.getjComponent() * v2.getjComponent() 
		         + v1.getkComponent() * v2.getkComponent();
	}
	/** crossProduct(Vector v1, Vector v2)
	 * 
	 * @param v1
	 * 		First vector for calculating cross product
	 * @param v2
	 *		Second vector for calculating cross product
	 * @return
	 * 		Cross product (Vector multiplication) vector of v1 x v2. 
	 * 		order matters. v1 x v2 != v2 x v1
	 */
	public static CopyOfVector crossProduct(CopyOfVector v1, CopyOfVector v2)
	{
		float[] newv = new float[3];
		newv[0] = v1.getjComponent() * v2.getkComponent() - v1.getkComponent() * v2.getjComponent();
		newv[1] = v1.getkComponent() * v2.getiComponent() - v1.getiComponent() * v2.getkComponent();
		newv[2] = v1.getiComponent() * v2.getjComponent() - v1.getjComponent() * v2.getiComponent();
		
		return new CopyOfVector(newv);
	}
	/**
	 * 
	 * @return
	 * 		Length of the Vector
	 */
	
	public float getLength()
	{
		return (float)(Math.sqrt(Math.pow(Vect[0], 2) + Math.pow(Vect[1], 2) + Math.pow(Vect[2], 2)));
	}
	/**
	 * 
	 * @return
	 * 	length of the the projection on XY plane
	 */
	public float getLengthXY()
	{
		return (float)(Math.sqrt(Math.pow(Vect[0], 2) + Math.pow(Vect[1], 2)));
	}
	/**
	 * 
	 * @return
	 * 	length of the the projection on XZ plane
	 */
	
	public float getLengthXZ()
	{
		return (float)(Math.sqrt(Math.pow(Vect[0], 2) + Math.pow(Vect[2], 2)));
	}
	
	/**
	 * 
	 * @return
	 * 	length of the the projection on YZ plane
	 */	
	public float getLengthYZ()
	{
		return (float)(Math.sqrt(Math.pow(Vect[1], 2) + Math.pow(Vect[2], 2)));
	}
	/**
	 * not correct!!!
	 * @param angle
	 */
	public void rotateX(float angle)
	{
		angle = (float)(angle * Math.PI / 180);
		float length = getLengthYZ();
		if (length != 0){
			float cosTheta = Vect[1] / length;
			float sinTheta = Vect[2] / length;
			//float theta = (float)(Math.acos(cosTheta)); //in radians
			float theta = getArcCosine(cosTheta, sinTheta); 
			theta = theta + angle;
			cosTheta = (float) (Math.cos(theta));
			sinTheta = (float) (Math.sin(theta));
			Vect[1] = cosTheta * length;
			Vect[2] = sinTheta * length;
		}
	}
	/**
	 * not correct!!!
	 * @param angle in degrees
	 */
	public void rotateY(float angle)
	{
		angle = (float)(angle * Math.PI / 180);
		float length = getLengthXZ();
		if (length != 0){
			float cosTheta = Vect[0] / length;
			float sinTheta = Vect[2] / length;			
			//float theta = (float)(Math.acos(cosTheta)); //in radians
			float theta = getArcCosine(cosTheta, sinTheta);
			Log.d("length:", length + "");
			Log.d("thetaOld:", theta + "");
			theta = theta + angle;		
			cosTheta = (float) (Math.cos(theta));
			sinTheta = (float) (Math.sin(theta));			
			Vect[0] = cosTheta * length;
			Vect[2] = sinTheta * length;
		}
	}
	/**
	 * not correct!!!
	 * @param angle
	 * 		angle, in degrees to rotate around z-axis
	 */
	public void rotateZ(float angle)
	{
		angle = (float)(angle * Math.PI / 180);
		float length = getLengthXY();
		if (length != 0){
			float cosTheta = Vect[0] / length;
			float sinTheta = Vect[1] / length;
			//float theta = (float)(Math.acos(cosTheta)); //in radians
			float theta = getArcCosine(cosTheta, sinTheta); 
			theta = theta + angle;
			cosTheta = (float) (Math.cos(theta));
			sinTheta = (float) (Math.sin(theta));
			Vect[0] = cosTheta * length;
			Vect[1] = sinTheta * length;
		}
	}
	
	// not correct!!!???
	public float getRotationAngleAroundYaxis()
	{
		float angle = 0.0f;
		float length = getLengthXZ();
		if (length != 0){
			float cosTheta = Vect[0] / length;
			float sinTheta = Vect[2] / length;		
			angle = (float) (/*(Math.PI * 2)*/ - getArcCosine(cosTheta, sinTheta)); 			
		}
		return (float) (angle / Math.PI * 180);
		
		/*float angle = 0.0f;
		float lengthXY = getLengthXY();
		if (lengthXY != 0){
			float tanTheta = Vect[2] / lengthXY;			
			angle = (float) Math.atan(tanTheta);
		}
		else if (Vect[2] > 0)
			angle = (float) (Math.PI / 2);
		else if (Vect[2] < 1)
			angle = (float) (- Math.PI / 2);
		angle = (float) (Math.PI * 2 - angle);
		angle = (float) (angle / Math.PI * 180);
		Log.d("Vector.graay.angle", angle + "");
		return angle;*/
	}
	
	// not correct!!!???
	public float getRotationAngleAroundZXaxis()
	{
		float angle = 0.0f;
		float lengthXZ = getLengthXZ();
		if (lengthXZ != 0){
			float tanTheta = Vect[1] / lengthXZ;
			//float sinTheta = Vect[1] / length;
			//Log.d("Vector.graaz.Vect[0]", Vect[0] + "");
			//Log.d("Vector.graaz.Vect[1]", Vect[1] + "");
			//Log.d("Vector.graaz.LengthXZ", lengthXZ + "");
			//angle = getArcCosine(cosTheta, sinTheta); 
			//if (Vect[1] >= 0)
			angle = (float) Math.atan(tanTheta);
			//else
				
		}
		else if (Vect[2] >= 0)
			angle = (float) (Math.PI / 2);
		else if (Vect[2] < 0)
			angle = (float) (Math.PI / 2);
		angle = (float) (angle / Math.PI * 180);
		//Log.d("Vector.graaz.angle", angle + "");
		return angle;
	}
	
	private float getArcCosine(float cosValue, float sinValue) 
	{
		double c = Math.acos(cosValue);
		if (sinValue < 0){
			/*if ((c > 0)&&(c < (Math.PI / 2)))
				c = (2 * Math.PI) - c;
			else if ((c > Math.PI / 2) && (c < (Math.PI)))
			{
				double c2 = (2 * Math.PI) - c;
				c = (2 * Math.PI) + c2;
			}*/
			c = (2 * Math.PI) - c;
		}		
		return (float) c;
	}
	/**
	 * 
	 * @param v
	 * @return
	 * returns the smallest angle between two vectors in radians.
	 */
	public float getAngleBetweenVectors(CopyOfVector v)
	{
		float dt = dotProduct(v);
		float lengthProduct = getLength() * v.getLength();
		float cosPhi = dt / lengthProduct;
		return (float) Math.acos(cosPhi);
		
	}
	
	/**
	 * 
	 * @param v
	 * @return
	 * returns the smallest angle between two vectors in degrees.
	 */
	public float getAngleBetweenVectorsInDegres(CopyOfVector v)
	{
		float dt = dotProduct(v);
		float lengthProduct = getLength() * v.getLength();
		float cosPhi = dt / lengthProduct;
		return (float) (Math.acos(cosPhi)* 180 / Math.PI);
		
	}
	/**
	 * 
	 * @return
	 * 	Normalized vector (vector of length 1 and the same direction as original)
	 */
	public CopyOfVector normilize()
	{
		float[] v2 = new float[3];
		float length = getLength();
		v2[0] = Vect[0] / length;
		v2[1] = Vect[1] / length;
		v2[2] = Vect[2] / length;
		
		return new CopyOfVector(v2);
		
	}
	/**
	 * 
	 * @param v
	 * Vector to be added to the current Vector
	 * @return
	 *  sum of two vectors
	 */
	public CopyOfVector addVector(CopyOfVector v)
	{
		float[] v2 = new float[3];
		v2[0] = Vect[0] + v.getiComponent();
		v2[1] = Vect[1] + v.getjComponent();
		v2[2] = Vect[2] + v.getkComponent();
		
		return new CopyOfVector(v2);
	}
	
	/**
	 * add to this vector, changing its values to the sum
	 * @param v
	 */
	public void add(CopyOfVector v)
	{
		
		Vect[0] += v.getiComponent();
		Vect[1] += v.getjComponent();
		Vect[2] += v.getkComponent();
		
		
	}
	/**
	 * subtracts from this vector, changing its values to the difference
	 * @param v
	 */
	public void subtract(CopyOfVector v)
	{
		
		Vect[0] -= v.getiComponent();
		Vect[1] -= v.getjComponent();
		Vect[2] -= v.getkComponent();
		
		
	}
	/**
	 * 
	 * @param v
	 * Vector to be subtracted from the current Vector
	 * @return
	 *  difference of two vectors
	 */
	public CopyOfVector subtractVector(CopyOfVector v)
	{
		float[] v2 = new float[3];
		v2[0] = Vect[0] - v.getiComponent();
		v2[1] = Vect[1] - v.getjComponent();
		v2[2] = Vect[2] - v.getkComponent();
		
		return new CopyOfVector(v2);
	}
	/**
	 * multiplies this vector by a scalar and returns new vector as a result;
	 * @param scalar
	 *  
	 * @return Vector after multiplication
	 */
	public CopyOfVector multiplyByScalar(float scalar)
	{
		float[] v2 = new float[3];
		v2[0] = Vect[0] * scalar;
		v2[1] = Vect[1] * scalar;
		v2[2] = Vect[2] * scalar;
		
		return new CopyOfVector(v2);
	}
	/**
	 * multiplies this vector by a scalar
	 * @param scalar
	 */
	public void multiplySelfByScalar(float scalar)
	{
		//float[] v2 = new float[3];
		Vect[0] *= scalar;
		Vect[1] *= scalar;
		Vect[2] *= scalar;
		
		//return new Vector(v2);
	}
	
	/**
	 * 
	 * @param scalar
	 * @return
	 */
	public CopyOfVector divideByScalar(float scalar)
	{
		float[] v2 = new float[3];
		v2[0] = Vect[0] / scalar;
		v2[1] = Vect[1] / scalar;
		v2[2] = Vect[2] / scalar;
		
		return new CopyOfVector(v2);
	}
	
	
	public String toString()
	{
		return getiComponent() + "i " +
				getjComponent() + "j "+
				getkComponent() +"k ";
	}
	
	public CopyOfVector clone()
	{
		return new CopyOfVector(Vect[0], Vect[1], Vect[2]);
		
	}
}

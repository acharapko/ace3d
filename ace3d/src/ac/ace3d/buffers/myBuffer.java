package ac.ace3d.buffers;

import java.lang.reflect.Array;

public class myBuffer<E> {
	
	int size;
	int index, length;
	E[] b;
	
	public myBuffer()
	{			
		this(128);
	}
	public myBuffer(int size)
	{
		this.size = size;
		index = 0;
		b = (E[]) new Array[size];
	}
	public E[] readBuffer(){
		return b;
	}
	public int getLength(){
		return b.length;
	}
	
	public int getPosition(){
		return index;
	}
	
	public void appendElement(E appendElment)
	{
		b[index] = appendElment;
		if (++index == b.length)
		{
			E[] b2 = (E[]) new Array[b.length * 2];
			for (int i = 0; i < b.length; i++)
				b2[i] = b2[i];
			b = b2;
		}
	}
	
	public void resetBuffer()
	{
		b = (E[]) new Array[size];
		index = 0;
	}
	
	public void append(E[] appendElements)
	{
		for (int i = 0; i < appendElements.length; i++)
			appendElement(appendElements[i]);
	}
}


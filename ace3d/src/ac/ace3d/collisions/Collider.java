package ac.ace3d.collisions;

import ac.ace3d.Vector;
import ac.ace3d.world.Ace3dObject;
import ac.ace3d.Orientation;
import android.util.Log;

public abstract class Collider implements Collidable {

	Orientation orientation;
	protected Vector relativePosition, deltaRelPosition, nextRelPos;
	private Ace3dObject aceObj;
	private Vector tempPosChangeVector = new Vector();
	private Vector tmpWorldPos = new Vector();
	/**
	 * 
	 * @param aceObject
	 * @param position position relative to the object containing the collider
	 */
	public Collider(Ace3dObject aceObject, Vector position)
	{
		aceObj = aceObject; 
		relativePosition = position;	
		deltaRelPosition = new Vector();
		nextRelPos = new Vector();
		nextRelPos.copy(relativePosition);
	}
	
	/**
	 * 
	 * @return
	 * 		returns the change in location during the current frame.
	 * 
	 */
	public Vector getPositionChange()
	{
		if (aceObj != null)
		{
			tempPosChangeVector.copy(aceObj.getPositionChange());
			tempPosChangeVector.add(deltaRelPosition);
			return tempPosChangeVector;
		}
		else 
			return new Vector();
			//return  Vector.getBlancVector();
	} 
	
	public void setAce3dObject(Ace3dObject aceObj)
	{
		this.aceObj = aceObj;
	}
	
	/*public Vector getDirection()
	{
		return aceObj.getDirection();
	}*/

	public void changeRealtivePosition(Vector dD) {
		relativePosition.add(dD);
		
	}
	
	public void getRelativePosition(Vector v) {
		
		v.copy(relativePosition);
		//return relativePosition;
	}
	
	public void getDeltaRelPosition(Vector v) {
		
		v.copy(deltaRelPosition);
		//return deltaRelPosition.clone();
		//return deltaRelPosition;
	}
	/**
	 * sets the relative position change of the collider due to objects rotation
	 * change isn't applied untill applyRotation() method is called.
	 * @param y rotation around y axis
	 * @param horizont rotation from horizon
	 */
	public void setRotation(float y, float horizon)
	{
		nextRelPos.copy(relativePosition);
		//nextRelPos = relativePosition.clone();
		nextRelPos.rotateY(y);
		//nextRelPos.rotateX(horizon);
		//deltaRelPosition = nextRelPos.subtractVector(relativePosition);
		deltaRelPosition.subtractTwoVectors(nextRelPos, relativePosition);
		/*Log.d("setRotation", "----------------------");
		Log.d("Y (rad)", (float)(y * Math.PI / 180) + "");
		Log.d("relPos", relativePosition.toString());
		Log.d("nextRelPos", nextRelPos.toString());
		Log.d("deltaRelPos", deltaRelPosition.toString());*/
		//c.changeWorldPosition(c.getWorldPosition());
		//c.setRealtivePosition(cPos);
	}
	/**
	 * resets change in relative position of the collider due to objects rotation if 
	 * such change has been set, but not applied by method applyRotation();
	 */
	public void resetRotation()
	{
		//Log.d("COLLIDER", "Resetting Change in Reative position " + relativePosition + " <- " + nextRelPos);
		nextRelPos = relativePosition.clone();
		//deltaRelPosition.resetVector();
		relativePosition.resetVector();
	}

	public void applyRotation()
	{
		//Log.d("COLLIDER", "Changing relative position " + relativePosition + " <- " + nextRelPos);
		relativePosition.copy(nextRelPos);
		deltaRelPosition.resetVector();
	}
	
	/*public final Vector getWorldPosition() {
		tmpWorldPos.sumTwoVectors(relativePosition, aceObj.getWorldPosition());
		return tmpWorldPos;
		//return relativePosition.addVector(aceObj.getWorldPosition());
	}*/
	
	public final void getWorldPosition(Vector worldPos) {
		aceObj.getWorldPosition(tmpWorldPos);
		worldPos.sumTwoVectors(relativePosition, tmpWorldPos);
		
		//return relativePosition.addVector(aceObj.getWorldPosition());
	}
	
	public final void getHostObjectsWorldPosition(Vector worldPos) {
		
		 aceObj.getWorldPosition(worldPos);
	}


	public void setRealtivePosition(Vector pos) {
		relativePosition = pos;
		
	}

	public void setAceObj(Ace3dObject aceObj) {
		this.aceObj = aceObj;
	}

	public Ace3dObject getAceObj() {
		return aceObj;
	}


}

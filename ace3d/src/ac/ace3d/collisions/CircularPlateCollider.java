package ac.ace3d.collisions;

import ac.ace3d.Vector;
import ac.ace3d.world.Ace3dObject;
import ac.ace3d.Orientation;

public class CircularPlateCollider extends PlaneCollider {
	private float Radius;
	
	/*public CircularPlateCollider(Ace3dObject aceObject, Vector position,
			Vector Normal, float radius) {
		super(aceObject, position, Normal);
		Radius = radius;
		
	}*/
	
	public CircularPlateCollider(Ace3dObject aceObject, Vector position,
			Orientation or, float radius)
	{
		super(aceObject, position, or);
		Radius = radius;
	}

	/*public void detectCollision(Collidable object, CollisionInfo ci) {
		//CollisionInfo cp = new CollisionInfo();
		
		if (object instanceof PointCollider)
		{
			CollisionDetecter.circularWithPoint(this, (PointCollider)object, ci);
		}
		
		//return cp;
	}*/
	
	public float getRadius()
	{
		return Radius;
	}



}

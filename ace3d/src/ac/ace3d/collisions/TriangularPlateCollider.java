package ac.ace3d.collisions;

import ac.ace3d.Vector;
import ac.ace3d.world.Ace3dObject;
import ac.ace3d.Orientation;
import android.util.Log;

public class TriangularPlateCollider extends PlaneCollider {

	private Vector adjSide1, adjSide2;
	private Vector oppSide;
	//private float maxDimension = 0.0f;
	
	 //store this values to reduce number of computations
	private float AdjSide1Length, AdjSide2Length, oppSideLength;
	private float angleBetweenAdjSides, angleBetweenAdjSide1OppSide;
	
	public TriangularPlateCollider(Ace3dObject aceObject, Vector position,
			Orientation or, Vector AdjSide1, Vector AdjSide2) {
		super(aceObject, position, or);
		this.adjSide1 = AdjSide1;
		this.adjSide2 = AdjSide2;
		
		oppSide = adjSide2.subtractVector(adjSide1);
		oppSideLength = oppSide.getLength();
		
		AdjSide1Length = AdjSide1.getLength();
		AdjSide2Length = AdjSide2.getLength();		
		angleBetweenAdjSides = AdjSide1.getAngleBetweenVectors(AdjSide2);
		angleBetweenAdjSide1OppSide = AdjSide1.getAngleBetweenVectors(oppSide);
	}
	
	public TriangularPlateCollider(Ace3dObject aceObject, Vector position,
			Vector normal, Vector AdjSide1, Vector AdjSide2) {
		super(aceObject, position, normal);
		this.adjSide1 = AdjSide1;
		this.adjSide2 = AdjSide2;
		
		oppSide = adjSide2.subtractVector(adjSide1);
		oppSideLength = oppSide.getLength();
		
		AdjSide1Length = AdjSide1.getLength();
		AdjSide2Length = AdjSide2.getLength();		
		angleBetweenAdjSides = AdjSide1.getAngleBetweenVectors(AdjSide2);
		angleBetweenAdjSide1OppSide = AdjSide1.getAngleBetweenVectors(oppSide);
	}
	
	public TriangularPlateCollider(Ace3dObject aceObject, Vector position,
			Orientation or, float AdjSide1, float AdjSide2) {
		this(aceObject, position, or, new Vector(AdjSide1, 0, 0), new Vector(0, AdjSide2, 0));
		rotate(or);
	}
	
	public TriangularPlateCollider(Ace3dObject aceObject, Vector position,
			Vector normal, float AdjSide1, float AdjSide2) {
		this(aceObject, position, normal, new Vector(AdjSide1, 0, 0), new Vector(0, AdjSide2, 0));
		rotate(orientation);
	}
	
	/*public void setOrientation(Orientation or)
	{
		super.setOrientation(or);
		rotate(or);
	}*/
	
	public void rotate(Orientation or)
	{		
		float raaY = or.getRotationY(); 
		float raaZ = or.getRotationFromHorizont();
		//Log.d("rpc.rotate.raaY", raaY + "");
		//Log.d("rpc.rotate.raaZ", raaZ + "");			
		
		//x axis of the object rotated after the call to rotateY. 
		//rotateY sets the rotation around Y axis, which causes X and Z axis to rotate
		//so we need to compensate for this rotation
		
		//m.rotateX(rotation);
		adjSide1.rotateY(raaY);
		adjSide2.rotateY(raaY);
		if ((raaY > 90) && (raaY < 270))
			raaZ = 180 - raaZ;
		adjSide1.rotateZ(raaZ);
		adjSide2.rotateZ(raaZ);
		adjSide1.rotateX(or.getRotationArounAxis());
		adjSide2.rotateX(or.getRotationArounAxis());
		
	}
	
	public float getAngleBetweenAdjSides()
	{
		return angleBetweenAdjSides;
	}
	
	public float angleBetweenAdjSide1OppSide()
	{
		return angleBetweenAdjSide1OppSide;
	}
	
	public float getAdjSide1Length()
	{
		return AdjSide1Length;
	}
	
	public float getAdjSide2Length()
	{
		return AdjSide2Length;
	}
	
	public Vector getAdjSide1()
	{
		return adjSide1;		
	}
	
	public Vector getAdjSide2()
	{
		return adjSide2;
	}
	
	public Vector getOppSide()
	{
		return oppSide;
	}

	/*public void setMaxDimension(float maxDimension) {
		this.maxDimension = maxDimension;
	}

	public float getMaxDimension() {
		return maxDimension;
	}*/
	

	
	/*public void detectCollision(Collidable object, CollisionInfo ci) {
		CollisionInfo cp  = new CollisionInfo();
		if (object instanceof PointCollider)
		{
			CollisionDetecter.triangularWithPoint(this, (PointCollider)object, ci);
		}
		//return cp;
	}*/

	

}

package ac.ace3d.collisions;

import java.util.ArrayList;

import ac.ace3d.Vector;
import ac.ace3d.collisions.CollisionInfo.CollisionType;
import ac.ace3d.world.*;

public class GravityDetecter extends CollisionDetecter{
	
	private static Vector tempObjWorlPos = new Vector();
	private static Vector tempSurfaceWorlPos = new Vector();
	
	public static CollisionInfo checkGravity(Ace3dObject surface, MovingAce3dObject object)
	{
		//default CollisionInfo with no collision flag
		CollisionInfo ci = new CollisionInfo();
				
		object.getWorldPosition(tempObjWorlPos);
		surface.getWorldPosition(tempSurfaceWorlPos);
		float distance = (tempSurfaceWorlPos.subtractVector(tempObjWorlPos)).getLength();
				
		if (object instanceof MovingAce3dObject)
			if (((MovingAce3dObject)object).getNoCollisionRadius()  > distance){
						
				//surface.moveColliders();					
				//((MovingAce3dObject)object).moveColliders();
				
				//check for collision from all colliders of the Ace3dObject
				ArrayList<GravityCollider> oCldrs = object.getGravityColliders();
				ArrayList<Collider> colliders = surface.getColliders();
				
				for (int i = 0; i < colliders.size(); i++)
					for (int j = 0; j < oCldrs.size(); j++)
					{
						Collider cl = oCldrs.get(j); 
						detectCollision(cl,colliders.get(i), ci);
						//exit loops if collision happens
						if (ci.getType() != CollisionType.NO_COLLISION){
							i = colliders.size();
							j = oCldrs.size();
						}
					}
			}
		return ci;
	}

}

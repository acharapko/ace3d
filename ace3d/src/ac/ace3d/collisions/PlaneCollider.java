package ac.ace3d.collisions;

import ac.ace3d.Vector;
import ac.ace3d.meshes.Mesh;
import ac.ace3d.world.Ace3dObject;
import ac.ace3d.Orientation;
import android.util.Log;

public abstract class PlaneCollider extends Collider{

    private Vector Normal = new Vector();
	Orientation orientation;
	
	public PlaneCollider(Ace3dObject aceObject, Vector relativePosition, Orientation or)
	{
		super(aceObject, relativePosition);	
		orientation = or;
	}
	
	public PlaneCollider(Ace3dObject aceObject, Vector relativePosition, Vector Normal)
	{
		super(aceObject, relativePosition);	
		
		float raaY = Normal.getRotationAngleAroundYaxis();
		float raaZ = Normal.getRotationAngleAroundZXaxis(); 
		if ((raaY > 90) && (raaY < 270))
			raaZ = 180 - raaZ;
		
		orientation = new Orientation(raaY, raaZ, 0);
	}
	
	/**
	 * sets the current realtive position of the Collider
	 * @param pos
	 */
	public void setRelativePosition(Vector pos)
	{
		relativePosition = pos;
	}
	
	public void changeRelativePosition(Vector dD)
	{
		relativePosition.add(dD);
	}
	
	/*public Vector getWorldPosition()
	{
		return worldPosition;
	}*/
	
	
	public Orientation getOrientation()
	{
		return orientation;
	}
	
	
	
	/*public void setOrientation(Orientation or)
	{
		orientation = or;
	}*/
	
	public void changeOrientation(float y, float horizont, float axis)
	{
		orientation.rotateAroundY(y);
		orientation.rotateAroundHorizontal(horizont);
		orientation.rotateAroundAxis(axis);
	}
	
	public void changeOrientation(Orientation or)
	{
		orientation.rotateAroundY(or.getRotationY());
		orientation.rotateAroundHorizontal(or.getRotationFromHorizont());
		orientation.rotateAroundAxis(or.getRotationArounAxis());
	}
	
	public Vector getNormal()
	{
		//float Vect[] = new float[3];
		float ii, jj, kk;
		//angle = (float)(angle * Math.PI / 180);
		double angleY = orientation.getRotationY() * Math.PI / 180;
		/*Vect[2] = (float) Math.cos(angleY);
		Vect[0] = (float) Math.sin(angleY);		
		Vect[1] = (float) Math.tan(orientation.getRotationFromHorizont() * Math.PI / 180);*/
		ii = (float) Math.cos(angleY);
		kk = -(float) Math.sin(angleY);		
		jj = (float) Math.tan(orientation.getRotationFromHorizont() * Math.PI / 180);
		
		float length = Vector.getLength(ii, jj, kk);
		ii = ii / length;
		jj = jj / length;
		kk = kk / length;
		
		Normal.resetVector(ii, jj, kk);
		
		return Normal;
		
	}
	

}


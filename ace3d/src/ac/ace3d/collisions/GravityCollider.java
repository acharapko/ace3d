package ac.ace3d.collisions;

import ac.ace3d.Vector;
import ac.ace3d.world.Ace3dObject;
import ac.ace3d.world.MovingAce3dObject;

public class GravityCollider extends PointCollider{

	private Vector gravityDir;
	private float height; // distance from surface
	private float passingHeight; //height(elevation) that can be overcomed
	private float lastElevation;
	
	public GravityCollider(Ace3dObject aceObj, Vector Position, 
			Vector gravityDirection) {
		this(aceObj, Position, gravityDirection, 1, 0.4f);
	} 
	
	protected GravityCollider(Ace3dObject aceObj, Vector Position, 
			Vector gravityDirection, float height, float passingHeight) {
		super(aceObj, Position);
		gravityDir = gravityDirection.normilize();
		this.height = height;
		this.passingHeight = passingHeight;
		lastElevation = 0;
	}

	
	public CollisionInfo detectCollision(Collidable object) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Vector getPositionChange()
	{
		Vector retV = ((MovingAce3dObject)getAceObj()).getColliderVelocityDueToGravity();
		//return gravityDir.multiplyByScalar(height);
		if (height > retV.getLength())
			retV = gravityDir.multiplyByScalar(height);
		return retV;
	}

	public void setPassingHeight(float passingHeight) {
		this.passingHeight = passingHeight;
	}

	public float getPassingHeight() {
		return passingHeight;
	}
	
	public void setHeight(float height) {
		this.height = height;
	}

	public float getHeight() {
		return height;
	}

	public void setLastElevation(float lastElevation) {
		this.lastElevation = lastElevation;
	}

	public float getLastElevation() {
		return lastElevation;
	}
	

}

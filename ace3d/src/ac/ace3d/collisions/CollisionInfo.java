package ac.ace3d.collisions;

import ac.ace3d.Vector;

public class CollisionInfo {
	
	public enum CollisionType {NO_COLLISION, POINT_TO_PLANE, GRAVITY_TO_PLANE};
	private Vector collisionPoint;
	private float cDist;
	private CollisionType type;
	private ObjInfo o1, o2;
	private Vector collisionVector;
	private Vector colliderRotationPosChange;
	private Vector planeNormal;
	private boolean isFinal = false;
	
	private float gcHeight, gcPassingHeight, lastElevation;
	
	public CollisionInfo()
	{
		this(new Vector(), Float.NaN, CollisionType.NO_COLLISION);		
	}
	
	public CollisionInfo(Vector point, float distanceToCollision)
	{
		this(point, distanceToCollision, CollisionType.POINT_TO_PLANE);		
	}
	
	public CollisionInfo(Vector point, float distanceToCollision, CollisionType collisionType)
	{
		setcDist(distanceToCollision);
		setCollisionPoint(point);		
		type = collisionType;
	}
	
	public void reset()
	{
		collisionPoint.resetVector();
		type = CollisionType.NO_COLLISION;
		cDist = Float.NaN;
		isFinal = false;
	}
	/**
	 * Finalizes the collisionInfo object preventing modification of its fields.
	 */
	public void finalize()
	{
		isFinal = true;
	}
	
	public void setcDist(float cDist) {
		if (!isFinal)
			this.cDist = cDist;		
	}
	
	public void setPlaneNormal(Vector PlaneNormal)
	{
		if (!isFinal)
			planeNormal = PlaneNormal;		
	}
	public void setCollisionVector(Vector colVect)
	{
		if (!isFinal)
			collisionVector = colVect;
	}
	
	
	public float getcDist() {
		return cDist;
	}
	
	public int getObject1ID()
	{
		return o1.getID();
	}
	
	public int getObject2ID()
	{
		return o2.getID();
	}
	
	/**
	 * 
	 * @return vector along which collision has happened
	 */
	public Vector getCollisionVector()
	{
		return collisionVector;
	}
	
	/**
	 * 
	 * @return vector with shortest dastanc to the plane of collision
	 */
	public Vector getPlaneNormal()
	{
		return planeNormal;
	}
	
	public void setCollisionPoint(Vector collisionPoint) {
		if (!isFinal)
			this.collisionPoint = collisionPoint;
	}
	public Vector getCollisionPoint() {
		//return collisionPoint.clone();
		return collisionPoint;
	}
	
	public void setType(CollisionType type) {
		this.type = type;
	}
	public CollisionType getType() {
		return type;
	}
	public void setGcHeight(float gcHeight) {
		if (!isFinal)
			this.gcHeight = gcHeight;
	}
	public float getGcHeight() {
		return gcHeight;
	}
	public void setGcPassingHeight(float gcPassingHeight) {
		if (!isFinal)
			this.gcPassingHeight = gcPassingHeight;
	}
	public float getGcPassingHeight() {
		return gcPassingHeight;
	}
	public void setLastElevation(float lastElevation) {
		if (!isFinal)
			this.lastElevation = lastElevation;
	}
	public float getLastElevation() {
		return lastElevation;
	}
	
	public void setColliderRotationPosChange(Vector cilliderRotationPosChange) {
		if (!isFinal)
			this.colliderRotationPosChange = cilliderRotationPosChange;
	}

	public Vector getColliderRotationPosChange() {
		return colliderRotationPosChange;
	}

	private class ObjInfo
	{
		private Vector dD;
		private int ID;
		private String tag;
		
		public ObjInfo()
		{
			this(new Vector(), 0,"");
		}
		
		public ObjInfo(Vector displacementChange, int ID, String tag)
		{
			dD = displacementChange;
			this.ID = ID;
			this.tag = tag;
		}
		
		public String getTag()
		{
			return tag;
		}
		
		public int getID()
		{
			return ID;
		}
		
		public Vector getDispalcementChange()
		{
			return dD;
		}
	}
	
}

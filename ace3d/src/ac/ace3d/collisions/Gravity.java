package ac.ace3d.collisions;

import java.util.ArrayList;

public interface Gravity {

	public CollisionInfo checkGravity(Collidable object);
	
	public ArrayList<GravityCollider> getGravityColliders();
}

package ac.ace3d.collisions;

import ac.ace3d.Vector;
import ac.ace3d.world.Locatable;

public interface Collidable extends Locatable {	
	
	public Vector getPositionChange();
	
	//public void detectCollision(Collidable c, CollisionInfo ci);

	
}

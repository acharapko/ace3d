package ac.ace3d.collisions;	

import ac.ace3d.Vector;
import ac.ace3d.world.Ace3dObject;
import ac.ace3d.Orientation;

public abstract class PlateCollider{
	
	protected Vector worldPosition;
	private Ace3dObject aceObj;
	
	protected PlateCollider(Ace3dObject aceObj, Vector Position)
	{
		this.aceObj = aceObj;
		worldPosition = Position;
	}
	/**
	 * sets the current position of the Collider
	 * @param pos
	 */
	public void setWorldPosition(Vector pos)
	{
		worldPosition = pos;
	}
	
	public void changeWorldPosition(Vector dD)
	{
		worldPosition = worldPosition.addVector(dD);
	}
	
	public Vector getWorldPosition()
	{
		return worldPosition;
	}
	public void setAce3dObject(Ace3dObject aceObj)
	{
		this.aceObj = aceObj;
	}
	
	/*public Vector getDirection()
	{
		return aceObj.getDirection();
	}*/
	
	public Orientation getOrientation()
	{
		return aceObj.getOrientation();
	}
	
	/**
	 * 
	 * @return
	 * 		returns the change in location during the current frame.
	 * change is obtained form Ace3dObject associated with this Collider.
	 */
	public Vector getPositionChange()
	{
		if (aceObj != null)
			return aceObj.getPositionChange();
		else 
			return new Vector();
	} 
	
	/*public Vector getSpeed()
	{
		return aceObj.getPositionChange();
	} */
	/**
	 * @deprecated
	 */
	public abstract Vector detectCollision(Collidable object, CollisionInfo ci);
	
}

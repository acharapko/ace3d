package ac.ace3d.collisions;

import ac.ace3d.Vector;
import ac.ace3d.world.Ace3dObject;
import ac.ace3d.Orientation;
import android.util.Log;

public class RectangularPlateCollider extends PlaneCollider {

	private float halfWidthScalar, halfHeightScalar;	
	private Vector halfWidth, halfHeight; 
	
	/**
	 * 
	 * @param aceObject - Ace3dObject which holds this collider
	 * @param relativePosition - Ralative position of the collider
	 * @param or - orientation of the collider
	 * @param w - width
	 * @param h - height
	 */
	public RectangularPlateCollider(Ace3dObject aceObject, Vector relativePosition, Orientation or, float w, float h)
	{
		super(aceObject, relativePosition, or);
		halfWidth = new Vector(w/2, 0f, 0f);
		halfHeight = new Vector(0f, h/2, 0f);
		//halfHeight = halfHeight.addVector(position);
		//halfWidth = halfWidth.addVector(position);
		halfWidthScalar = w/2;
		halfHeightScalar = h/2;
		//add rotation of halfWidth and halfHeight
		rotate(orientation);
		
	}	
	
	public RectangularPlateCollider(Ace3dObject aceObject, Vector position, Vector norm, float w, float h)
	{
		super(aceObject, position, norm);
		halfWidth = new Vector(w/2, 0f, 0f);
		halfHeight = new Vector(0f, h/2, 0f);
		//halfHeight = halfHeight.addVector(position);
		//halfWidth = halfWidth.addVector(position);
		halfWidthScalar = w/2;
		halfHeightScalar = h/2;
		//add rotation of halfWidth and halfHeight
		rotate(orientation);
		
	}	
	
	/*public void setOrientation(Orientation or)
	{
		super.setOrientation(or);
		rotate(or);
	}*/
	
	public float getRotationY()
	{
		return orientation.getRotationY();
	}
	
	public float getRotationFromHorizont()
	{
		return orientation.getRotationFromHorizont();
	}
	
	public void rotate(Orientation or)
	{
		//m.rotateX(rotation); // this call causes Y and Z axis of the object to rotate.
		//float raaY = Normal.getRotationAngleAroundYaxis() - 90; //Normal is perpendicular to the plane
		//float raaZ = Normal.getRotationAngleAroundZXaxis();		
		
		float raaY = or.getRotationY(); 
		float raaZ = or.getRotationFromHorizont();
		
		//Log.d("rpc.rotate.raaY", raaY + "");
		//Log.d("rpc.rotate.raaZ", raaZ + "");
		//Normal.rotateY(raaY);
		//Normal.rotateZ(raaZ);
			
		
		//x axis of the object rotated after the call to rotateY. 
		//rotateY sets the rotation around Y axis, which causes X and Z axis to rotate
		//so we need to compensate for this rotation
		
		//m.rotateX(rotation);
		halfWidth.rotateY(raaY);
		halfHeight.rotateY(raaY);
		if ((raaY > 90) && (raaY < 270))
			raaZ = 180 - raaZ;
		halfWidth.rotateZ(raaZ);
		halfHeight.rotateZ(raaZ);
		halfWidth.rotateX(or.getRotationArounAxis());
		halfHeight.rotateX(or.getRotationArounAxis());
		
	}
	
	public Vector getHalfHeight()
	{
		return halfHeight;
	}
	
	public Vector getHalfWidth()
	{
		return halfWidth;
	}
	
	public float getHalfHeightScalar()
	{
		return halfHeightScalar;
	}
	
	public float getHalfWidthScalar()
	{
		return halfWidthScalar;
	}
	
	
	/*public void detectCollision(Collidable object, CollisionInfo ci) {
		// point to plane collision
		//CollisionInfo cp = new CollisionInfo();
		boolean isColliding = false;
		if (object instanceof PointCollider)
		{
			 CollisionDetecter.rectangularWithPoint(this, (PointCollider)object, ci);			
		}
		//plane to plane collision
		else if (object instanceof PlaneCollider)
		{
			
		}
		//return cp;	
	}*/
}

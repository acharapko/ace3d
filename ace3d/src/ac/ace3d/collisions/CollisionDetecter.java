package ac.ace3d.collisions;

import java.util.ArrayList;

import ac.ace3d.ACEMath;
import ac.ace3d.Vector;
import ac.ace3d.collisions.CollisionInfo.CollisionType;
import ac.ace3d.world.Ace3dObject;
import android.util.Log;
/**
 * this class incorporates collision detection algorithms for all Ace3D colliders.
 * @author AC
 *
 */
public class CollisionDetecter {
	
	public static final float piOver2 = (float) (Math.PI / 2);
	public static int testCCounter = 0;
	private static Vector dD = new Vector();
	private static Vector pointDeltaRelPos = new Vector();
	private static Vector deltaRelPos = new Vector();
	
	private static Vector tempObjWorldPosition1 = new Vector();
	private static Vector tempObjWorldPosition2 = new Vector();
	
	private static Vector tempPCWorldPosition1 = new Vector();
	private static Vector tempRPCWorldPosition1 = new Vector();
	private static Vector tempTPCWorldPosition1 = new Vector();
		
	/**
	 * Traces ray to the surface, to determine where ray hits the surface 
	 * 
	 * @param ray ray being traced
	 * @param rayPosition position of the ray
	 * @param sufaceNormal normal to the surface
	 * @param surfaceNormalPosition position of the surface
	 * @param ci CollisionInfo context
	 */
	public final static void traceRay(Vector ray, Vector rayPosition, 
			Vector surfaceNormal, Vector surfaceNormalPosition, CollisionInfo ci)
	{
		float cDist = Float.NaN;
		//Vector Normal = getNormal();
		/*Log.d("traceRay.ray.x", ray.getiComponent() + "");
		Log.d("traceRay.ray.y", ray.getjComponent() + "");
		Log.d("traceRay.ray.z", ray.getkComponent() + "");*/
		
		/*Log.d("traceRay.rayPosition.x", rayPosition.getiComponent() + "");
		Log.d("traceRay.rayPosition.y", rayPosition.getjComponent() + "");
		Log.d("traceRay.rayPosition.z", rayPosition.getkComponent() + "");*/
		// Normal has incorrect orientation!!!
		/*Log.d("traceRay.Normal.x", Normal.getiComponent() + "");
		Log.d("traceRay.Normal.y", Normal.getjComponent() + "");
		Log.d("traceRay.Normal.z", Normal.getkComponent() + "");*/
		//Vector collisionPoint = new Vector(); old
		float dotProductRayNorm = ray.dotProduct(surfaceNormal);
		//Log.d("dotProductRayNorm", dotProductRayNorm + "");
		// make sure Normal and direction not perpendicular   , otherwise division by 0 happens
		if (dotProductRayNorm != 0){
			float d = surfaceNormalPosition.dotProduct(surfaceNormal); 
			cDist = (d - rayPosition.dotProduct(surfaceNormal)) / dotProductRayNorm;
			//ray = ray.multiplyByScalar(cDist); old
			//ray = ray.multiplyByScalar(cDist);
			ray.multiplySelfByScalar(cDist);
			/*Log.d("traceRay.rayPosition.x", rayPosition.getiComponent() + "");
			Log.d("traceRay.rayPosition.y", rayPosition.getjComponent() + "");
			Log.d("traceRay.rayPosition.z", rayPosition.getkComponent() + "");*/
			
			//Log.d("traceRay.rayXscalar.x", ray.getiComponent() + "");
			//Log.d("traceRay.rayXscalar.y", ray.getjComponent() + "");
			//Log.d("traceRay.rayXscalar.z", ray.getkComponent() + "");
			rayPosition.add(ray);
			
			//collisionPoint = rayPosition.addVector(ray.multiplyByScalar(cDist));
			//Log.d("traceRay.collisionPoint.x", collisionPoint.getiComponent() + "");
			//Log.d("traceRay.collisionPoint.y", collisionPoint.getjComponent() + "");
			//Log.d("traceRay.collisionPoint.z", collisionPoint.getkComponent() + "");
			ci.setcDist(ACEMath.roundf(cDist));
		}
		//ci.setcDist(ACEMath.roundf(cDist));
		ci.setCollisionPoint(rayPosition);
		//return new CollisionInfo(rayPosition, cDist);
	}
	
	public static final void detectCollision(Ace3dObject obj1, Ace3dObject obj2, CollisionInfo ci) // creates new CollisionInfo Objects
	{
		//CollisionInfo ci = new CollisionInfo();
		//if object is of type Ace3dObject than it has noCollisionRadius
		//normally all Collidable passed to this method objects will be Ace3dObject
		float ncr = ((Ace3dObject)obj2).getNoCollisionRadius();			
		obj2.getWorldPosition(tempObjWorldPosition2);
		obj1.getWorldPosition(tempObjWorldPosition1);
		//float distance = (obj1.getWorldPosition().subtractVector(pos)).getLength(); old way
		float distance = ACEMath.getLengthBetweenVectors(tempObjWorldPosition1, tempObjWorldPosition2);
			
		//	Log.d("ACE3d.detectCollision.distance", distance + "");
		//Log.d("ACE3d.detectCollision.ncr_x2", (ncr + obj1.getNoCollisionRadius()) + "");
			
		//if distance between objects is less then sum of noCollisionRadii, 
		//then collision is possible
		if (distance < (ncr + obj1.getNoCollisionRadius())) 
		{
			float cr = ((Ace3dObject)obj2).getCollisionRadius();
			//two object are close enough for us not to check colliders
			
			if ((distance < (cr + obj1.getCollisionRadius())) && (cr > 0) 
					&& (obj1.getCollisionRadius() > 0)) 
			{							
				//ci = new CollisionInfo(pos, 0.001f);			// new Collision Info	
				ci.setCollisionPoint(tempObjWorldPosition2);
				ci.setcDist(0.001f);
				ci.setType(CollisionType.POINT_TO_PLANE);
			}
			else{
				//since two objects are close, place colliders to the right spot
				//obj1.moveColliders();					
				//obj2.moveColliders();
				//check for collision from all colliders of the Ace3dObject
				
				ArrayList<Collider> o1Cldrs = ((Ace3dObject)obj1).getColliders();
				ArrayList<Collider> o2Cldrs = ((Ace3dObject)obj2).getColliders();
				for (int i = 0; i < o1Cldrs.size(); i++)
					for (int j = 0; j < o2Cldrs.size(); j++)
					{
						Collider cl = o2Cldrs.get(j); 
						detectCollision(cl, o1Cldrs.get(i), ci);
						//exit loops if collision happens
						if (ci.getType() != CollisionType.NO_COLLISION){
							i = o1Cldrs.size();
							j = o2Cldrs.size();
						}
					}
			}				
		}	
		//return ci;
	}
	
	protected static void detectCollision(Collider col1, Collider col2, CollisionInfo ci)
	{
		if (col1 instanceof PointCollider){
			if (col2 instanceof RectangularPlateCollider)
				rectangularWithPoint((RectangularPlateCollider)col2, (PointCollider)col1, ci);
			else if(col2 instanceof TriangularPlateCollider)
				triangularWithPoint((TriangularPlateCollider)col2, 
						(PointCollider)col1, ci);
		}
		
		else if(col2 instanceof PointCollider){
			if (col1 instanceof RectangularPlateCollider)
				rectangularWithPoint((RectangularPlateCollider)col1, (PointCollider)col2, ci);
			else if(col1 instanceof TriangularPlateCollider)
				triangularWithPoint((TriangularPlateCollider)col1, 
						(PointCollider)col2, ci);
		}		
				
	}
	
	
	/*private static Vector pointWithPoint(PointCollider pc1, PointCollider pc2)
	{
		return null;
	}
	
	private static Vector rectangularWithPlane(RectangularPlateCollider rpc, PlaneCollider plc)
	{
		return null;
	}*/
	
	private static final void rectangularWithPoint(RectangularPlateCollider rpc, PointCollider pc, CollisionInfo cInfo)
	{
		boolean isColliding = false;
		
		dD.resetVector();
		dD.subtractTwoVectors(pc.getPositionChange(), rpc.getPositionChange());
		//dD = pc.getPositionChange().subtractVector(rpc.getPositionChange()); // new vector
		
		//pc.getPositionChange().subtract(rpc.getPositionChange());
		//Vector dD = pc.getPositionChange();
		float cDist = Math.abs(cInfo.getcDist());
		//Vector drp = pc.getHostObjectsWorldPosition().subtractVector(pc.getRelativePosition());
		/*traceRay(pc.getRelativePosition(), pc.getHostObjectsWorldPosition(), rpc.getNormal(), rpc.getWorldPosition(), cInfo);
		
		if ((cDist >= 0) && (cDist <= 1))
		{
			cInfo.setType(CollisionType.POINT_TO_PLANE);
			cInfo.setPlaneNormal(rpc.getNormal());
			cInfo.setColliderRotationPosChange(pc.getDeltaRelPosition().subtractVector(rpc.getDeltaRelPosition()));
		
			cInfo.setCollisionVector(pc.getRelativePosition());
			cInfo.finalize();
			return;
		}*/
	
		/*Log.d("dD.rwp", dD.toString() + "");
		Log.d("pos.rwp", pos.toString() + "");
		Log.d("rpc.getNormal().rwp", rpc.getNormal().toString() + "");
		Log.d("rpc.getWorldPosition.rwp", rpc.getWorldPosition().toString() + "");*/
		pc.getWorldPosition(tempPCWorldPosition1);
		rpc.getWorldPosition(tempRPCWorldPosition1);
		traceRay(dD, tempPCWorldPosition1, rpc.getNormal(), tempRPCWorldPosition1, cInfo);
		 cDist = cInfo.getcDist();
		Vector collisionPoint = cInfo.getCollisionPoint();
		
		//Log.d("cDist.rwp", cDist + "");
		
		//float dotProductDirNorm = dD.dotProduct(Normal);
		if ((cDist >= 0) && (cDist <= 1))	{
			/*Log.d("collisionPoint.x", collisionPoint.getiComponent() + "");
			Log.d("collisionPoint.y", collisionPoint.getjComponent() + "");
			Log.d("collisionPoint.z", collisionPoint.getkComponent() + "");*/
			//rotate to make calculations easier
			//Log.d("collisionPoint.gRY", -rpc.getRotationY() + "");
			/*collisionPoint.rotateY(-rpc.getRotationY());
			collisionPoint.rotateZ(-rpc.getRotationFromHorizont());*/
			
			Vector Length = tempRPCWorldPosition1.subtractVector(collisionPoint);
			float length = Length.getLength();
			//Log.d("length", length + "");
			/*Log.d("collisionPoint.x", collisionPoint.getiComponent() + "");
			Log.d("collisionPoint.y", collisionPoint.getjComponent() + "");
			Log.d("collisionPoint.z", collisionPoint.getkComponent() + "");*/
			float angle = rpc.getHalfHeight().getAngleBetweenVectors(Length);//problem
			if (angle > piOver2)
				angle = (float) (Math.PI - angle);
			//Log.d("angle", angle + "");
			float hh = (float) (length * Math.cos(angle)); //<---previous
			//float hh = (float) (length * Math.sin(angle));
			//Log.d("hh", hh + "");
			if (rpc.getHalfHeightScalar() > hh){
				//angle = rpc.getHalfWidth().getAngleBetweenVectors(Length);//problem
				//if (angle > piOver2)
					//angle = (float) (Math.PI - angle);
				float ww = (float) (length * Math.sin(angle));//  <---previous
				//float ww = (float) (length * Math.cos(angle));
				//Log.d("ww", ww + "");
				if (rpc.getHalfWidthScalar() > ww){
					isColliding = true;	
					
					if (pc instanceof GravityCollider){
						cInfo.setType(CollisionType.GRAVITY_TO_PLANE);
						cInfo.setGcHeight(((GravityCollider)pc).getHeight());
						cInfo.setGcPassingHeight(((GravityCollider)pc).getPassingHeight());
					}
					else{
						cInfo.setType(CollisionType.POINT_TO_PLANE);
						cInfo.setPlaneNormal(rpc.getNormal());
						rpc.getDeltaRelPosition(deltaRelPos);
						pc.getDeltaRelPosition(pointDeltaRelPos);
						cInfo.setColliderRotationPosChange(pointDeltaRelPos.subtractVector(deltaRelPos));
						//cInfo.setColliderRotationPosChange(pc.getDeltaRelPosition().subtractVector(rpc.getDeltaRelPosition()));
					}
					
					pc.getHostObjectsWorldPosition(tempPCWorldPosition1);
					cInfo.setCollisionVector(collisionPoint.subtractVector(tempPCWorldPosition1));
					cInfo.finalize();
				}
			}
		}
		
		if (!isColliding)
			cInfo.reset();
			//create blank collisionInfo if no collision
			//cInfo = new CollisionInfo();
		//return cInfo;
	}
	
	
	private static final void triangularWithPoint(TriangularPlateCollider tpc, PointCollider pc, CollisionInfo cInfo)
	{
		boolean isColliding = false;
		
		//Log.d("Triangular With Point", testCCounter++ + "");
		
		//Vector dD = pc.getPositionChange().subtractVector(tpc.getPositionChange());
		dD.resetVector();
		dD.subtractTwoVectors(pc.getPositionChange(), tpc.getPositionChange());
		
		//pc.getPositionChange().subtract(tpc.getPositionChange());
		//Vector dD = pc.getPositionChange();
		
		/*Log.d("pc.getPositionChange().twp", pc.getPositionChange().toString());
		Log.d("tpc.getPositionChange().twp", tpc.getPositionChange().toString());
		
		Log.d("dD.twp", dD.toString());
		Log.d("pos.twp", pos.toString());*/
		//if ((dD.getLength() < tpc.getMaxDimension()) || (tpc.getMaxDimension() == 0.0f))
		pc.getWorldPosition(tempPCWorldPosition1);
		tpc.getWorldPosition(tempTPCWorldPosition1);
		
		traceRay(dD, tempPCWorldPosition1,tpc.getNormal(), tempTPCWorldPosition1, cInfo);
		
		float cDist = cInfo.getcDist();
		Vector Length =  cInfo.getCollisionPoint();
		
		//Log.d("cDist.twp", cDist + "");
		
		//float dotProductDirNorm = dD.dotProduct(Normal);
		if ((cDist >= 0) && (cDist <= 1))	{
			Length.subtract(tempTPCWorldPosition1);
			//Vector Length = tpc.worldPosition.subtractVector(collisionPoint);
			
			/*Log.d("tpc.worldPosition", tpc.worldPosition.toString());			
			Log.d("length", Length.getLength() + "");
			Log.d("collisionPoint.x", Length.getiComponent() + "");
			Log.d("collisionPoint.y", Length.getjComponent() + "");
			Log.d("collisionPoint.z", Length.getkComponent() + "");*/
			
			float angleAdjSide1 = tpc.getAdjSide1().getAngleBetweenVectors(Length);
			float angleAdjSide2 = tpc.getAdjSide2().getAngleBetweenVectors(Length);
			
			/*Log.d("AdjSide1", tpc.getAdjSide1().getiComponent() + "i "
					+ tpc.getAdjSide1().getjComponent() + "j "+
					tpc.getAdjSide1().getkComponent() +"k ");
			Log.d("AdjSide2", tpc.getAdjSide2().getiComponent() + "i " 
					+ tpc.getAdjSide2().getjComponent() + "j " +
					tpc.getAdjSide2().getkComponent() +"k ");			
			Log.d("angleAdjSide1", angleAdjSide1 + "");
			Log.d("angleAdjSide2", angleAdjSide2 + "");
			Log.d("getAngleBetweenAdjSides()", tpc.getAngleBetweenAdjSides() + "");*/
			
			if (angleAdjSide1 <= tpc.getAngleBetweenAdjSides() && 
					angleAdjSide2 <= tpc.getAngleBetweenAdjSides())
			{
				Vector c1 = tpc.getAdjSide1().subtractVector(Length);
				float Theta = c1.getAngleBetweenVectors(tpc.getAdjSide1());
				
				//Log.d("Theta", Theta + "");
				//Log.d("angleBetweenAdjSide1OppSide()()", tpc.angleBetweenAdjSide1OppSide() + "");
				
				if (Theta <= tpc.angleBetweenAdjSide1OppSide()){
					isColliding = true;		
					if (pc instanceof GravityCollider){
						cInfo.setType(CollisionType.GRAVITY_TO_PLANE);
						cInfo.setGcHeight(((GravityCollider)pc).getHeight());
						cInfo.setGcPassingHeight(((GravityCollider)pc).getPassingHeight());
					}
					else{
						cInfo.setType(CollisionType.POINT_TO_PLANE);
						cInfo.setPlaneNormal(tpc.getNormal());
						
						tpc.getDeltaRelPosition(deltaRelPos);
						pc.getDeltaRelPosition(pointDeltaRelPos);
						cInfo.setColliderRotationPosChange(pointDeltaRelPos.subtractVector(deltaRelPos));
						
						//cInfo.setColliderRotationPosChange(pc.getDeltaRelPosition().subtractVector(tpc.getDeltaRelPosition()));
					}
					cInfo.setCollisionVector(dD);
					cInfo.finalize();
				}
			}		
		}
		
		if (!isColliding)
			cInfo.reset();
			//create blank collisionInfo if no collision
			//cInfo = new CollisionInfo();
		//return cInfo;
	}
	
	@SuppressWarnings("unused")
	private static final void circularWithPoint(CircularPlateCollider cpc, PointCollider pc, CollisionInfo cInfo)
	{
		boolean isColliding = false;		
		
		//PointCollider p = (PointCollider)object;
		//Vector dD = pc.getPositionChange().subtractVector(cpc.getPositionChange());
		//Vector pos = pc.getWorldPosition();
		dD.resetVector();
		dD.subtractTwoVectors(pc.getPositionChange(), cpc.getPositionChange());
		pc.getWorldPosition(tempPCWorldPosition1);
		cpc.getWorldPosition(tempTPCWorldPosition1);
		
		traceRay(dD, tempPCWorldPosition1,cpc.getNormal(),tempTPCWorldPosition1, cInfo);
		float cDist = cInfo.getcDist();
		Vector collisionPoint = cInfo.getCollisionPoint();
		
		Log.d("cDist", cDist + "");
		
		//float dotProductDirNorm = dD.dotProduct(Normal);
		if ((cDist >= 0) && (cDist <= 1))	{
			Vector Length = tempTPCWorldPosition1.subtractVector(collisionPoint);
			float length = Length.getLength();
			Log.d("length", length + "");
			//Log.d("collisionPoint.x", collisionPoint.getiComponent() + "");
			//Log.d("collisionPoint.y", collisionPoint.getjComponent() + "");
			//Log.d("collisionPoint.z", collisionPoint.getkComponent() + "");
			if (length <= cpc.getRadius())
			{
				isColliding = true;
				cInfo.setCollisionVector(dD);
				cInfo.finalize();
			}
		}
		
		if (!isColliding)
			cInfo.reset();
			//create blank collisionInfo if no collision
			//cInfo = new CollisionInfo();
		//return cInfo;
	}

}

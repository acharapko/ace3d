package ac.ace3d;

import android.util.Log;

public class Vectorf implements Cloneable {

	private float i, j, k;
	
	public Vectorf()
	{
		this(0,0,0); //default Vector
	}
	
	public Vectorf(float[] v)
	{
		if (v.length == 3){
			i = v[0];
			j = v[1];
			k = v[2];
		}
		else
		{
			i = 0;
			j = 0;
			j = 1;
		}
				
	}
	public Vectorf(float x, float y, float z)
	{
		i = x;
		j = y;
		k = z;
	}
	/**
	 * sets vector if it is a Zero-vector
	 * @param v
	 * Vector parameter.
	 */
	public void setVector(float[] v)
	{
		if (getLength() == 0)
			if (v.length == 3){
				i = v[0];
				j = v[1];
				k = v[2];
			}
		
	}
	
	/**
	 * 
	 * @return
	 * true if vector has no length, false if vector is not zero
	 */
	
	public boolean isZero()
	{
		boolean r = true;
		if (getLength() > 0)
			r = false;		
		return r;
	}
	
	/**getiComponent()
	 * 
	 * @return
	 * 		returns i component of a vector (along the x-axis)
	 */
	public float getiComponent()
	{
		return i;
	}
	/**getjComponent()
	 * 
	 * @return
	 * 		returns j component of a vector (along the y-axis)
	 */
	public float getjComponent()
	{
		return j;
	}
	/**getkComponent()
	 * 
	 * @return
	 * 		returns k component of a vector (along the z-axis)
	 */
	public float getkComponent()
	{
		return k;
	}
	/** dotProduct(Vector v)
	 * 
	 * @param v
	 * 		Second Vector used to calculate dot product
	 * @return
	 * 		dot product of the vector and the vector v
	 */
	public float dotProduct(Vectorf v)
	{
		return v.getiComponent() * i + v.getjComponent() * j 
		            + v.getkComponent() * k;
	}
	
	
	/** crossProduct(Vector v)
	 * 
	 * @param v
	 * 		Second Vector used to calculate cross product. 
	 * 		Order of vectors matter for cross product. 
	 * @return
	 * 		cross product of the vector and the vector v
	 */
	
	public Vector crossProduct(Vectorf v)
	{
		float[] newv = new float[3];
		newv[0] = j * v.getkComponent() - k * v.getjComponent();
		newv[1] = k * v.getiComponent() - i * v.getkComponent();
		newv[2] = i * v.getjComponent() - j * v.getiComponent();
		
		return new Vector(newv);
		
	}
	/** dotProduct(Vector v1, Vector v2)
	 * 
	 * @param v1
	 * 		First vector for calculating dot product
	 * @param v2
	 * 		Second vector for calculating dot product
	 * @return
	 * 		dot product of v1 and v2
	 */
	public static float dotProduct(Vectorf v1, Vectorf v2)
	{
		return v1.getiComponent() * v2.getiComponent() + v1.getjComponent() * v2.getjComponent() 
		         + v1.getkComponent() * v2.getkComponent();
	}
	/** crossProduct(Vector v1, Vector v2)
	 * 
	 * @param v1
	 * 		First vector for calculating cross product
	 * @param v2
	 *		Second vector for calculating cross product
	 * @return
	 * 		Cross product (Vector multiplication) vector of v1 x v2. 
	 * 		order matters. v1 x v2 != v2 x v1
	 */
	public static Vector crossProduct(Vectorf v1, Vectorf v2)
	{
		float[] newv = new float[3];
		newv[0] = v1.getjComponent() * v2.getkComponent() - v1.getkComponent() * v2.getjComponent();
		newv[1] = v1.getkComponent() * v2.getiComponent() - v1.getiComponent() * v2.getkComponent();
		newv[2] = v1.getiComponent() * v2.getjComponent() - v1.getjComponent() * v2.getiComponent();
		
		return new Vector(newv);
	}
	/**
	 * 
	 * @return
	 * 		Length of the Vector
	 */
	
	public float getLength()
	{
		return (float)(Math.sqrt(Math.pow(i, 2) + Math.pow(j, 2) + Math.pow(k, 2)));
	}
	/**
	 * 
	 * @return
	 * 	length of the the projection on XY plane
	 */
	public float getLengthXY()
	{
		return (float)(Math.sqrt(Math.pow(i, 2) + Math.pow(j, 2)));
	}
	/**
	 * 
	 * @return
	 * 	length of the the projection on XZ plane
	 */
	
	public float getLengthXZ()
	{
		return (float)(Math.sqrt(Math.pow(i, 2) + Math.pow(k, 2)));
	}
	
	/**
	 * 
	 * @return
	 * 	length of the the projection on YZ plane
	 */	
	public float getLengthYZ()
	{
		return (float)(Math.sqrt(Math.pow(j, 2) + Math.pow(k, 2)));
	}
	/**
	 * not correct!!!
	 * @param angle
	 */
	public void rotateX(float angle)
	{
		angle = (float)(angle * Math.PI / 180);
		float length = getLengthYZ();
		if (length != 0){
			float cosTheta = j / length;
			float sinTheta = k / length;
			//float theta = (float)(Math.acos(cosTheta)); //in radians
			float theta = getArcCosine(cosTheta, sinTheta); 
			theta = theta + angle;
			cosTheta = (float) (Math.cos(theta));
			sinTheta = (float) (Math.sin(theta));
			j = cosTheta * length;
			k = sinTheta * length;
		}
	}
	/**
	 * not correct!!!
	 * @param angle in degrees
	 */
	public void rotateY(float angle)
	{
		angle = (float)(angle * Math.PI / 180);
		float length = getLengthXZ();
		if (length != 0){
			float cosTheta = i / length;
			float sinTheta = j / length;			
			//float theta = (float)(Math.acos(cosTheta)); //in radians
			float theta = getArcCosine(cosTheta, sinTheta);
			Log.d("length:", length + "");
			Log.d("thetaOld:", theta + "");
			theta = theta + angle;		
			cosTheta = (float) (Math.cos(theta));
			sinTheta = (float) (Math.sin(theta));			
			i = cosTheta * length;
			k = sinTheta * length;
		}
	}
	/**
	 * not correct!!!
	 * @param angle
	 * 		angle, in degrees to rotate around z-axis
	 */
	public void rotateZ(float angle)
	{
		angle = (float)(angle * Math.PI / 180);
		float length = getLengthXY();
		if (length != 0){
			float cosTheta = i / length;
			float sinTheta = j / length;
			//float theta = (float)(Math.acos(cosTheta)); //in radians
			float theta = getArcCosine(cosTheta, sinTheta); 
			theta = theta + angle;
			cosTheta = (float) (Math.cos(theta));
			sinTheta = (float) (Math.sin(theta));
			i = cosTheta * length;
			j = sinTheta * length;
		}
	}
	
	// not correct!!!???
	public float getRotationAngleAroundYaxis()
	{
		float angle = 0.0f;
		float length = getLengthXZ();
		if (length != 0){
			float cosTheta = i / length;
			float sinTheta = k / length;		
			angle = (float) (/*(Math.PI * 2)*/ - getArcCosine(cosTheta, sinTheta)); 			
		}
		return (float) (angle / Math.PI * 180);
		
		/*float angle = 0.0f;
		float lengthXY = getLengthXY();
		if (lengthXY != 0){
			float tanTheta = k / lengthXY;			
			angle = (float) Math.atan(tanTheta);
		}
		else if (k > 0)
			angle = (float) (Math.PI / 2);
		else if (k < 1)
			angle = (float) (- Math.PI / 2);
		angle = (float) (Math.PI * 2 - angle);
		angle = (float) (angle / Math.PI * 180);
		Log.d("Vector.graay.angle", angle + "");
		return angle;*/
	}
	
	// not correct!!!???
	public float getRotationAngleAroundZXaxis()
	{
		float angle = 0.0f;
		float lengthXZ = getLengthXZ();
		if (lengthXZ != 0){
			float tanTheta = j / lengthXZ;
			//float sinTheta = j / length;
			//Log.d("Vector.graaz.i", i + "");
			//Log.d("Vector.graaz.j", j + "");
			//Log.d("Vector.graaz.LengthXZ", lengthXZ + "");
			//angle = getArcCosine(cosTheta, sinTheta); 
			//if (j >= 0)
			angle = (float) Math.atan(tanTheta);
			//else
				
		}
		else if (k >= 0)
			angle = (float) (Math.PI / 2);
		else if (k < 0)
			angle = (float) (Math.PI / 2);
		angle = (float) (angle / Math.PI * 180);
		//Log.d("Vector.graaz.angle", angle + "");
		return angle;
	}
	
	private float getArcCosine(float cosValue, float sinValue) 
	{
		double c = Math.acos(cosValue);
		if (sinValue < 0){
			/*if ((c > 0)&&(c < (Math.PI / 2)))
				c = (2 * Math.PI) - c;
			else if ((c > Math.PI / 2) && (c < (Math.PI)))
			{
				double c2 = (2 * Math.PI) - c;
				c = (2 * Math.PI) + c2;
			}*/
			c = (2 * Math.PI) - c;
		}		
		return (float) c;
	}
	/**
	 * 
	 * @param v
	 * @return
	 * returns the smallest angle between two vectors in radians.
	 */
	public float getAngleBetweenVectors(Vectorf v)
	{
		float dt = dotProduct(v);
		float lengthProduct = getLength() * v.getLength();
		float cosPhi = dt / lengthProduct;
		return (float) Math.acos(cosPhi);
		
	}
	
	/**
	 * 
	 * @param v
	 * @return
	 * returns the smallest angle between two vectors in degrees.
	 */
	public float getAngleBetweenVectorsInDegres(Vectorf v)
	{
		float dt = dotProduct(v);
		float lengthProduct = getLength() * v.getLength();
		float cosPhi = dt / lengthProduct;
		return (float) (Math.acos(cosPhi)* 180 / Math.PI);
		
	}
	/**
	 * 
	 * @return
	 * 	Normalized vector (vector of length 1 and the same direction as original)
	 */
	public Vector normilize()
	{
		float[] v2 = new float[3];
		float length = getLength();
		v2[0] = i / length;
		v2[1] = j / length;
		v2[2] = k / length;
		
		return new Vector(v2);
		
	}
	/**
	 * 
	 * @param v
	 * Vector to be added to the current Vector
	 * @return
	 *  sum of two vectors
	 */
	public Vector addVector(Vector v)
	{
		float[] v2 = new float[3];
		v2[0] = i + v.getiComponent();
		v2[1] = j + v.getjComponent();
		v2[2] = k + v.getkComponent();
		
		return new Vector(v2);
	}
	
	/**
	 * add to this vector, changing its values to the sum
	 * @param v
	 */
	public void add(Vectorf v)
	{
		
		i += v.getiComponent();
		j += v.getjComponent();
		k += v.getkComponent();
		
		
	}
	/**
	 * subtracts from this vector, changing its values to the difference
	 * @param v
	 */
	public void subtract(Vectorf v)
	{
		
		i -= v.getiComponent();
		j -= v.getjComponent();
		k -= v.getkComponent();
		
		
	}
	/**
	 * 
	 * @param v
	 * Vector to be subtracted from the current Vector
	 * @return
	 *  difference of two vectors
	 */
	public Vectorf subtractVector(Vectorf v)
	{
		float[] v2 = new float[3];
		v2[0] = i - v.getiComponent();
		v2[1] = j - v.getjComponent();
		v2[2] = k - v.getkComponent();
		
		return new Vectorf(v2);
	}
	/**
	 * multiplies this vector by a scalar and returns new vector as a result;
	 * @param scalar
	 *  
	 * @return Vector after multiplication
	 */
	public Vectorf multiplyByScalar(float scalar)
	{
		float[] v2 = new float[3];
		v2[0] = i * scalar;
		v2[1] = j * scalar;
		v2[2] = k * scalar;
		
		return new Vectorf(v2);
	}
	/**
	 * multiplies this vector by a scalar
	 * @param scalar
	 */
	public void multiplySelfByScalar(float scalar)
	{
		//float[] v2 = new float[3];
		i *= scalar;
		j *= scalar;
		k *= scalar;
		
		//return new Vector(v2);
	}
	
	/**
	 * 
	 * @param scalar
	 * @return
	 */
	public Vectorf divideByScalar(float scalar)
	{
		float[] v2 = new float[3];
		v2[0] = i / scalar;
		v2[1] = j / scalar;
		v2[2] = k / scalar;
		
		return new Vectorf(v2);
	}
	
	
	public String toString()
	{
		return getiComponent() + "i " +
				getjComponent() + "j "+
				getkComponent() +"k ";
	}
	
	public Vectorf clone()
	{
		return new Vectorf(i, j, k);
		
	}
}
